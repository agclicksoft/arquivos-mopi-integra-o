
CREATE TABLE IF NOT EXISTS alunos (
   id int(10) unsigned NOT NULL AUTO_INCREMENT,
   ra char(10) NOT NULL DEFAULT '0',
   codPessoa char(07) NOT NULL DEFAULT '0',
   unidade char(15) NOT NULL DEFAULT '#',
-- codPessoa_pai char(10) NOT NULL DEFAULT '0',
-- codPessoa_mae char(10) NOT NULL DEFAULT '0',
   codpessoa_respAcad char(07) NOT NULL DEFAULT '0',
   tipoPessoa_respAcad char(03) NOT NULL DEFAULT '#',
   codPessoa_respFinanc char(07) NOT NULL DEFAULT '0',
   tipoPessoa_respFinanc char(03) NOT NULL DEFAULT '#',

   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;





CREATE TABLE IF NOT EXISTS alunoacademicos (
   id int(10) unsigned NOT NULL AUTO_INCREMENT,
   ra char(10) NOT NULL DEFAULT '0',

   tipoMatricula CHAR(03) NOT NULL DEFAULT '#',
   tipoAluno CHAR(03) NOT NULL default '#',

   anoLetivo char(05) NOT NULL DEFAULT '#',

   codCurso char(03) NOT NULL DEFAULT '#',
   codSerie char(03) NOT NULL DEFAULT '#',
   turma char(30) NOT NULL DEFAULT '#',
   turno char(15) NOT NULL DEFAULT '#',

   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;





CREATE TABLE IF NOT EXISTS alunopessoas (
   id int(10) unsigned NOT NULL AUTO_INCREMENT,

   codPessoa CHAR(07) NOT NULL DEFAULT '0',     -- codPessoa
   nome CHAR(70) NOT NULL DEFAULT '#',          -- nome
   sexo CHAR(03) NOT NULL DEFAULT 'M',          -- sexo
   dataNasciment CHAR(25) NOT NULL DEFAULT '0', -- Data de Nascimento
   nacionalidade CHAR(03) NOT NULL DEFAULT '#', -- Nacionalidade
   estadoNatal CHAR(03) NOT NULL DEFAULT '#',   -- Estado Natal
   estadoCivil CHAR(03) NOT NULL DEFAULT '#',   -- Estado Civil

   endRua CHAR(40) NOT NULL DEFAULT '#',        --  Rua
   endNumero CHAR(10) NOT NULL DEFAULT '#',     -- Numero
   endComplement CHAR(30) NOT NULL DEFAULT '#', -- Complemento
   endCEP CHAR(10) NOT NULL DEFAULT '#',        -- CEP
   endBairro CHAR(25) NOT NULL DEFAULT '#',     -- Bairro
   endCidade CHAR(25) NOT NULL DEFAULT '#',     -- Cidade
   endEstado CHAR(03) NOT NULL DEFAULT '#',     -- Estado

   contEMail CHAR(50) NOT NULL default '#',       -- eMail
   contCelular CHAR(15) NOT NULL DEFAULT '#',     -- Celular
   contTelResidenc CHAR(15) NOT NULL DEFAULT '#', -- Telefone Residencial

   docCertNasc CHAR(20) NOT NULL DEFAULT '#',           -- Certidão de Nascimento
   docNum_certNasc CHAR(20) NOT NULL DEFAULT '0',       -- Numero da Certidão
   docIdentNumero CHAR(20) NOT NULL DEFAULT '0',        -- Numero da Identidade
   docIdent_orgEmissor CHAR(25) NOT NULL DEFAULT '#',   -- Orgão Emissor
   docIdent_dataEmissao CHAR(25) NOT NULL DEFAULT '0',  -- Data de Emissão
   docEstadEmissor CHAR(03) NOT NULL DEFAULT '#',       -- Estado Emissor
   docCPF CHAR(15) NOT NULL DEFAULT '0',                -- CPF

   dataUpDate CHAR(25) NOT NULL DEFAULT '0', -- Data de Atualizaçao do Registro
   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



   -- _  __  _  __.-  --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=--  -.__  _  __  _ --



CREATE TABLE IF NOT EXISTS responsavelpessoas (
   id int(10) unsigned NOT NULL AUTO_INCREMENT,
   ra char(10) NOT NULL DEFAULT '0',

   tipoRespons CHAR(15) NOT NULL DEFAULT '0', 
   grauParent CHAR(03) NOT NULL DEFAULT '0', 
   codPessoa CHAR(07) NOT NULL DEFAULT '0', 
   nome CHAR(70) NOT NULL DEFAULT '#', 
   sexo CHAR(03) NOT NULL DEFAULT 'M', 
   dataNasciment CHAR(25) NOT NULL DEFAULT '0', 
   nacionalidade CHAR(03) NOT NULL DEFAULT '#',
   estadoNatal CHAR(03) NOT NULL DEFAULT '#',
   estadoCivil CHAR(03) NOT NULL DEFAULT '#',

   endRua CHAR(40) NOT NULL DEFAULT '#', 
   endNumero CHAR(10) NOT NULL DEFAULT '#', 
   endComplement CHAR(30) NOT NULL DEFAULT '#', 
   endCEP CHAR(10) NOT NULL DEFAULT '#', 
   endBairro CHAR(25) NOT NULL DEFAULT '#', 
   endCidade CHAR(25) NOT NULL DEFAULT '#', 
   endEstado CHAR(03) NOT NULL DEFAULT '#', 

   contEMail CHAR(50) NOT NULL default '#',
   contCelular CHAR(15) NOT NULL DEFAULT '#',
   contTelResidenc CHAR(15) NOT NULL DEFAULT '#',

   docCertNasc CHAR(20) NOT NULL DEFAULT '#', 
   docNum_certNasc CHAR(20) NOT NULL DEFAULT '0', 
   docIdentNumero CHAR(20) NOT NULL DEFAULT '0', 
   docIdent_orgEmissor CHAR(25) NOT NULL DEFAULT '#', 
   docIdent_dataEmissao CHAR(25) NOT NULL DEFAULT '0', 
   docEstadEmissor CHAR(03) NOT NULL DEFAULT '#', 
   docCPF CHAR(15) NOT NULL DEFAULT '0', 

   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE IF NOT EXISTS professorpessoas (
   id int(10) unsigned NOT NULL AUTO_INCREMENT,
   codProf char(07) NOT NULL DEFAULT '0',

-- tipo CHAR(15) NOT NULL DEFAULT '0', 
-- funcao CHAR(03) NOT NULL DEFAULT '0', 
   codPessoa CHAR(07) NOT NULL DEFAULT '0', 
   nome CHAR(70) NOT NULL DEFAULT '#', 
   sexo CHAR(03) NOT NULL DEFAULT 'M', 
   dataNasciment CHAR(25) NOT NULL DEFAULT '0', 
   nacionalidade CHAR(03) NOT NULL DEFAULT '#', 
   estadoNatal CHAR(03) NOT NULL DEFAULT '#', 
   estadoCivil CHAR(03) NOT NULL DEFAULT '#', 

   endRua CHAR(40) NOT NULL DEFAULT '#', 
   endNumero CHAR(10) NOT NULL DEFAULT '#', 
   endComplement CHAR(30) NOT NULL DEFAULT '#', 
   endCEP CHAR(10) NOT NULL DEFAULT '#', 
   endBairro CHAR(25) NOT NULL DEFAULT '#', 
   endCidade CHAR(25) NOT NULL DEFAULT '#', 
   endEstado CHAR(03) NOT NULL DEFAULT '#', 

   contEMail CHAR(50) NOT NULL default '#', 
   contCelular CHAR(15) NOT NULL DEFAULT '#', 
   contTelResidenc CHAR(15) NOT NULL DEFAULT '#', 

   docIdentNumero CHAR(20) NOT NULL DEFAULT '0', 
   docIdent_orgEmissor CHAR(25) NOT NULL DEFAULT '#',
   docIdent_dataEmissao CHAR(25) NOT NULL DEFAULT '0', 
   docEstadEmissor CHAR(03) NOT NULL DEFAULT '#', 
   docCPF CHAR(15) NOT NULL DEFAULT '0', 

   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


   -- _  __  _  __.-  --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=--  -.__  _  __  _ --


CREATE TABLE IF NOT EXISTS disciplinas (
   id int(10) unsigned NOT NULL AUTO_INCREMENT,

   codDisc char(05) NOT NULL DEFAULT '0',
   anoLetivo char(05) NOT NULL DEFAULT '#',
   codProfessor CHAR(05) NOT NULL default '0',
   nome CHAR(70) NOT NULL DEFAULT '#', 
   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;





CREATE TABLE IF NOT EXISTS etapas ( -- OLD
   id int(10) unsigned NOT NULL AUTO_INCREMENT,

   etapa char(03) NOT NULL DEFAULT '0',
   descricao char(35) NOT NULL DEFAULT '#',
   anoLetivo char(05) NOT NULL DEFAULT '#',
   codDisc CHAR(05) NOT NULL default '0',
   disciplina CHAR(70) NOT NULL DEFAULT '#', 
   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;





CREATE TABLE IF NOT EXISTS etapas (
   id int(10) unsigned NOT NULL AUTO_INCREMENT,

   codEtapa char(03) NOT NULL DEFAULT '0',
   descricao char(35) NOT NULL DEFAULT '#',
   anoLetivo char(05) NOT NULL DEFAULT '#',
   codDisc CHAR(05) NOT NULL default '0',
   tipoEtapa char(01) NOT NULL DEFAULT '#',
   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


   -- _  __  _  __.-  --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=--  -.__  _  __  _ --


CREATE TABLE IF NOT EXISTS notas ( -- OLD
   id int(10) unsigned NOT NULL AUTO_INCREMENT,
   ra char(10) NOT NULL DEFAULT '0',

   etapa char(03) NOT NULL DEFAULT '#',
   descricao char(35) NOT NULL DEFAULT '#',
   anoLetivo char(05) NOT NULL DEFAULT '#',
   codDisc CHAR(05) NOT NULL default '0',
   nota CHAR(10) NOT NULL DEFAULT '#', 
   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;





CREATE TABLE IF NOT EXISTS notasfaltas (
   id int(10) unsigned NOT NULL AUTO_INCREMENT,
   ra char(10) NOT NULL DEFAULT '0',

   codEtapa char(03) NOT NULL DEFAULT '#',
   descricao char(35) NOT NULL DEFAULT '#',
   anoLetivo char(05) NOT NULL DEFAULT '#',
   codDisc CHAR(05) NOT NULL default '0',
   tipoEtapa char(01) NOT NULL DEFAULT '#',
   notaFalta CHAR(10) NOT NULL DEFAULT '#', 
   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


   -- _  __  _  __.-  --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=--  -.__  _  __  _ --


CREATE TABLE IF NOT EXISTS servicos (
   id int(10) unsigned NOT NULL AUTO_INCREMENT,

   descricao char(35) NOT NULL DEFAULT '#',
   produto char(03) NOT NULL DEFAULT '#',
   prodStatus char(01) NOT NULL DEFAULT '#',
   prodValor CHAR(10) NOT NULL default '0',

   descBolsa char(03) NOT NULL DEFAULT '#',
   descPeriodo char(01) NOT NULL DEFAULT '#',
   descTipo char(01) NOT NULL DEFAULT '#',
   descValor char(10) NOT NULL DEFAULT '#',
   descStatus char(01) NOT NULL DEFAULT '#',

   codSerie char(03) NOT NULL DEFAULT '#',
   codTurma char(15) NOT NULL DEFAULT '#',
   codTurno char(15) NOT NULL DEFAULT '#',
   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
