
DROP TABLE SULTIMATE

CREATE TABLE SUPDATE
(
   ID int IDENTITY(1,1) PRIMARY KEY,
   MODULO char(50) NOT NULL DEFAULT '#',
   DTCORRENT char(30) NOT NULL DEFAULT '0',
   DTULTIMAT char(30) NOT NULL DEFAULT '0',
   ULTFLAG char(01) NOT NULL DEFAULT '0'
)

   -- _  __  _  __.-  --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=--  -.__  _  __  _ --


CREATE TABLE IF NOT EXISTS sysinfor (
   id INT(10) unsigned NOT NULL AUTO_INCREMENT,
   infor_I CHAR(50) NOT NULL DEFAULT '0',
   infor_II CHAR(50) NOT NULL DEFAULT '0',
   infor_III CHAR(50) NOT NULL DEFAULT '0',
   inFlag CHAR(01) NOT NULL DEFAULT '0',

   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO sysinfor
   ( infor_I, infor_II )
VALUES
   ( 'Alunos_TS', CONCAT(CURDATE(), '~', CURTIME()) ),
   ( 'Alunos_ST', CONCAT(CURDATE(), '~', CURTIME()) ),
   ( 'Respons_TS', CONCAT(CURDATE(), '~', CURTIME()) ),
   ( 'Respons_ST', CONCAT(CURDATE(), '~', CURTIME()) ),
   ( 'Professor_TS', CONCAT(CURDATE(), '~', CURTIME()) ),
   ( 'Professor_ST', CONCAT(CURDATE(), '~', CURTIME()) ),
   ( 'Notas_TS', CONCAT(CURDATE(), '~', CURTIME()) ),
   ( 'Notas_ST', CONCAT(CURDATE(), '~', CURTIME()) )


UPDATE supdate
-- SET dtUltima = CONCAT(CURDATE(), 'T', CURTIME(), '-03:00')
   SET dtUltimat = '2015-07-13~16:34:46'
 WHERE modulo = 'Alunos'


UPDATE supdate
   SET dtCorrent = dtUltimat,
         ultFlag = '0'
 WHERE modulo = 'Alunos' 


UPDATE supdate
   SET dtCorrent = '2015-07-13~15:40:00',
         ultFlag = '1'
 WHERE modulo = 'Alunos' 


-- ECT ult.*, CONCAT(CURDATE(), 'T', CURTIME(), '-03:00') xData
SELECT ult.*, CONCAT(CURDATE(), '~', CURTIME()) xData
  FROM supdate ult
-- '1', 'Alunos', '2015-07-13~15:40:00', '2015-07-13~16:34:46', '1' --


   -- _  __  _  __.-  --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=--  -.__  _  __  _ --


CREATE TABLE IF NOT EXISTS tabcontrol (
   id INT(10) unsigned NOT NULL AUTO_INCREMENT,
   Processo CHAR(50) NOT NULL DEFAULT 'X',
   inc_dateTime CHAR(50) NOT NULL DEFAULT '0',
   inc_Flag CHAR(01) NOT NULL DEFAULT '0',
   alt_dateTime CHAR(50) NOT NULL DEFAULT '0',
   alt_Flag CHAR(01) NOT NULL DEFAULT '0',

   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO tabcoltrol
   ( Processo, inc_dateTime, inc_Flag, alt_dateTime, alt_Flag )
VALUES
   ( 'Alunos_TS', '2015-12-22~12:00:00', '0', '2015-12-22~14:00:00', '0' ),
   ( 'Alunos_ST', '2015-12-22~12:00:00', '0', '2015-12-22~14:00:00', '0' ),
   ( 'Respons_TS', '2015-12-22~12:00:00', '0', '2015-12-22~14:00:00', '0' ),
   ( 'Respons_ST', '2015-12-22~12:00:00', '0', '2015-12-22~14:00:00', '0' ),
   ( 'Professor_TS', '2015-12-22~12:00:00', '0', '2015-12-22~14:00:00', '0' ),
   ( 'Professor_ST', '2015-12-22~12:00:00', '0', '2015-12-22~14:00:00', '0' ),
   ( 'Notas_TS', '2015-12-22~12:00:00', '0', '2015-12-22~14:00:00', '0' ),
   ( 'Notas_ST', '2015-12-22~12:00:00', '0', '2015-12-22~14:00:00', '0' )


update tabcontrol 
   set inc_dateTime = '2015-12-22~19:15:00', 
       inc_Flag = '0', 
       alt_dateTime = '2015-12-22~19:15:00',
       alt_Flag = '0'
 where id > 0
 