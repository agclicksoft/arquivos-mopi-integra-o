
CREATE TABLE IF NOT EXISTS salunopessoas (
   id int(10) unsigned NOT NULL AUTO_INCREMENT,
   ra char(10) NOT NULL DEFAULT '0',
   codPessoa CHAR(07) NOT NULL DEFAULT '0',
   idSys char(40) NOT NULL DEFAULT '#',

   nome CHAR(70) NOT NULL DEFAULT '#',
   sexo CHAR(03) NOT NULL DEFAULT 'M',
   dataNasciment CHAR(25) NOT NULL DEFAULT '0',
   nacionalidade CHAR(03) NOT NULL DEFAULT '#',
   naturalidade CHAR(25) NOT NULL DEFAULT '#',
   estadoNatal CHAR(03) NOT NULL DEFAULT '#',
-- cidadeNatal CHAR(25) NOT NULL DEFAULT '#',
   estadoCivil CHAR(03) NOT NULL DEFAULT '#',
   tipoAluno CHAR(03) NOT NULL DEFAULT '#',

   endRua CHAR(40) NOT NULL DEFAULT '#',
   endNumero CHAR(10) NOT NULL DEFAULT '#',
   endComplement CHAR(30) NOT NULL DEFAULT '#',
   endCEP CHAR(10) NOT NULL DEFAULT '#',
   endBairro CHAR(25) NOT NULL DEFAULT '#',
   endCidade CHAR(25) NOT NULL DEFAULT '#',
   endEstado CHAR(03) NOT NULL DEFAULT '#',

   contEMail CHAR(50) NOT NULL default '#',
   contCelular CHAR(15) NOT NULL DEFAULT '#',
   contTelResidenc CHAR(15) NOT NULL DEFAULT '#',

   docIdent_Numero CHAR(15) NOT NULL DEFAULT '0',
   docIdent_orgEmissor CHAR(25) NOT NULL DEFAULT '#',
   docIdent_dataEmissao CHAR(25) NOT NULL DEFAULT '0',
   docIdent_estEmissor CHAR(03) NOT NULL DEFAULT '#',
   docCPF CHAR(15) NOT NULL DEFAULT '0',

   docCert_Tipo CHAR(02) NOT NULL DEFAULT '#',
   docCert_Numero CHAR(35) NOT NULL DEFAULT '0',
   docCert_Cartorio CHAR(50) NOT NULL DEFAULT '#',
   docCert_Comarca CHAR(35) NOT NULL DEFAULT '#',
   docCert_Data CHAR(25) NOT NULL DEFAULT '0',
   docCert_Destrito CHAR(25) NOT NULL DEFAULT '#',
   docCert_UF CHAR(03) NOT NULL DEFAULT '#',
   docCert_Folha CHAR(10) NOT NULL DEFAULT '0',
   docCert_Livro CHAR(10) NOT NULL DEFAULT '0',
   docCert_NServent CHAR(10) NOT NULL DEFAULT '0',

   flagFoto CHAR(01) NOT NULL DEFAULT '0',
   flagProcess CHAR(01) NOT NULL DEFAULT '0',
   dataUpDate CHAR(25) NOT NULL DEFAULT '0',
   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



INSERT INTO salunopessoas
   (ra, nome)
VALUES
   ('2015007', 'Bicho do Mato')



CREATE TABLE IF NOT EXISTS talunopessoas (
   id int(10) unsigned NOT NULL AUTO_INCREMENT,
   ra char(10) NOT NULL DEFAULT '0',
   codPessoa CHAR(07) NOT NULL DEFAULT '0',
   idSys char(40) NOT NULL DEFAULT '#',

   nome CHAR(70) NOT NULL DEFAULT '#',
   sexo CHAR(03) NOT NULL DEFAULT 'M',
   dataNasciment CHAR(25) NOT NULL DEFAULT '0',
   nacionalidade CHAR(03) NOT NULL DEFAULT '#',
   naturalidade CHAR(25) NOT NULL DEFAULT '#',
   estadoNatal CHAR(03) NOT NULL DEFAULT '#',
-- cidadeNatal CHAR(25) NOT NULL DEFAULT '#',
   estadoCivil CHAR(03) NOT NULL DEFAULT '#',
   tipoAluno CHAR(03) NOT NULL DEFAULT '#',

   endRua CHAR(40) NOT NULL DEFAULT '#',
   endNumero CHAR(10) NOT NULL DEFAULT '#',
   endComplement CHAR(30) NOT NULL DEFAULT '#',
   endCEP CHAR(10) NOT NULL DEFAULT '#',
   endBairro CHAR(25) NOT NULL DEFAULT '#',
   endCidade CHAR(25) NOT NULL DEFAULT '#',
   endEstado CHAR(03) NOT NULL DEFAULT '#',

   contEMail CHAR(50) NOT NULL default '#',
   contCelular CHAR(15) NOT NULL DEFAULT '#',
   contTelResidenc CHAR(15) NOT NULL DEFAULT '#',

   docIdent_Numero CHAR(15) NOT NULL DEFAULT '0',
   docIdent_orgEmissor CHAR(25) NOT NULL DEFAULT '#',
   docIdent_dataEmissao CHAR(25) NOT NULL DEFAULT '0',
   docIdent_estEmissor CHAR(03) NOT NULL DEFAULT '#',
   docCPF CHAR(15) NOT NULL DEFAULT '0',

   docCert_Tipo CHAR(02) NOT NULL DEFAULT '#',
   docCert_Numero CHAR(35) NOT NULL DEFAULT '0',
   docCert_Cartorio CHAR(50) NOT NULL DEFAULT '#',
   docCert_Comarca CHAR(35) NOT NULL DEFAULT '#',
   docCert_Data CHAR(25) NOT NULL DEFAULT '0',
   docCert_Destrito CHAR(25) NOT NULL DEFAULT '#',
   docCert_UF CHAR(03) NOT NULL DEFAULT '#',
   docCert_Folha CHAR(10) NOT NULL DEFAULT '0',
   docCert_Livro CHAR(10) NOT NULL DEFAULT '0',
   docCert_NServent CHAR(10) NOT NULL DEFAULT '0',

   flagFoto CHAR(01) NOT NULL DEFAULT '0',
   flagProcess CHAR(01) NOT NULL DEFAULT '0',
   dataUpDate CHAR(25) NOT NULL DEFAULT '0',
   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


   -- _  __  _  __.-  --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=--  -.__  _  __  _ --



CREATE TABLE IF NOT EXISTS sirmaos (
   id int(10) unsigned NOT NULL AUTO_INCREMENT,

   idAluno int(10) unsigned NOT NULL DEFAULT '0',
   nome CHAR(70) NOT NULL DEFAULT '#', 
   flagMopi CHAR(03) NOT NULL DEFAULT '0',

   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS tirmaos (
   id int(10) unsigned NOT NULL AUTO_INCREMENT,

   idAluno int(10) unsigned NOT NULL DEFAULT '0',
   nome CHAR(70) NOT NULL DEFAULT '#', 
   flagMopi CHAR(03) NOT NULL DEFAULT '0',

   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



   -- _  __  _  __.-  --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=--  -.__  _  __  _ --



CREATE TABLE IF NOT EXISTS tresponsavelpessoas (
   id int(10) unsigned NOT NULL AUTO_INCREMENT,
   ra char(10) NOT NULL DEFAULT '0',
   codPessoa CHAR(07) NOT NULL DEFAULT '0', 
   codCFO CHAR(07) NOT NULL DEFAULT '0',

   nome CHAR(70) NOT NULL DEFAULT '#', 
   sexo CHAR(03) NOT NULL DEFAULT 'M', 
-- dataNasciment CHAR(25) NOT NULL DEFAULT '0', 
   nacionalidade CHAR(03) NOT NULL DEFAULT '#', 
-- estadoNatal CHAR(03) NOT NULL DEFAULT '#', 
-- cidadeNatal CHAR(25) NOT NULL DEFAULT '#',
   estadoCivil CHAR(03) NOT NULL DEFAULT '#', 
-- tipoRespons CHAR(15) NOT NULL DEFAULT '0', 
   flagAcademico CHAR(01) NOT NULL DEFAULT '0', 
   flagFinanceiro CHAR(01) NOT NULL DEFAULT '0', 
   grauParent CHAR(03) NOT NULL DEFAULT '0', 

   endRua CHAR(40) NOT NULL DEFAULT '#', 
   endNumero CHAR(10) NOT NULL DEFAULT '#', 
   endComplement CHAR(30) NOT NULL DEFAULT '#', 
   endCEP CHAR(10) NOT NULL DEFAULT '#', 
   endBairro CHAR(25) NOT NULL DEFAULT '#', 
   endCidade CHAR(25) NOT NULL DEFAULT '#', 
   endEstado CHAR(03) NOT NULL DEFAULT '#', 

   contEMail CHAR(50) NOT NULL default '#', 
   contCelular CHAR(15) NOT NULL DEFAULT '#', 
   contTelResidenc CHAR(15) NOT NULL DEFAULT '#', 

   docIdent_Numero CHAR(15) NOT NULL DEFAULT '0', 
   docIdent_orgEmissor CHAR(25) NOT NULL DEFAULT '#',
   docIdent_dataEmissao CHAR(25) NOT NULL DEFAULT '0', 
   docIdent_estEmissor CHAR(03) NOT NULL DEFAULT '#', 
   docCPF CHAR(15) NOT NULL DEFAULT '0', 

   profissao CHAR(05) NOT NULL DEFAULT '0', 
   profCargo CHAR(05) NOT NULL DEFAULT '0', 
   profEmpresa CHAR(25) NOT NULL DEFAULT '0', 

   alunoID CHAR(40) NOT NULL DEFAULT '0',

   flagProcess CHAR(01) NOT NULL DEFAULT '0',
   dataUpDate CHAR(40) NOT NULL DEFAULT '0', -- Data de Atualizaçao do Registro
   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE IF NOT EXISTS sresponsavelpessoas (
   id int(10) unsigned NOT NULL AUTO_INCREMENT,
   ra char(10) NOT NULL DEFAULT '0',
   codPessoa CHAR(07) NOT NULL DEFAULT '0', 
   codCFO CHAR(07) NOT NULL DEFAULT '0',

   nome CHAR(70) NOT NULL DEFAULT '#', 
   sexo CHAR(03) NOT NULL DEFAULT 'M', 
-- dataNasciment CHAR(25) NOT NULL DEFAULT '0', 
   nacionalidade CHAR(03) NOT NULL DEFAULT '#', 
-- estadoNatal CHAR(03) NOT NULL DEFAULT '#', 
-- cidadeNatal CHAR(25) NOT NULL DEFAULT '#',
   estadoCivil CHAR(03) NOT NULL DEFAULT '#', 
-- tipoRespons CHAR(15) NOT NULL DEFAULT '0', 
   flagAcademico CHAR(01) NOT NULL DEFAULT '0', 
   flagFinanceiro CHAR(01) NOT NULL DEFAULT '0', 
   grauParent CHAR(03) NOT NULL DEFAULT '0', 

   endRua CHAR(40) NOT NULL DEFAULT '#', 
   endNumero CHAR(10) NOT NULL DEFAULT '#', 
   endComplement CHAR(30) NOT NULL DEFAULT '#', 
   endCEP CHAR(10) NOT NULL DEFAULT '#', 
   endBairro CHAR(25) NOT NULL DEFAULT '#', 
   endCidade CHAR(25) NOT NULL DEFAULT '#', 
   endEstado CHAR(03) NOT NULL DEFAULT '#', 

   contEMail CHAR(50) NOT NULL default '#', 
   contCelular CHAR(15) NOT NULL DEFAULT '#', 
   contTelResidenc CHAR(15) NOT NULL DEFAULT '#', 

   docIdent_Numero CHAR(15) NOT NULL DEFAULT '0', 
   docIdent_orgEmissor CHAR(25) NOT NULL DEFAULT '#',
   docIdent_dataEmissao CHAR(25) NOT NULL DEFAULT '0', 
   docIdent_estEmissor CHAR(03) NOT NULL DEFAULT '#', 
   docCPF CHAR(15) NOT NULL DEFAULT '0', 

   profissao CHAR(05) NOT NULL DEFAULT '0', 
   profCargo CHAR(05) NOT NULL DEFAULT '0', 
   profEmpresa CHAR(25) NOT NULL DEFAULT '0', 

   alunoID CHAR(40) NOT NULL DEFAULT '0',

   flagProcess CHAR(01) NOT NULL DEFAULT '0',
   dataUpDate CHAR(40) NOT NULL DEFAULT '0', -- Data de Atualizaçao do Registro
   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


   -- _  __  _  __.-  --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=--  -.__  _  __  _ --


CREATE TABLE IF NOT EXISTS tprofessorpessoas (
   id int(10) unsigned NOT NULL AUTO_INCREMENT,
   codProf char(07) NOT NULL DEFAULT '0',
   codPessoa CHAR(07) NOT NULL DEFAULT '0', 

-- tipo CHAR(15) NOT NULL DEFAULT '0', 
-- funcao CHAR(03) NOT NULL DEFAULT '0', 

   nome CHAR(70) NOT NULL DEFAULT '#', 
   sexo CHAR(03) NOT NULL DEFAULT 'M', 
   dataNasciment CHAR(25) NOT NULL DEFAULT '0', 
   nacionalidade CHAR(03) NOT NULL DEFAULT '#', 
   estadoNatal CHAR(03) NOT NULL DEFAULT '#', 
   estadoCivil CHAR(03) NOT NULL DEFAULT '#', 

   endRua CHAR(40) NOT NULL DEFAULT '#', 
   endNumero CHAR(10) NOT NULL DEFAULT '#', 
   endComplement CHAR(30) NOT NULL DEFAULT '#', 
   endCEP CHAR(10) NOT NULL DEFAULT '#', 
   endBairro CHAR(25) NOT NULL DEFAULT '#', 
   endCidade CHAR(25) NOT NULL DEFAULT '#', 
   endEstado CHAR(03) NOT NULL DEFAULT '#', 

   contEMail CHAR(50) NOT NULL default '#', 
   contCelular CHAR(15) NOT NULL DEFAULT '#', 
   contTelResidenc CHAR(15) NOT NULL DEFAULT '#', 

   docIdent_Numero CHAR(15) NOT NULL DEFAULT '0', 
   docIdent_orgEmissor CHAR(25) NOT NULL DEFAULT '#',
   docIdent_dataEmissao CHAR(25) NOT NULL DEFAULT '0', 
   docEstadEmissor CHAR(03) NOT NULL DEFAULT '#', 
   docCPF CHAR(15) NOT NULL DEFAULT '0', 

   flagProcess CHAR(01) NOT NULL DEFAULT '0',
   dataUpDate CHAR(40) NOT NULL DEFAULT '0', -- Data de Atualizaçao do Registro
   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS sprofessorpessoas (
   id int(10) unsigned NOT NULL AUTO_INCREMENT,
   codProf char(07) NOT NULL DEFAULT '0',
   codPessoa CHAR(07) NOT NULL DEFAULT '0', 

-- tipo CHAR(15) NOT NULL DEFAULT '0', 
-- funcao CHAR(03) NOT NULL DEFAULT '0', 

   nome CHAR(70) NOT NULL DEFAULT '#', 
   sexo CHAR(03) NOT NULL DEFAULT 'M', 
   dataNasciment CHAR(25) NOT NULL DEFAULT '0', 
   nacionalidade CHAR(03) NOT NULL DEFAULT '#', 
   estadoNatal CHAR(03) NOT NULL DEFAULT '#', 
   estadoCivil CHAR(03) NOT NULL DEFAULT '#', 

   endRua CHAR(40) NOT NULL DEFAULT '#', 
   endNumero CHAR(10) NOT NULL DEFAULT '#', 
   endComplement CHAR(30) NOT NULL DEFAULT '#', 
   endCEP CHAR(10) NOT NULL DEFAULT '#', 
   endBairro CHAR(25) NOT NULL DEFAULT '#', 
   endCidade CHAR(25) NOT NULL DEFAULT '#', 
   endEstado CHAR(03) NOT NULL DEFAULT '#', 

   contEMail CHAR(50) NOT NULL default '#', 
   contCelular CHAR(15) NOT NULL DEFAULT '#', 
   contTelResidenc CHAR(15) NOT NULL DEFAULT '#', 

   docIdent_Numero CHAR(15) NOT NULL DEFAULT '0', 
   docIdent_orgEmissor CHAR(25) NOT NULL DEFAULT '#',
   docIdent_dataEmissao CHAR(25) NOT NULL DEFAULT '0', 
   docEstadEmissor CHAR(03) NOT NULL DEFAULT '#', 
   docCPF CHAR(15) NOT NULL DEFAULT '0', 

   flagProcess CHAR(01) NOT NULL DEFAULT '0',
   dataUpDate CHAR(40) NOT NULL DEFAULT '0', -- Data de Atualizaçao do Registro
   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


   -- _  __  _  __.-  --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=--  -.__  _  __  _ --


CREATE TABLE IF NOT EXISTS tdisciplinas (
   id int(10) unsigned NOT NULL AUTO_INCREMENT,

   codDisc char(05) NOT NULL DEFAULT '0',
   anoLetivo char(05) NOT NULL DEFAULT '#',
   codProfessor CHAR(05) NOT NULL default '0',
   nome CHAR(70) NOT NULL DEFAULT '#', 

   dataUpDate CHAR(25) NOT NULL DEFAULT '0', -- Data de Atualizaçao do Registro
   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


   -- _  __  _  __.-  --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=--  -.__  _  __  _ --


CREATE TABLE IF NOT EXISTS snotas (
   id int(10) unsigned NOT NULL AUTO_INCREMENT,
   ra char(10) NOT NULL DEFAULT '0',

   etapa char(03) NOT NULL DEFAULT '#',
   descricao char(35) NOT NULL DEFAULT '#',
   anoLetivo char(05) NOT NULL DEFAULT '#',
   codDisc CHAR(05) NOT NULL default '0',
   nota CHAR(10) NOT NULL DEFAULT '#', 

   dataUpDate CHAR(25) NOT NULL DEFAULT '0', -- Data de Atualizaçao do Registro
   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS tnotasfaltas (
   id int(10) unsigned NOT NULL AUTO_INCREMENT,
   ra char(10) NOT NULL DEFAULT '0',

   codEtapa char(03) NOT NULL DEFAULT '#',
   descricao char(35) NOT NULL DEFAULT '#',
   anoLetivo char(05) NOT NULL DEFAULT '#',
   codDisc CHAR(05) NOT NULL default '0',
   tipoEtapa char(01) NOT NULL DEFAULT '#',
   notaFalta CHAR(10) NOT NULL DEFAULT '#', 

   flagProcess CHAR(01) NOT NULL DEFAULT '0',
   dataUpDate CHAR(25) NOT NULL DEFAULT '0', -- Data de Atualizaçao do Registro
   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


   -- _  __  _  __.-  --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=--  -.__  _  __  _ --


CREATE TABLE IF NOT EXISTS talunoacademicos (
   id int(10) unsigned NOT NULL AUTO_INCREMENT,
   ra char(40) NOT NULL DEFAULT '0',
   unidade char(03) NOT NULL DEFAULT '#',

   tipoMatricula CHAR(03) NOT NULL DEFAULT '#',
   tipoAluno CHAR(03) NOT NULL default '#',

   anoLetivo char(05) NOT NULL DEFAULT '#',

   codCurso char(03) NOT NULL DEFAULT '#',
   codSerie char(03) NOT NULL DEFAULT '#',
   turma char(30) NOT NULL DEFAULT '#',
   turno char(15) NOT NULL DEFAULT '#',

   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS salunoacademicos (
   id int(10) unsigned NOT NULL AUTO_INCREMENT,
   ra char(40) NOT NULL DEFAULT '0',
   unidade char(03) NOT NULL DEFAULT '#',

   tipoMatricula CHAR(03) NOT NULL DEFAULT '#',
   tipoAluno CHAR(03) NOT NULL default '#',

   anoLetivo char(05) NOT NULL DEFAULT '#',

   codCurso char(03) NOT NULL DEFAULT '#',
   codSerie char(03) NOT NULL DEFAULT '#',
   turma char(30) NOT NULL DEFAULT '#',
   turno char(15) NOT NULL DEFAULT '#',

   PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
