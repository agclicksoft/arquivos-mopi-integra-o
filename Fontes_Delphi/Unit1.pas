unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls, WideStrings, DBXMySql, SqlExpr, FMTBcd,
  ZAbstractRODataset, ZAbstractDataset, ZDataset, ZAbstractConnection,
  ZConnection, ExtCtrls, Menus, Mask, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdHTTP;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    ZConnection1: TZConnection;
    ZQuery1: TZQuery;
    ADOQuery1: TADOQuery;
    Button2: TButton;
    Button3: TButton;
    TrayIcon1: TTrayIcon;
    MainMenu1: TMainMenu;
    Iniciar1: TMenuItem;
    Finalizar1: TMenuItem;
    Timer1: TTimer;
    IdHTTP1: TIdHTTP;
    Ocultar1: TMenuItem;
    RadioButton_ST: TRadioButton;
    _TS: TRadioButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TrayIcon1DblClick(Sender: TObject);
    procedure Iniciar1Click(Sender: TObject);
    procedure Finalizar1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Ocultar1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    xStr_infHora, xStr_Data, xStr_upData, xStr_insData: string;
    xStr_SQL, xStr_URL, xStr_codUltimat, xStr_RA: string;
    procedure xProc_stAluno;
    procedure xProc_tsAluno;
    procedure xProc_stRespons;
    procedure xProc_tsRespons;
    procedure xProc_stProfessor;
    procedure xProc_tsProfessor;
    procedure xProc_stNotaFalta;
    procedure xProc_tsNotaFalta;
    function xFunc_atualizacSPendent(prmInfor: string): Boolean;
    function xFunc_atualizacTPendent(prmInfor: string): Boolean;
    function xFunc_gerarRA(prmInfor: string): String;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses Func_1R;

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
   Memo1.Lines.Clear;
   if (RadioButton_ST.Checked) then
      xProc_stProfessor
   else
      xProc_tsProfessor;
end;


procedure TForm1.Button2Click(Sender: TObject);
begin
   Memo1.Lines.Clear;
   if (RadioButton_ST.Checked) then
      xProc_stAluno
   else
      xProc_tsAluno;
end;


procedure TForm1.Button3Click(Sender: TObject);
begin
   Memo1.Lines.Clear;
   if (RadioButton_ST.Checked) then
      xProc_stRespons
   else
      xProc_tsRespons;
end;


procedure TForm1.Finalizar1Click(Sender: TObject);
begin
   Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + '- Finalizar...';
   Timer1.Enabled := False;
   Timer1.Interval := 5000;

   if (ZConnection1.Connected) then
      ZConnection1.Disconnect;
end;


procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   if not (Finalizar1.Enabled) then
   begin
      ShowMessage( 'Processo em andamento... por favor aguarde!' );
      Action := caNone;
      Exit;
   end;

   if (Application.MessageBox('Deseja finalizar o sistema?', '[module web]', 36) = ID_NO) then
   begin
      Action := caNone;
      Exit;
   end
   else
   begin
      if ( ZConnection1.Connected ) then
      begin
         ZQuery1.Close;
         ZConnection1.Disconnect;
      end
   end;
end;


procedure TForm1.Iniciar1Click(Sender: TObject);
begin
   Memo1.Lines.Text := #13#13 + '- Iniciar...';
   try
      if not (ZConnection1.Connected) then
         ZConnection1.Connect;
   except
// except on E: Exception do
  //  ShowMessage( E.ToString );
   end;

   Timer1.Enabled := True;
end;


procedure TForm1.Ocultar1Click(Sender: TObject);
begin
   Self.Hide
end;


procedure TForm1.Timer1Timer(Sender: TObject);
begin
   if (Timer1.Interval = 5000) then
      Timer1.Interval := 300000;
   // Timer1.Interval := 150000;

   xStr_infHora := FormatDateTime('hh:nn:ss', Now());
   Memo1.Lines.Clear;

   Finalizar1.Enabled := False;
   Timer1.Enabled := False;

   try
      xStr_URL := 'http://clicksoft.com.br/mopi/int/upprofes_st.php';
      Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + 'curl > '+ xStr_URL;
      IdHTTP1.Get( xStr_URL );

      xStr_URL := 'http://clicksoft.com.br/mopi/int/upaluno_st.php';
      Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + 'curl > '+ xStr_URL;
      IdHTTP1.Get( xStr_URL );

      xStr_URL := 'http://clicksoft.com.br/mopi/int/uprespons_st.php';
      Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + 'curl > '+ xStr_URL;
      IdHTTP1.Get( xStr_URL );

      xStr_URL := 'http://clicksoft.com.br/mopi/int/upnotafalta_st.php';
      Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + 'curl > '+ xStr_URL;
      IdHTTP1.Get( xStr_URL );
   except
// except on E: Exception do
  //  ShowMessage( E.ToString );
   end;


   // - - - P R O F E S S O R - - - \\
   if (xFunc_atualizacSPendent( 'Professor_ST' )) then
      xProc_stProfessor
   else
      Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_infHora + ' - Aguardando atualizaçoes... professor.';

   if not (xFunc_atualizacTPendent( 'Professor_TS' )) then
      xProc_tsProfessor
   else
      Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_infHora + ' - Aguardando atualizaçoes... professor.';


   // - - - A L U N O - - - \\
   if (xFunc_atualizacSPendent( 'Alunos_ST' )) then
      xProc_stAluno
   else
      Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_infHora + ' - Aguardando atualizaçoes... aluno.';

   if not (xFunc_atualizacTPendent( 'Alunos_TS' )) then
      xProc_tsAluno
   else
      Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_infHora + ' - Aguardando atualizaçoes... aluno.';


   // - - - R E S P O N S A V E L - - - \\
   if (xFunc_atualizacSPendent( 'Respons_ST' )) then
      xProc_stRespons
   else
      Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_infHora + ' - Aguardando atualizaçoes... responsavel.';

   if not (xFunc_atualizacTPendent( 'Respons_TS' )) then
      xProc_tsRespons
   else
      Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_infHora + ' - Aguardando atualizaçoes... responsavel.';


   // - - - N O T A x F A L T A - - - \\
   if (xFunc_atualizacSPendent( 'NotaFalta_ST' )) then
      xProc_stNotaFalta
   else
      Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_infHora + ' - Aguardando atualizaçoes... notas e faltas.';

   if not (xFunc_atualizacTPendent( 'NotaFalta_TS' )) then
      xProc_tsNotaFalta
   else
      Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_infHora + ' - Aguardando atualizaçoes... notas e faltas.';


   try
      xStr_URL := 'http://clicksoft.com.br/mopi/int/upprofes_ts.php';
      Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + 'curl > '+ xStr_URL;
      IdHTTP1.Get( xStr_URL );

      xStr_URL := 'http://clicksoft.com.br/mopi/int/upaluno_ts.php';
      Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + 'curl > '+ xStr_URL;
      IdHTTP1.Get( xStr_URL );

      xStr_URL := 'http://clicksoft.com.br/mopi/int/uprespons_ts.php';
      Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + 'curl > '+ xStr_URL;
      IdHTTP1.Get( xStr_URL );

      xStr_URL := 'http://clicksoft.com.br/mopi/int/upnotafalta_ts.php';
      Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + 'curl > '+ xStr_URL;
      IdHTTP1.Get( xStr_URL );

      Timer1.Enabled := True;
   except
// except on E: Exception do
  //  ShowMessage( E.ToString );
   end;

   Finalizar1.Enabled := True;
end;


procedure TForm1.TrayIcon1DblClick(Sender: TObject);
begin
   if (Self.Visible) then
      Self.Hide
   else
      Self.Show;
end;

   // _  __  _  __.- - --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=-- - -.__  _  __  _ \\


function TForm1.xFunc_atualizacSPendent(prmInfor: string): Boolean;
begin
   with ZQuery1 do
   begin
      Close;
      SQL.Clear;
      SQL.Add( 'SELECT * FROM sysinfor WHERE infor_I = "'+ prmInfor +'"' );
      Open;

      if (FieldByName('inFlag').AsString = '1') then
         Result := True
      else
         Result := False;
   end;
end;


function TForm1.xFunc_atualizacTPendent(prmInfor: string): Boolean;
begin
   with ZQuery1 do
   begin
      Close;
      SQL.Clear;
      SQL.Add( 'SELECT * FROM sysinfor WHERE infor_I = "'+ prmInfor +'"' );
      Open;

      if (FieldByName('inFlag').AsString = '1') then
         Result := True
      else
         Result := False;
   end;
end;


function TForm1.xFunc_gerarRA(prmInfor: string): String;
begin
   xStr_SQL :=
        'UPDATE Corpore_Clicksoft.dbo.GAUTOINC' + #13
      + '   SET VALAUTOINC = (VALAUTOINC + 1)' + #13
      + ' WHERE CODAUTOINC = ''RA''' + #13
      + '   AND CODCOLIGADA = ''0''' + #13
      + '   AND CODSISTEMA = ''S''';
// Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;
   ADOQuery1.SQL.Clear;
   ADOQuery1.SQL.Add( xStr_SQL );
   ADOQuery1.ExecSQL;

   xStr_SQL :=
        'SELECT subString( SP.TEXTO, 1, 2 ) + ''0'' + CONVERT(CHAR(10), GI.VALAUTOINC) AS xRA' + #13
      + '  FROM Corpore_Clicksoft.dbo.SPARAM SP, Corpore_Clicksoft.dbo.GAUTOINC GI' + #13
      + ' WHERE SP.ID = ''MASCARAMATRICULA''' + #13
      + '   AND SP.CODFILIAL = ''1''' + #13
      + '   AND GI.CODAUTOINC = ''RA''' + #13
      + '   AND GI.CODCOLIGADA = ''0''' + #13
      + '   AND GI.CODSISTEMA = ''S''';
// Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;
   ADOQuery1.SQL.Clear;
   ADOQuery1.SQL.Add( xStr_SQL );
   ADOQuery1.Open;

   Result := Trim( ADOQuery1.FieldByName('xRA').AsString );
end;


procedure TForm1.xProc_stAluno;
begin
   try
      ZQuery1.Close;
      ZQuery1.SQL.Clear;
      ZQuery1.SQL.Add( 'SELECT * FROM salunopessoas' );
      ZQuery1.Open;

      if (ZQuery1.RecordCount > 0) then
      begin
         ZQuery1.First;
         with ZQuery1 do
         begin
            while (NOT ZQuery1.Eof) do
            begin
               if (FieldByName('flagProcess').AsString = '1') then
               begin
                  xStr_Data := Copy( FieldByName('dataUpDate').AsString, 01, 19 ) + '.000';
                  xStr_Data := StringReplace( xStr_Data, 'T', ' ', [rfReplaceAll, rfIgnoreCase] );

                  xStr_SQL :=
                   'INSERT INTO Corpore_Clicksoft.dbo.PPESSOA' + #13
                  + '   (codigo, nome, sexo, dtnascimento, nacionalidade, estadonatal, estadocivil, rua, numero, complemento, bairro, cep, email, telefone2, telefone1, cartidentidade, dtemissaoident, orgemissorident, ufcartident,' + #13
                  + '    aluno, professor, usuariobiblios, funcionario, exfuncionario, candidato, reccreatedby, reccreatedon, recmodifiedby, recmodifiedon)' + #13
                  + 'VALUES' + #13
                  + ' ( ( SELECT (MAX( codigo ) + 1) FROM Corpore_Clicksoft.dbo.PPESSOA )'
                  + ', '+ QuotedStr( UTF8Decode( FieldByName('nome').AsString ) ) +', '+ QuotedStr( FieldByName('sexo').AsString )
                  + ', '+ funcRIf(FieldByName('dataNasciment').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('dataNasciment').AsString ))
                  + ', '+ funcRIf(FieldByName('nacionalidade').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('nacionalidade').AsString ))
                  + ', '+ funcRIf(FieldByName('estadoNatal').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('estadoNatal').AsString ))
                  + ', '+ funcRIf(FieldByName('estadoCivil').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('estadoCivil').AsString ))
                  + ', '+ funcRIf(FieldByName('endRua').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endRua').AsString ))
                  + ', '+ funcRIf(FieldByName('endNumero').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endNumero').AsString ))
                  + ', '+ funcRIf(FieldByName('endComplement').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endComplement').AsString ))
                  + ', '+ funcRIf(FieldByName('endBairro').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endBairro').AsString ))
                  + ', '+ funcRIf(FieldByName('endCEP').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endCEP').AsString ))
                  + ', '+ funcRIf(FieldByName('contEMail').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('contEMail').AsString ))
                  + ', '+ funcRIf(FieldByName('contCelular').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('contCelular').AsString ))
                  + ', '+ funcRIf(FieldByName('contTelResidenc').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('contTelResidenc').AsString ))
                  + ', '+ funcRIf(FieldByName('docIdentNumero').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docIdentNumero').AsString ))
                  + ', '+ funcRIf(FieldByName('docIdent_dataEmissao').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docIdent_dataEmissao').AsString ))
                  + ', '+ funcRIf(FieldByName('docIdent_orgEmissor').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docIdent_orgEmissor').AsString ))
                  + ', '+ funcRIf(FieldByName('docEstadEmissor').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docEstadEmissor').AsString )) + #13
                  + ', ''0'', ''0'', ''0'', ''0'', ''0'', ''0'', ''luciano.gael'', GETDATE(), ''luciano.gael'', GETDATE() )';
                  Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;
                  ADOQuery1.SQL.Clear;
                  ADOQuery1.SQL.Add( xStr_SQL );
                  ADOQuery1.ExecSQL;

                  xStr_SQL := 'SELECT MAX( codigo ) codUltimat FROM Corpore_Clicksoft.dbo.PPESSOA';
                  Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;
                  ADOQuery1.SQL.Clear;
                  ADOQuery1.SQL.Add( xStr_SQL );
                  ADOQuery1.Open;
                  xStr_codUltimat := ADOQuery1.FieldByName('codUltimat').AsString;

                  xStr_SQL :=
                    'INSERT INTO Corpore_Clicksoft.dbo.SPESSOA' + #13
                  + '   (codigo, reccreatedby, reccreatedon, recmodifiedby, recmodifiedon)' + #13
                  + 'VALUES' + #13
                  + '   ('+ QuotedStr( xStr_codUltimat ) +', ''luciano.gael'', GETDATE(), ''luciano.gael'', GETDATE())' + #13;
                  Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;
                  ADOQuery1.SQL.Clear;
                  ADOQuery1.SQL.Add( xStr_SQL );
                  ADOQuery1.ExecSQL;

                  xStr_RA := xFunc_gerarRA( FieldByName('ra').AsString );

                  xStr_SQL :=
                    'INSERT INTO Corpore_Clicksoft.dbo.SALUNO' + #13
                  + '   (codcoligada, ra, codtipoaluno, codtipocurso, codpessoa, reccreatedby, reccreatedon, recmodifiedby, recmodifiedon)' + #13
                  + 'VALUES' + #13
                  + '   (''1'', '+ QuotedStr( xStr_RA ) +', '+ QuotedStr( FieldByName('tipoAluno').AsString ) +', ''1'', '+ QuotedStr( xStr_codUltimat ) +', ''luciano.gael'', GETDATE(), ''luciano.gael'', GETDATE())' + #13;
                  Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;
                  ADOQuery1.SQL.Clear;
                  ADOQuery1.SQL.Add( xStr_SQL );
                  ADOQuery1.ExecSQL;

                  xStr_URL := 'http://clicksoft.com.br/mopi/int/setcod_aluno.php?id='+ FieldByName('dataUpDate').AsString +'&codpessoa='+ xStr_codUltimat +'&ra='+ xStr_RA;
                  Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + 'curl > '+ xStr_URL;
                  IdHTTP1.Get( xStr_URL );
               end
               else
               begin
                  xStr_Data := Copy( FieldByName('dataUpDate').AsString, 01, 19 ) + '.000';
                  xStr_Data := StringReplace( xStr_Data, 'T', ' ', [rfReplaceAll, rfIgnoreCase] );

                  xStr_SQL :=
                    'UPDATE Corpore_Clicksoft.dbo.PPESSOA' + #13
                  + '   SET nome = '+ QuotedStr( UTF8Decode( FieldByName('nome').AsString ) ) +', ' + #13
                  + '       sexo = '+ funcRIf(FieldByName('sexo').AsString = '-vazio-', 'NULL', QuotedStr( UTF8Decode( FieldByName('sexo').AsString ) )) +', ' + #13
                  + '       nacionalidade = '+ funcRIf(FieldByName('nacionalidade').AsString = '-vazio-', 'NULL', QuotedStr( UTF8Decode( FieldByName('nacionalidade').AsString ) )) +', ' + #13
                  + '       estadoNatal = '+ funcRIf(FieldByName('estadoNatal').AsString = '-vazio-', 'NULL', QuotedStr( UTF8Decode( FieldByName('estadoNatal').AsString ) )) +', ' + #13
                  + '       estadocivil = '+ funcRIf(FieldByName('estadoCivil').AsString = '-vazio-', 'NULL', QuotedStr( UTF8Decode( FieldByName('estadoCivil').AsString ) )) +', ' + #13
                  + '       rua = '+ funcRIf(FieldByName('endRua').AsString = '-vazio-', 'NULL', QuotedStr( UTF8Decode( FieldByName('endRua').AsString ) )) +', ' + #13
                  + '       numero = '+ funcRIf(FieldByName('endNumero').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endNumero').AsString )) +', ' + #13
                  + '       complemento = '+ funcRIf(FieldByName('endComplement').AsString = '-vazio-', 'NULL', QuotedStr( UTF8Decode( FieldByName('endComplement').AsString ) )) +', ' + #13
                  + '       cep = '+ funcRIf(FieldByName('endCEP').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endCEP').AsString )) +', ' + #13
                  + '       bairro = '+ funcRIf(FieldByName('endBairro').AsString = '-vazio-', 'NULL', QuotedStr( UTF8Decode( FieldByName('endBairro').AsString ) )) +', ' + #13
               // + '       cidade = '+ funcRIf(FieldByName('endCidade').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endCidade').AsString )) +', ' + #13
               // + '       estado = '+ funcRIf(FieldByName('endEstado').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endEstado').AsString )) +', ' + #13
                  + '       email = '+ funcRIf(FieldByName('contEMail').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('contEMail').AsString )) +', ' + #13
                  + '       telefone2 = '+ funcRIf(FieldByName('contCelular').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('contCelular').AsString )) +', ' + #13
                  + '       telefone1 = '+ funcRIf(FieldByName('contTelResidenc').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('contTelResidenc').AsString )) +', ' + #13
                  + '       cartidentidade = '+ funcRIf(FieldByName('docIdentNumero').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docIdentNumero').AsString )) +', ' + #13
                  + '       dtemissaoident = '+ funcRIf(FieldByName('docIdent_dataEmissao').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docIdent_dataEmissao').AsString )) +', ' + #13
                  + '       orgemissorident = '+ funcRIf(FieldByName('docIdent_orgEmissor').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docIdent_orgEmissor').AsString )) +', ' + #13
                  + '       ufcartident = '+ funcRIf(FieldByName('docEstadEmissor').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docEstadEmissor').AsString )) + #13
                  + ' WHERE CODIGO = ' + FieldByName('codPessoa').AsString + #13
                  + '   AND (CONVERT(DateTime, '+ QuotedStr( xStr_Data ) +') > RECMODIFIEDON OR RECMODIFIEDON IS NULL)';
                  Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;

                  ADOQuery1.SQL.Clear;
                  ADOQuery1.SQL.Add( xStr_SQL );
                  ADOQuery1.ExecSQL;
               end;

               ZQuery1.Next;
            end;
         end;

         xStr_upData := FormatDateTime('YYYY-MM-DD~hh:nn:ss', Now());
         ZQuery1.SQL.Clear;
         ZQuery1.SQL.Add( 'UPDATE sysinfor SET infor_II = '+ QuotedStr( xStr_upData ) +' WHERE infor_I = "Alunos_TS"' );
         ZQuery1.ExecSQL;

         if not (Button1.Visible) then
         begin
            ZQuery1.Close;
            ZQuery1.SQL.Clear;
            ZQuery1.SQL.Add( 'DELETE FROM salunopessoas' );
            ZQuery1.ExecSQL;
         end;

         ZQuery1.SQL.Clear;
         ZQuery1.SQL.Add( 'UPDATE sysinfor SET infor_II = '+ QuotedStr( xStr_upData ) +', inFlag = "0" WHERE infor_I = "Alunos_ST"' );
         ZQuery1.ExecSQL;
      end;
   except
// except on E: Exception do
  //  ShowMessage( E.ToString );
   end;
end;


procedure TForm1.xProc_tsAluno;
begin
   ZQuery1.Close;
   ZQuery1.SQL.Clear;
   ZQuery1.SQL.Add( 'SELECT * FROM sysinfor WHERE infor_I = "Alunos_TS"' );
   ZQuery1.Open;

   xStr_Data := StringReplace( ZQuery1.FieldByName('infor_II').AsString, '~', ' ', [rfReplaceAll, rfIgnoreCase] );
   xStr_Data := xStr_Data +'.000';

   xStr_SQL :=
     'SELECT *' + #13
   + '  FROM (' + #13
   + '   SELECT DISTINCT ALUN.ra, ALUN.codpessoa, ISNULL(ALUN.codcfo, ''0'') codpessoa_respfinanc,' + #13
   + '          ISNULL(ALUN.codparentcfo, ''0'') tipopessoa_respfinanc, ISNULL(ALUN.codpessoaraca, ''0'') codpessoa_respacad,' + #13
   + '          ISNULL(ALUN.codparentraca, ''0'') tipopessoa_respacad,' + #13
   + '          (CASE WHEN MATR.codfilial = ''4'' THEN ''itanhanga'' ELSE ''tijuca'' END) unidade,' + #13
   + '          PESS.nome, ISNULL(PESS.sexo, ''F'') sexo, ISNULL(PESS.dtnascimento, 0) datanasciment, PESS.nacionalidade,' + #13
   + '          ISNULL(PESS.estadonatal, ''XX'') estadonatal, ISNULL(PESS.estadocivil, ''X'') estadocivil,' + #13
   + '          ALUN.codtipoaluno, ' + #13
   + '     -- Enderec --' + #13
   + '          ISNULL(PESS.rua, ''-vazio-'') rua, ISNULL(PESS.numero, ''-vazio-'') numero, ISNULL(PESS.complemento, ''-vazio-'') complemento,' + #13
   + '          ISNULL(PESS.bairro, ''-vazio-'') bairro, ISNULL(PESS.cidade, ''-vazio-'') cidade, ISNULL(PESS.estado, ''-vazio-'') estado,' + #13
   + '          ISNULL(PESS.cep, ''-vazio-'') cep,' + #13
   + '     -- Contact --' + #13
   + '          ISNULL(PESS.email, ''-vazio-'') email,' + #13
   + '          ISNULL(PESS.telefone2, ''-vazio-'') celular, ISNULL(PESS.telefone1, ''-vazio-'')  telefoneResidencial,' + #13
   + '     -- Document --' + #13
   + '           ISNULL(PESS.cartidentidade, ''-vazio-'') cartidentidade, ISNULL(PESS.dtemissaoident, 0) dtemissaoident,' + #13
   + '           ISNULL(PESS.orgemissorident, ''-vazio-'') orgemissorident, ISNULL(PESS.ufcartident, ''XX'') ufcartident,' + #13
   + '           ISNULL(PESS.cpf, ''-vazio-'') cpf,' + #13
   + '     -- Control --' + #13
   + '         ''0'' flagprocess, PESS.recmodifiedon dataupdate' + #13
   + '     FROM Corpore_Clicksoft.dbo.SALUNO ALUN, Corpore_Clicksoft.dbo.PPESSOA PESS, Corpore_Clicksoft.dbo.SMATRICPL MATR' + #13
   + '    WHERE PESS.CODIGO = ALUN.CODPESSOA' + #13
   + '      AND MATR.RA = ALUN.RA' + #13
   + '      AND ALUN.RA BETWEEN ''2009231'' AND ''2009312''' + #13
   + '      AND PESS.recmodifiedon > CONVERT(DateTime, '+ QuotedStr( xStr_Data ) +')' + #13
   + #13
   + '   UNION' + #13
   + #13
   + '   SELECT DISTINCT ALUN.ra, ALUN.codpessoa, ISNULL(ALUN.codcfo, ''0'') codpessoa_respfinanc,' + #13
   + '          ISNULL(ALUN.codparentcfo, ''0'') tipopessoa_respfinanc, ISNULL(ALUN.codpessoaraca, ''0'') codpessoa_respacad,' + #13
   + '          ISNULL(ALUN.codparentraca, ''0'') tipopessoa_respacad,' + #13
   + '          ''-vazio-'' unidade,' + #13
   + '          PESS.nome, ISNULL(PESS.sexo, ''F'') sexo, ISNULL(PESS.dtnascimento, 0) datanasciment, PESS.nacionalidade,' + #13
   + '          ISNULL(PESS.estadonatal, ''XX'') estadonatal, ISNULL(PESS.estadocivil, ''X'') estadocivil,' + #13
   + '          ALUN.codtipoaluno, ' + #13
   + '     -- Enderec --' + #13
   + '          ISNULL(PESS.rua, ''-vazio-'') rua, ISNULL(PESS.numero, ''-vazio-'') numero, ISNULL(PESS.complemento, ''-vazio-'') complemento,' + #13
   + '          ISNULL(PESS.bairro, ''-vazio-'') bairro, ISNULL(PESS.cidade, ''-vazio-'') cidade, ISNULL(PESS.estado, ''-vazio-'') estado,' + #13
   + '          ISNULL(PESS.cep, ''-vazio-'') cep,' + #13
   + '     -- Contact --' + #13
   + '          ISNULL(PESS.email, ''-vazio-'') email,' + #13
   + '          ISNULL(PESS.telefone2, ''-vazio-'') celular, ISNULL(PESS.telefone1, ''-vazio-'')  telefoneResidencial,' + #13
   + '     -- Document --' + #13
   + '           ISNULL(PESS.cartidentidade, ''-vazio-'') cartidentidade, ISNULL(PESS.dtemissaoident, 0) dtemissaoident,' + #13
   + '           ISNULL(PESS.orgemissorident, ''-vazio-'') orgemissorident, ISNULL(PESS.ufcartident, ''XX'') ufcartident,' + #13
   + '           ISNULL(PESS.cpf, ''-vazio-'') cpf,' + #13
   + '     -- Control --' + #13
   + '         ''1'' flagprocess, PESS.recmodifiedon dataupdate' + #13
   + '     FROM Corpore_Clicksoft.dbo.SALUNO ALUN, Corpore_Clicksoft.dbo.PPESSOA PESS' + #13
   + '    WHERE PESS.CODIGO = ALUN.CODPESSOA' + #13
   + '      AND PESS.recmodifiedon > CONVERT(DateTime, '+ QuotedStr( xStr_Data ) +')' + #13
   + '      AND PESS.reccreatedon = PESS.recmodifiedon' + #13
   + ') subQuery' + #13
   + ' ORDER BY subQuery.dataupdate DESC';
   if (Button1.Visible) then
      Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;

   ADOQuery1.SQL.Clear;
   ADOQuery1.SQL.Add( xStr_SQL );
   ADOQuery1.Open;

   if (ADOQuery1.RecordCount > 0) then
   begin
      ADOQuery1.First;
      with ADOQuery1 do
      begin
         while (NOT ADOQuery1.Eof) do
         begin
            xStr_SQL :=
               'INSERT INTO talunopessoas' + #13
             + '   (ra, codPessoa, unidade, nome, endRua, endNumero, endComplement, endCEP, endBairro, contEMail, contCelular, contTelResidenc, ' + #13
             + '    sexo, dataNasciment, nacionalidade, estadoNatal, estadoCivil, tipoAluno, endCidade, endEstado, ' + #13
             + '    docIdentNumero, docIdent_orgEmissor, docIdent_dataEmissao, docEstadEmissor, docCPF, flagprocess, dataUpDate)' + #13
             + 'VALUES' + #13
             + '   ('+ QuotedStr( FieldByName('ra').AsString ) +', '+ QuotedStr( FieldByName('codpessoa').AsString ) +', '+ QuotedStr( FieldByName('unidade').AsString ) +', '+
                       QuotedStr( FieldByName('nome').AsString ) +', '+
                       QuotedStr( FieldByName('rua').AsString ) +', '+ QuotedStr( FieldByName('numero').AsString ) +', '+ QuotedStr( FieldByName('complemento').AsString ) +', '+
                       QuotedStr( FieldByName('cep').AsString ) +', '+ QuotedStr( FieldByName('bairro').AsString ) +', '+ QuotedStr( FieldByName('email').AsString ) +', '+
                       QuotedStr( FieldByName('celular').AsString ) +', '+ QuotedStr( FieldByName('telefoneResidencial').AsString ) +', '+
                       QuotedStr( FieldByName('sexo').AsString ) +', '+ QuotedStr( FieldByName('datanasciment').AsString ) +', '+ QuotedStr( FieldByName('nacionalidade').AsString ) +', '+
                       QuotedStr( FieldByName('estadonatal').AsString ) +', '+ QuotedStr( FieldByName('estadocivil').AsString ) +', '+ QuotedStr( FieldByName('codtipoaluno').AsString ) +', '+ QuotedStr( FieldByName('cidade').AsString ) +', '+
                       QuotedStr( FieldByName('estado').AsString ) +', '+ QuotedStr( FieldByName('cartidentidade').AsString ) +', '+ QuotedStr( FieldByName('orgemissorident').AsString ) +', '+
                       QuotedStr( FieldByName('dtemissaoident').AsString ) +', '+ QuotedStr( FieldByName('ufcartident').AsString ) +', '+ QuotedStr( FieldByName('cpf').AsString ) +', '+
                       QuotedStr( FieldByName('flagprocess').AsString ) +', '+ QuotedStr( FieldByName('dataupdate').AsString ) +')';
            Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;

            ZQuery1.SQL.Clear;
            ZQuery1.SQL.Add( xStr_SQL );
            ZQuery1.ExecSQL;

            ADOQuery1.Next;
         end;
      end;

      xStr_upData := FormatDateTime('YYYY-MM-DD~hh:nn:ss', Now());
      xStr_SQL :=
         'UPDATE sysinfor' + #13
       + '   SET infor_II = '+ QuotedStr( xStr_upData ) +', '+ #13
       + '       inFlag = "1"' + #13
       + ' WHERE infor_I = "Alunos_TS"';
      if (Button1.Visible) then
         Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;

      ZQuery1.SQL.Clear;
      ZQuery1.SQL.Add( xStr_SQL );
      ZQuery1.ExecSQL;
   end;
end;

// _  __  _  __.- - --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=-- - -.__  _  __  _ \\


procedure TForm1.xProc_stRespons;
begin
   try
      ZQuery1.Close;
      ZQuery1.SQL.Clear;
      ZQuery1.SQL.Add( 'SELECT * FROM sresponsavelpessoas' );
      ZQuery1.Open;

      if (ZQuery1.RecordCount > 0) then
      begin
         ZQuery1.First;
         with ZQuery1 do
         begin
            while (NOT ZQuery1.Eof) do
            begin
               if (FieldByName('flagProcess').AsString = '1') then
               begin
                  xStr_Data := Copy( FieldByName('dataUpDate').AsString, 01, 19 ) + '.000';
                  xStr_Data := StringReplace( xStr_Data, 'T', ' ', [rfReplaceAll, rfIgnoreCase] );

                  xStr_SQL :=
                   'INSERT INTO Corpore_Clicksoft.dbo.PPESSOA' + #13
                  + '   (codigo, nome, sexo, nacionalidade, estadonatal, estadocivil, rua, numero, complemento, bairro, cep, email, telefone2, telefone1, cartidentidade, dtemissaoident, orgemissorident, ufcartident,' + #13
                  + '    aluno, professor, usuariobiblios, funcionario, exfuncionario, candidato, reccreatedby, reccreatedon, recmodifiedby, recmodifiedon)' + #13
                  + 'VALUES' + #13
                  + ' ( ( SELECT (MAX( codigo ) + 1) FROM Corpore_Clicksoft.dbo.PPESSOA )'
                  + ', '+ QuotedStr( UTF8Decode( FieldByName('nome').AsString ) ) +', '+ QuotedStr( FieldByName('sexo').AsString )
                  + ', '+ funcRIf(FieldByName('nacionalidade').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('nacionalidade').AsString ))
                  + ', '+ funcRIf(FieldByName('estadoNatal').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('estadoNatal').AsString ))
                  + ', '+ funcRIf(FieldByName('estadoCivil').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('estadoCivil').AsString ))
                  + ', '+ funcRIf(FieldByName('endRua').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endRua').AsString ))
                  + ', '+ funcRIf(FieldByName('endNumero').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endNumero').AsString ))
                  + ', '+ funcRIf(FieldByName('endComplement').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endComplement').AsString ))
                  + ', '+ funcRIf(FieldByName('endBairro').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endBairro').AsString ))
                  + ', '+ funcRIf(FieldByName('endCEP').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endCEP').AsString ))
                  + ', '+ funcRIf(FieldByName('contEMail').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('contEMail').AsString ))
                  + ', '+ funcRIf(FieldByName('contCelular').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('contCelular').AsString ))
                  + ', '+ funcRIf(FieldByName('contTelResidenc').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('contTelResidenc').AsString ))
                  + ', '+ funcRIf(FieldByName('docIdentNumero').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docIdentNumero').AsString ))
                  + ', '+ funcRIf(FieldByName('docIdent_dataEmissao').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docIdent_dataEmissao').AsString ))
                  + ', '+ funcRIf(FieldByName('docIdent_orgEmissor').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docIdent_orgEmissor').AsString ))
                  + ', '+ funcRIf(FieldByName('docEstadEmissor').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docEstadEmissor').AsString )) + #13
                  + ', ''0'', ''0'', ''0'', ''0'', ''0'', ''0'', ''luciano.gael'', GETDATE(), ''luciano.gael'', GETDATE() )';
                  Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;
                  ADOQuery1.SQL.Clear;
                  ADOQuery1.SQL.Add( xStr_SQL );
                  ADOQuery1.ExecSQL;

                  xStr_SQL := 'SELECT MAX( codigo ) codUltimat FROM Corpore_Clicksoft.dbo.PPESSOA';
                  Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;
                  ADOQuery1.SQL.Clear;
                  ADOQuery1.SQL.Add( xStr_SQL );
                  ADOQuery1.Open;
                  xStr_codUltimat := ADOQuery1.FieldByName('codUltimat').AsString;

                  xStr_SQL :=
                    'UPDATE Corpore_Clicksoft.dbo.SALUNO' + #13
                  + '   SET codpessoaraca = '+ xStr_codUltimat + #13
                  + '       codparentraca = '+ QuotedStr( FieldByName('grauParent').AsString ) +',' + #13
                  + '       recmodifiedby = ''luciano.gael'',' + #13
                  + '       recmodifiedon = GETDATE()' + #13
                  + ' WHERE ra = '+ QuotedStr( FieldByName('ra').AsString );
                  Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;
                  ADOQuery1.SQL.Clear;
                  ADOQuery1.SQL.Add( xStr_SQL );
                  ADOQuery1.ExecSQL;

                  xStr_URL := 'http://clicksoft.com.br/mopi/int/setcod_respons.php?id='+ FieldByName('dataUpDate').AsString +'&codpessoa='+ xStr_codUltimat;
                  Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + 'curl > '+ xStr_URL;
                  IdHTTP1.Get( xStr_URL );
               end
               else
               begin
                  xStr_Data := Copy( FieldByName('dataUpDate').AsString, 01, 19 ) + '.000';
                  xStr_Data := StringReplace( xStr_Data, 'T', ' ', [rfReplaceAll, rfIgnoreCase] );

                  xStr_SQL :=
                    'UPDATE Corpore_Clicksoft.dbo.PPESSOA' + #13
                  + '   SET nome = '+ QuotedStr( UTF8Decode( FieldByName('nome').AsString ) ) +', ' + #13
                  + '       sexo = '+ funcRIf(FieldByName('sexo').AsString = '-vazio-', 'NULL', QuotedStr( UTF8Decode( FieldByName('sexo').AsString ) )) +', ' + #13
                  + '       nacionalidade = '+ funcRIf(FieldByName('nacionalidade').AsString = '-vazio-', 'NULL', QuotedStr( UTF8Decode( FieldByName('nacionalidade').AsString ) )) +', ' + #13
                  + '       estadoNatal = '+ funcRIf(FieldByName('estadoNatal').AsString = '-vazio-', 'NULL', QuotedStr( UTF8Decode( FieldByName('estadoNatal').AsString ) )) +', ' + #13
                  + '       estadocivil = '+ funcRIf(FieldByName('estadoCivil').AsString = '-vazio-', 'NULL', QuotedStr( UTF8Decode( FieldByName('estadoCivil').AsString ) )) +', ' + #13
                  + '       rua = '+ funcRIf(FieldByName('endRua').AsString = '-vazio-', 'NULL', QuotedStr( UTF8Decode( FieldByName('endRua').AsString ) )) +', ' + #13
                  + '       numero = '+ funcRIf(FieldByName('endNumero').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endNumero').AsString )) +', ' + #13
                  + '       complemento = '+ funcRIf(FieldByName('endComplement').AsString = '-vazio-', 'NULL', QuotedStr( UTF8Decode( FieldByName('endComplement').AsString ) )) +', ' + #13
                  + '       cep = '+ funcRIf(FieldByName('endCEP').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endCEP').AsString )) +', ' + #13
                  + '       bairro = '+ funcRIf(FieldByName('endBairro').AsString = '-vazio-', 'NULL', QuotedStr( UTF8Decode( FieldByName('endBairro').AsString ) )) +', ' + #13
               // + '       cidade = '+ funcRIf(FieldByName('endCidade').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endCidade').AsString )) +', ' + #13
               // + '       estado = '+ funcRIf(FieldByName('endEstado').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endEstado').AsString )) +', ' + #13
                  + '       email = '+ funcRIf(FieldByName('contEMail').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('contEMail').AsString )) +', ' + #13
                  + '       telefone2 = '+ funcRIf(FieldByName('contCelular').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('contCelular').AsString )) +', ' + #13
                  + '       telefone1 = '+ funcRIf(FieldByName('contTelResidenc').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('contTelResidenc').AsString )) +', ' + #13
                  + '       cartidentidade = '+ funcRIf(FieldByName('docIdentNumero').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docIdentNumero').AsString )) +', ' + #13
                  + '       dtemissaoident = '+ funcRIf(FieldByName('docIdent_dataEmissao').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docIdent_dataEmissao').AsString )) +', ' + #13
                  + '       orgemissorident = '+ funcRIf(FieldByName('docIdent_orgEmissor').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docIdent_orgEmissor').AsString )) +', ' + #13
                  + '       ufcartident = '+ funcRIf(FieldByName('docEstadEmissor').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docEstadEmissor').AsString )) + #13
                  + ' WHERE CODIGO = ' + FieldByName('codPessoa').AsString + #13
                  + '   AND (CONVERT(DateTime, '+ QuotedStr( xStr_Data ) +') > RECMODIFIEDON OR RECMODIFIEDON IS NULL)';
                  Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;

                  ADOQuery1.SQL.Clear;
                  ADOQuery1.SQL.Add( xStr_SQL );
                  ADOQuery1.ExecSQL;
               end;

               ZQuery1.Next;
            end;
         end;

         ZQuery1.Close;
         ZQuery1.SQL.Clear;
         ZQuery1.SQL.Add( 'SELECT * FROM sysinfor WHERE infor_I = "Respons_TS"' );
         ZQuery1.Open;

         xStr_insData := StringReplace( ZQuery1.FieldByName('infor_II').AsString, '~', ' ', [rfReplaceAll, rfIgnoreCase] );
         xStr_insData := xStr_insData +'.000';

         xStr_upData := FormatDateTime('YYYY-MM-DD~hh:nn:ss', Now());
         ZQuery1.SQL.Clear;
         ZQuery1.SQL.Add( 'UPDATE sysinfor SET infor_II = '+ QuotedStr( xStr_upData ) +' WHERE infor_I = "Respons_TS"' );
         ZQuery1.ExecSQL;

         if not (Button1.Visible) then
         begin
            ZQuery1.Close;
            ZQuery1.SQL.Clear;
            ZQuery1.SQL.Add( 'DELETE FROM sresponsavelpessoas' );
            ZQuery1.ExecSQL;
         end;

         ZQuery1.SQL.Clear;
         ZQuery1.SQL.Add( 'UPDATE sysinfor SET infor_II = '+ QuotedStr( xStr_upData ) +', inFlag = "0" WHERE infor_I = "Respons_ST"' );
         ZQuery1.ExecSQL;
      end;
   except
// except on E: Exception do
  //  ShowMessage( E.ToString );
   end;
end;


procedure TForm1.xProc_tsRespons;
begin
   ZQuery1.Close;
   ZQuery1.SQL.Clear;
   ZQuery1.SQL.Add( 'SELECT * FROM sysinfor WHERE infor_I = "Respons_TS"' );
   ZQuery1.Open;

   xStr_Data := StringReplace( ZQuery1.FieldByName('infor_II').AsString, '~', ' ', [rfReplaceAll, rfIgnoreCase] );
   xStr_Data := xStr_Data +'.000';

   if (Length( xStr_insData ) < 5) then
      xStr_insData := xStr_Data;

   xStr_SQL :=
     ' SELECT *' + #13
   + '  FROM (' + #13
   + '    SELECT DISTINCT ALUN.ra, ''academico'' tiporespons, ISNULL(ALUN.codparentraca, ''0'') grauparent,' + #13
   + '           PESS.codigo, PESS.nome, ISNULL(PESS.sexo, ''F'') sexo, ISNULL(PESS.dtnascimento, 0) datanasciment, PESS.nacionalidade,' + #13
   + '           ISNULL(PESS.estadonatal, ''XX'') estadonatal, ISNULL(PESS.estadocivil, ''X'') estadocivil,' + #13
   + '     -- Enderec --' + #13
   + '          ISNULL(PESS.rua, ''-vazio-'') rua, ISNULL(PESS.numero, ''-vazio-'') numero, ISNULL(PESS.complemento, ''-vazio-'') complemento,' + #13
   + '          ISNULL(PESS.bairro, ''-vazio-'') bairro, ISNULL(PESS.cidade, ''-vazio-'') cidade, ISNULL(PESS.estado, ''-vazio-'') estado,' + #13
   + '          ISNULL(PESS.cep, ''-vazio-'') cep,' + #13
   + '     -- Contact --' + #13
   + '          ISNULL(PESS.email, ''-vazio-'') email,' + #13
   + '          ISNULL(PESS.telefone2, ''-vazio-'') celular, ISNULL(PESS.telefone1, ''-vazio-'')  telefoneResidencial,' + #13
   + '     -- Document --' + #13
   + '           ISNULL(PESS.cartidentidade, ''-vazio-'') cartidentidade, ISNULL(PESS.dtemissaoident, 0) dtemissaoident,' + #13
   + '           ISNULL(PESS.orgemissorident, ''-vazio-'') orgemissorident, ISNULL(PESS.ufcartident, ''XX'') ufcartident,' + #13
   + '           ISNULL(PESS.cpf, ''-vazio-'') cpf,' + #13
   + '     -- Control --' + #13
   + '         ''1'' flagprocess, PESS.recmodifiedon dataupdate' + #13
   + '      FROM Corpore_Clicksoft.dbo.SALUNO ALUN, Corpore_Clicksoft.dbo.PPESSOA PESS' + #13
   + '     WHERE PESS.CODIGO = ALUN.CODPESSOARACA' + #13
   + '       AND PESS.reccreatedon > CONVERT(DateTime, '+ QuotedStr( xStr_insData ) +')' + #13
// + '       AND PESS.reccreatedon = PESS.recmodifiedon' + #13
   + #13
   + '   UNION' + #13
   + #13
   + '    SELECT DISTINCT ALUN.ra, ''academico'' tiporespons, ISNULL(ALUN.codparentraca, ''0'') grauparent,' + #13
   + '           PESS.codigo, PESS.nome, ISNULL(PESS.sexo, ''F'') sexo, ISNULL(PESS.dtnascimento, 0) datanasciment, PESS.nacionalidade,' + #13
   + '           ISNULL(PESS.estadonatal, ''XX'') estadonatal, ISNULL(PESS.estadocivil, ''X'') estadocivil,' + #13
   + '     -- Enderec --' + #13
   + '          ISNULL(PESS.rua, ''-vazio-'') rua, ISNULL(PESS.numero, ''-vazio-'') numero, ISNULL(PESS.complemento, ''-vazio-'') complemento,' + #13
   + '          ISNULL(PESS.bairro, ''-vazio-'') bairro, ISNULL(PESS.cidade, ''-vazio-'') cidade, ISNULL(PESS.estado, ''-vazio-'') estado,' + #13
   + '          ISNULL(PESS.cep, ''-vazio-'') cep,' + #13
   + '     -- Contact --' + #13
   + '          ISNULL(PESS.email, ''-vazio-'') email,' + #13
   + '          ISNULL(PESS.telefone2, ''-vazio-'') celular, ISNULL(PESS.telefone1, ''-vazio-'')  telefoneResidencial,' + #13
   + '     -- Document --' + #13
   + '           ISNULL(PESS.cartidentidade, ''-vazio-'') cartidentidade, ISNULL(PESS.dtemissaoident, 0) dtemissaoident,' + #13
   + '           ISNULL(PESS.orgemissorident, ''-vazio-'') orgemissorident, ISNULL(PESS.ufcartident, ''XX'') ufcartident,' + #13
   + '           ISNULL(PESS.cpf, ''-vazio-'') cpf,' + #13
   + '     -- Control --' + #13
   + '         ''0'' flagprocess, PESS.recmodifiedon dataupdate' + #13
   + '     FROM Corpore_Clicksoft.dbo.SALUNO ALUN, Corpore_Clicksoft.dbo.PPESSOA PESS' + #13
   + '    WHERE PESS.CODIGO = ALUN.CODPESSOARACA' + #13
   + '      AND ALUN.RA BETWEEN ''2009231'' AND ''2009312''' + #13
   + '      AND PESS.recmodifiedon > CONVERT(DateTime, '+ QuotedStr( xStr_Data ) +')' + #13
   + '      AND PESS.reccreatedon < CONVERT(DateTime, '+ QuotedStr( xStr_insData ) +')' + #13
   + ') subQuery' + #13
   + ' ORDER BY subQuery.dataupdate DESC';
   if (Button1.Visible) then
      Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;

   ADOQuery1.SQL.Clear;
   ADOQuery1.SQL.Add( xStr_SQL );
   ADOQuery1.Open;

   if (ADOQuery1.RecordCount > 0) then
   begin
      ADOQuery1.First;
      with ADOQuery1 do
      begin
         while (NOT ADOQuery1.Eof) do
         begin
            xStr_SQL :=
               'INSERT INTO tresponsavelpessoas' + #13
             + '   (ra, codPessoa, nome, endRua, endNumero, endComplement, endCEP, endBairro, contEMail, contCelular, contTelResidenc, ' + #13
             + '    sexo, dataNasciment, nacionalidade, estadoNatal, estadoCivil, tipoRespons, grauParent, endCidade, endEstado,' + #13
             + '    docIdentNumero, docIdent_orgEmissor, docIdent_dataEmissao, docEstadEmissor, docCPF, flagprocess, dataUpDate)' + #13
             + 'VALUES' + #13
             + '   ('+ QuotedStr( FieldByName('ra').AsString ) +', '+ QuotedStr( FieldByName('codigo').AsString ) +', '+ QuotedStr( FieldByName('nome').AsString ) +', '+
                       QuotedStr( FieldByName('rua').AsString ) +', '+ QuotedStr( FieldByName('numero').AsString ) +', '+ QuotedStr( FieldByName('complemento').AsString ) +', '+
                       QuotedStr( FieldByName('cep').AsString ) +', '+ QuotedStr( FieldByName('bairro').AsString ) +', '+ QuotedStr( FieldByName('email').AsString ) +', '+
                       QuotedStr( FieldByName('celular').AsString ) +', '+ QuotedStr( FieldByName('telefoneResidencial').AsString ) +', '+
                       QuotedStr( FieldByName('sexo').AsString ) +', '+ QuotedStr( FieldByName('datanasciment').AsString ) +', '+ QuotedStr( FieldByName('nacionalidade').AsString ) +', '+
                       QuotedStr( FieldByName('estadonatal').AsString ) +', '+ QuotedStr( FieldByName('estadocivil').AsString ) +', '+
                       QuotedStr( FieldByName('tiporespons').AsString ) +', '+ QuotedStr( FieldByName('grauparent').AsString ) +', '+ QuotedStr( FieldByName('cidade').AsString ) +', '+
                       QuotedStr( FieldByName('estado').AsString ) +', '+ QuotedStr( FieldByName('cartidentidade').AsString ) +', '+ QuotedStr( FieldByName('orgemissorident').AsString ) +', '+
                       QuotedStr( FieldByName('dtemissaoident').AsString ) +', '+ QuotedStr( FieldByName('ufcartident').AsString ) +', '+ QuotedStr( FieldByName('cpf').AsString ) +', '+
                       QuotedStr( FieldByName('flagprocess').AsString ) +', '+ QuotedStr( FieldByName('dataupdate').AsString ) +')';
            Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;

            ZQuery1.SQL.Clear;
            ZQuery1.SQL.Add( xStr_SQL );
            ZQuery1.ExecSQL;

            ADOQuery1.Next;
         end;
      end;

      xStr_upData := FormatDateTime('YYYY-MM-DD~hh:nn:ss', Now());
      xStr_SQL :=
         'UPDATE sysinfor' + #13
       + '   SET infor_II = '+ QuotedStr( xStr_upData ) +', '+ #13
       + '       inFlag = "1"' + #13
       + ' WHERE infor_I = "Respons_TS"';
      if (Button1.Visible) then
         Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;

      ZQuery1.SQL.Clear;
      ZQuery1.SQL.Add( xStr_SQL );
      ZQuery1.ExecSQL;
   end;
end;

// _  __  _  __.- - --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=-- - -.__  _  __  _ \\


procedure TForm1.xProc_stProfessor;
begin
   try
      ZQuery1.Close;
      ZQuery1.SQL.Clear;
      ZQuery1.SQL.Add( 'SELECT * FROM sprofessorpessoas' );
      ZQuery1.Open;

      if (ZQuery1.RecordCount > 0) then
      begin
         ZQuery1.First;
         with ZQuery1 do
         begin
            while (NOT ZQuery1.Eof) do
            begin
               if (FieldByName('flagProcess').AsString = '1') then
               begin
                  xStr_SQL :=
                   'INSERT INTO Corpore_Clicksoft.dbo.PPESSOA' + #13
                  + '   (codigo, nome, sexo, dtnascimento, nacionalidade, estadonatal, estadocivil, rua, numero, complemento, bairro, cep, email, telefone2, telefone1, cartidentidade, dtemissaoident, orgemissorident, ufcartident, cpf,' + #13
                  + '    aluno, professor, usuariobiblios, funcionario, exfuncionario, candidato, reccreatedby, reccreatedon, recmodifiedby, recmodifiedon)' + #13
                  + 'VALUES' + #13
                  + ' ( ( SELECT (MAX( codigo ) + 1) FROM Corpore_Clicksoft.dbo.PPESSOA )'
                  + ', '+ QuotedStr( UTF8Decode( FieldByName('nome').AsString ) ) +', '+ QuotedStr( FieldByName('sexo').AsString )
                  + ', '+ funcRIf(FieldByName('dataNasciment').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('dataNasciment').AsString ))
                  + ', '+ funcRIf(FieldByName('nacionalidade').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('nacionalidade').AsString ))
                  + ', '+ funcRIf(FieldByName('estadoNatal').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('estadoNatal').AsString ))
                  + ', '+ funcRIf(FieldByName('estadoCivil').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('estadoCivil').AsString ))
                  + ', '+ funcRIf(FieldByName('endRua').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endRua').AsString ))
                  + ', '+ funcRIf(FieldByName('endNumero').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endNumero').AsString ))
                  + ', '+ funcRIf(FieldByName('endComplement').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endComplement').AsString ))
                  + ', '+ funcRIf(FieldByName('endBairro').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endBairro').AsString ))
                  + ', '+ funcRIf(FieldByName('endCEP').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endCEP').AsString ))
                  + ', '+ funcRIf(FieldByName('contEMail').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('contEMail').AsString ))
                  + ', '+ funcRIf(FieldByName('contCelular').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('contCelular').AsString ))
                  + ', '+ funcRIf(FieldByName('contTelResidenc').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('contTelResidenc').AsString ))
                  + ', '+ funcRIf(FieldByName('docIdentNumero').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docIdentNumero').AsString ))
                  + ', '+ funcRIf(FieldByName('docIdent_dataEmissao').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docIdent_dataEmissao').AsString ))
                  + ', '+ funcRIf(FieldByName('docIdent_orgEmissor').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docIdent_orgEmissor').AsString ))
                  + ', '+ funcRIf(FieldByName('docEstadEmissor').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docEstadEmissor').AsString ))
                  + ', '+ funcRIf(FieldByName('docCPF').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docCPF').AsString )) + #13
                  + ', ''0'', ''0'', ''0'', ''0'', ''0'', ''0'', ''luciano.gael'', GETDATE(), ''luciano.gael'', GETDATE() )';
                  Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;
                  ADOQuery1.SQL.Clear;
                  ADOQuery1.SQL.Add( xStr_SQL );
                  ADOQuery1.ExecSQL;

                  xStr_SQL :=
                    'INSERT INTO Corpore_Clicksoft.dbo.SPROFESSOR' + #13
                  + '   (codcoligada, codprof, codpessoa, reccreatedby, reccreatedon, recmodifiedby, recmodifiedon)' + #13
                  + 'VALUES' + #13
                  + '   (''1'', (SELECT (MAX( CONVERT(DECIMAL, CODPROF) ) + 1) FROM Corpore_Clicksoft.dbo.SPROFESSOR), (SELECT MAX( codigo ) FROM Corpore_Clicksoft.dbo.PPESSOA), ''luciano.gael'', GETDATE(), ''luciano.gael'', GETDATE())' + #13;
                  Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;
                  ADOQuery1.SQL.Clear;
                  ADOQuery1.SQL.Add( xStr_SQL );
                  ADOQuery1.ExecSQL;

                  xStr_SQL :=
                    'SELECT PRO.CODPESSOA, PRO.CODPROF' + #13
                  + '  FROM Corpore_Clicksoft.dbo.PPESSOA PES, Corpore_Clicksoft.dbo.SPROFESSOR PRO' + #13
                  + ' WHERE PES.CODIGO = (SELECT MAX( codigo ) FROM Corpore_Clicksoft.dbo.PPESSOA)' + #13
                  + '   AND PRO.CODPESSOA = PES.CODIGO' + #13;
                  Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;
                  ADOQuery1.SQL.Clear;
                  ADOQuery1.SQL.Add( xStr_SQL );
                  ADOQuery1.Open;

                  xStr_URL := 'http://clicksoft.com.br/mopi/int/setcod_profes.php?id='+ FieldByName('dataUpDate').AsString +'&codprof='+ ADOQuery1.FieldByName('codprof').AsString +'&codpessoa='+ ADOQuery1.FieldByName('codpessoa').AsString;
                  Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + 'curl > '+ xStr_URL;
                  IdHTTP1.Get( xStr_URL );
               end
               else
               begin
                  xStr_Data := Copy( FieldByName('dataUpDate').AsString, 01, 19 ) + '.000';
                  xStr_Data := StringReplace( xStr_Data, 'T', ' ', [rfReplaceAll, rfIgnoreCase] );

                  xStr_SQL :=
                    'UPDATE Corpore_Clicksoft.dbo.PPESSOA' + #13
                  + '  SET nome = '+ QuotedStr( UTF8Decode( FieldByName('nome').AsString ) ) +', ' + #13
                  + '      sexo = '+ QuotedStr( FieldByName('sexo').AsString ) +', ' + #13
                  + '      dtnascimento = '+ funcRIf(FieldByName('dataNasciment').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('dataNasciment').AsString )) +', ' + #13
                  + '      nacionalidade = '+ funcRIf(FieldByName('nacionalidade').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('nacionalidade').AsString )) +', ' + #13
                  + '      estadonatal = '+ funcRIf(FieldByName('estadoNatal').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('estadoNatal').AsString )) +', ' + #13
                  + '      estadocivil = '+ funcRIf(FieldByName('estadoCivil').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('estadoCivil').AsString )) +', ' + #13
                  + '      rua = '+ funcRIf(FieldByName('endRua').AsString = '-vazio-', 'NULL', QuotedStr( UTF8Decode( FieldByName('endRua').AsString ) )) +', ' + #13
                  + '      numero = '+ funcRIf(FieldByName('endNumero').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endNumero').AsString )) +', ' + #13
                  + '      complemento = '+ funcRIf(FieldByName('endComplement').AsString = '-vazio-', 'NULL', QuotedStr( UTF8Decode( FieldByName('endComplement').AsString ) )) +', ' + #13
                  + '      bairro = '+ funcRIf(FieldByName('endBairro').AsString = '-vazio-', 'NULL', QuotedStr( UTF8Decode( FieldByName('endBairro').AsString ) )) +', ' + #13
               // + '      cidade
               // + '      estado
                  + '      cep = '+ funcRIf(FieldByName('endCEP').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('endCEP').AsString )) +', ' + #13
                  + '      email = '+ funcRIf(FieldByName('contEMail').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('contEMail').AsString )) +', ' + #13
                  + '      telefone2 = '+ funcRIf(FieldByName('contCelular').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('contCelular').AsString )) +', ' + #13
                  + '      telefone1 = '+ funcRIf(FieldByName('contTelResidenc').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('contTelResidenc').AsString )) +', ' + #13
                  + '      cartidentidade = '+ funcRIf(FieldByName('docIdentNumero').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docIdentNumero').AsString )) +', ' + #13
                  + '      dtemissaoident = '+ funcRIf(FieldByName('docIdent_dataEmissao').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docIdent_dataEmissao').AsString )) +', ' + #13
                  + '      orgemissorident = '+ funcRIf(FieldByName('docIdent_orgEmissor').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docIdent_orgEmissor').AsString )) +', ' + #13
                  + '      ufcartident = '+ funcRIf(FieldByName('docEstadEmissor').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docEstadEmissor').AsString )) +', ' + #13
                  + '      cpf = '+ funcRIf(FieldByName('docCPF').AsString = '-vazio-', 'NULL', QuotedStr( FieldByName('docCPF').AsString )) +', ' + #13
                  + '      recmodifiedby = ''luciano.gael'', '+ #13
                  + '      recmodifiedon = GETDATE()'+ #13
                  + ' WHERE CODIGO = ' + FieldByName('codPessoa').AsString + #13
                  + '   AND (CONVERT(DateTime, '+ QuotedStr( xStr_Data ) +') > RECMODIFIEDON OR RECMODIFIEDON IS NULL)';
                  Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;

                  ADOQuery1.SQL.Clear;
                  ADOQuery1.SQL.Add( xStr_SQL );
                  ADOQuery1.ExecSQL;
               end;

               ZQuery1.Next;
            end;
         end;

         xStr_upData := FormatDateTime('YYYY-MM-DD~hh:nn:ss', Now());
         ZQuery1.SQL.Clear;
         ZQuery1.SQL.Add( 'UPDATE sysinfor SET infor_II = '+ QuotedStr( xStr_upData ) +' WHERE infor_I = "Professor_TS"' );
         ZQuery1.ExecSQL;

         if not (Button1.Visible) then
         begin
            ZQuery1.Close;
            ZQuery1.SQL.Clear;
            ZQuery1.SQL.Add( 'DELETE FROM sprofessorpessoas' );
            ZQuery1.ExecSQL;
         end;

         ZQuery1.SQL.Clear;
         ZQuery1.SQL.Add( 'UPDATE sysinfor SET infor_II = '+ QuotedStr( xStr_upData ) +', inFlag = "0" WHERE infor_I = "Professor_ST"' );
         ZQuery1.ExecSQL;
      end;
   except
// except on E: Exception do
  //  ShowMessage( E.ToString );
   end;
end;


procedure TForm1.xProc_tsProfessor;
begin
   ZQuery1.Close;
   ZQuery1.SQL.Clear;
   ZQuery1.SQL.Add( 'SELECT * FROM sysinfor WHERE infor_I = "Professor_TS"' );
   ZQuery1.Open;

   xStr_Data := StringReplace( ZQuery1.FieldByName('infor_II').AsString, '~', ' ', [rfReplaceAll, rfIgnoreCase] );
   xStr_Data := xStr_Data +'.000';

   xStr_SQL :=
     '   SELECT tProf.codprof, tPess.codigo codpessoa, tPess.nome, ISNULL(tPess.sexo, ''F'') sexo,' + #13
   + '        ISNULL(tPess.dtnascimento, 0) datanasciment, ISNULL(tPess.nacionalidade, ''00'') nacionalidade,' + #13
   + '        ISNULL(tPess.estadonatal, ''XX'') estadonatal, ISNULL(tPess.estadocivil, ''X'') estadocivil,' + #13
   + '     -- Enderec --' + #13
   + '        ISNULL(tPess.rua, ''-vazio-'') rua, ISNULL(tPess.numero, ''-vazio-'') numero, ISNULL(tPess.complemento, ''-vazio-'') complemento,' + #13
   + '        ISNULL(tPess.bairro, ''-vazio-'') bairro, ISNULL(tPess.cidade, ''-vazio-'') cidade, ISNULL(tPess.estado, ''XX'') estado,' + #13
   + '        ISNULL(tPess.cep, ''-vazio-'') cep,' + #13
   + '     -- Contat --' + #13
   + '        ISNULL(tPess.email, ''-vazio-'') email,' + #13
   + '        ISNULL(tPess.telefone2, ''-vazio-'') celular, ISNULL(tPess.telefone1, ''-vazio-'')  telefoneResidencial,' + #13
   + '     -- Document --' + #13
   + '        -- ''-vazio-'' doccertnasc, ''-vazio-'' docnum_certnasc,' + #13
   + '        ISNULL(tPess.cartidentidade, ''-vazio-'') cartidentidade, ISNULL(tPess.dtemissaoident, 0) dtemissaoident,' + #13
   + '        ISNULL(tPess.orgemissorident, ''-vazio-'') orgemissorident, ISNULL(tPess.ufcartident, ''XX'') ufcartident,' + #13
   + '        ISNULL(tPess.cpf, ''-vazio-'') cpf,' + #13
   + '     -- Control --' + #13
   + '        (CASE WHEN tPess.recmodifiedon = tPess.reccreatedon THEN ''1'' ELSE ''0'' END) flagprocess, tPess.recmodifiedon dataupdate' + #13
   + '   FROM Corpore_Clicksoft.dbo.SPROFESSOR tProf, Corpore_Clicksoft.dbo.PPESSOA tPess' + #13
   + '  WHERE tPess.CODIGO = tProf.CODPESSOA' + #13
   + '    AND tPess.recmodifiedon > CONVERT(DateTime, '+ QuotedStr( xStr_Data ) +')' + #13
   + '  ORDER BY tPess.recmodifiedon DESC';
   if (Button1.Visible) then
      Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;

   ADOQuery1.SQL.Clear;
   ADOQuery1.SQL.Add( xStr_SQL );
   ADOQuery1.Open;

   if (ADOQuery1.RecordCount > 0) then
   begin
      ADOQuery1.First;
      with ADOQuery1 do
      begin
         while (NOT ADOQuery1.Eof) do
         begin
            xStr_SQL :=
               'INSERT INTO tprofessorpessoas' + #13
             + '   (codProf, codPessoa, nome, endRua, endNumero, endComplement, endCEP, endBairro, contEMail, contCelular, contTelResidenc, ' + #13
             + '    sexo, dataNasciment, nacionalidade, estadoNatal, estadoCivil, endCidade, endEstado, ' + #13
             + '    docIdentNumero, docIdent_orgEmissor, docIdent_dataEmissao, docEstadEmissor, docCPF, flagprocess, dataUpDate)' + #13
             + 'VALUES' + #13
             + '   ('+ QuotedStr( FieldByName('codprof').AsString ) +', '+ QuotedStr( FieldByName('codpessoa').AsString ) +', '+ QuotedStr( FieldByName('nome').AsString ) +', '+
                       QuotedStr( FieldByName('rua').AsString ) +', '+ QuotedStr( FieldByName('numero').AsString ) +', '+ QuotedStr( FieldByName('complemento').AsString ) +', '+
                       QuotedStr( FieldByName('cep').AsString ) +', '+ QuotedStr( FieldByName('bairro').AsString ) +', '+ QuotedStr( FieldByName('email').AsString ) +', '+
                       QuotedStr( FieldByName('celular').AsString ) +', '+ QuotedStr( FieldByName('telefoneResidencial').AsString ) +', '+
                       QuotedStr( FieldByName('sexo').AsString ) +', '+ QuotedStr( FieldByName('datanasciment').AsString ) +', '+ QuotedStr( FieldByName('nacionalidade').AsString ) +', '+
                       QuotedStr( FieldByName('estadonatal').AsString ) +', '+ QuotedStr( FieldByName('estadocivil').AsString ) +', '+ QuotedStr( FieldByName('cidade').AsString ) +', '+
                       QuotedStr( FieldByName('estado').AsString ) +', '+ QuotedStr( FieldByName('cartidentidade').AsString ) +', '+ QuotedStr( FieldByName('orgemissorident').AsString ) +', '+
                       QuotedStr( FieldByName('dtemissaoident').AsString ) +', '+ QuotedStr( FieldByName('ufcartident').AsString ) +', '+ QuotedStr( FieldByName('cpf').AsString ) +', '+
                       QuotedStr( FieldByName('flagprocess').AsString ) +', '+ QuotedStr( FieldByName('dataupdate').AsString ) +')';
            Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;

            ZQuery1.SQL.Clear;
            ZQuery1.SQL.Add( xStr_SQL );
            ZQuery1.ExecSQL;

            ADOQuery1.Next;
         end;
      end;

      xStr_upData := FormatDateTime('YYYY-MM-DD~hh:nn:ss', Now());
      xStr_SQL :=
         'UPDATE sysinfor' + #13
       + '   SET infor_II = '+ QuotedStr( xStr_upData ) +', '+ #13
       + '       inFlag = "1"' + #13
       + ' WHERE infor_I = "Professor_TS"';
      if (Button1.Visible) then
         Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;

      ZQuery1.SQL.Clear;
      ZQuery1.SQL.Add( xStr_SQL );
      ZQuery1.ExecSQL;
   end;
end;

// _  __  _  __.- - --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=-- - -.__  _  __  _ \\


procedure TForm1.xProc_stNotaFalta;
begin
   try
      ZQuery1.Close;
      ZQuery1.SQL.Clear;
      ZQuery1.SQL.Add( 'SELECT * FROM snotasfaltas' );
      ZQuery1.Open;

      if (ZQuery1.RecordCount > 0) then
      begin
         ZQuery1.First;
         with ZQuery1 do
         begin
            while (NOT ZQuery1.Eof) do
            begin
               xStr_Data := Copy( FieldByName('dataUpDate').AsString, 01, 19 ) + '.000';
               xStr_Data := StringReplace( xStr_Data, 'T', ' ', [rfReplaceAll, rfIgnoreCase] );

               xStr_SQL :=
                 'UPDATE Corpore_Clicksoft.dbo.SNOTAETAPA' + #13
               + '   SET NOTAFALTA = '+ QuotedStr( FieldByName('notaFalta').AsString ) + #13
               + ' WHERE CODETAPA = '+ QuotedStr( FieldByName('codEtapa').AsString ) + #13
               + '   AND RA = '+ QuotedStr( FieldByName('ra').AsString ) + #13
               + '   AND IDTURMADISC = ( SELECT DISTINCT tNota.IDTURMADISC' + #13
               + '                         FROM Corpore_Clicksoft.dbo.SETAPAS tEtap, Corpore_Clicksoft.dbo.STURMADISC tDTur,' + #13
               + '                              Corpore_Clicksoft.dbo.SPLETIVO tPLet, Corpore_Clicksoft.dbo.SNOTAETAPA tNota' + #13
               + '                        WHERE tDTur.IDTURMADISC = tEtap.IDTURMADISC' + #13
               + '                          AND tPLet.IDPERLET = tDTur.IDPERLET' + #13
               + '                          AND tPLet.CODPERLET = '+ QuotedStr( FieldByName('anoLetivo').AsString ) + #13
               + '                          AND tNota.CODETAPA = '+ QuotedStr( FieldByName('codEtapa').AsString ) + #13
               + '                          AND tNota.TIPOETAPA = tEtap.TIPOETAPA' + #13
               + '                          AND tNota.IDTURMADISC = tEtap.IDTURMADISC' + #13
               + '                          AND tNota.RA = '+ QuotedStr( FieldByName('ra').AsString ) + #13
               + '                          AND tDTur.CODDISC = '+ QuotedStr( FieldByName('codDisc').AsString ) + ' )' + #13
               + '   AND (CONVERT(DateTime, '+ QuotedStr( xStr_Data ) +') > RECMODIFIEDON OR RECMODIFIEDON IS NULL)';
               Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;

               ADOQuery1.SQL.Clear;
               ADOQuery1.SQL.Add( xStr_SQL );
               ADOQuery1.ExecSQL;

               ZQuery1.Next;
            end;
         end;

         xStr_upData := FormatDateTime('YYYY-MM-DD~hh:nn:ss', Now());
         ZQuery1.SQL.Clear;
         ZQuery1.SQL.Add( 'UPDATE sysinfor SET infor_II = '+ QuotedStr( xStr_upData ) +' WHERE infor_I = "NotaFalta_TS"' );
         ZQuery1.ExecSQL;

         ZQuery1.Close;
         ZQuery1.SQL.Clear;
         ZQuery1.SQL.Add( 'DELETE FROM snotasfaltas' );
         ZQuery1.ExecSQL;

         ZQuery1.SQL.Clear;
         ZQuery1.SQL.Add( 'UPDATE sysinfor SET infor_II = '+ QuotedStr( xStr_upData ) +', inFlag = "0" WHERE infor_I = "NotaFalta_ST"' );
         ZQuery1.ExecSQL;
      end;
   except
// except on E: Exception do
  //  ShowMessage( E.ToString );
   end;
end;


procedure TForm1.xProc_tsNotaFalta;
begin
   ZQuery1.Close;
   ZQuery1.SQL.Clear;
   ZQuery1.SQL.Add( 'SELECT * FROM sysinfor WHERE infor_I = "NotaFalta_TS"' );
   ZQuery1.Open;

   xStr_Data := StringReplace( ZQuery1.FieldByName('infor_II').AsString, '~', ' ', [rfReplaceAll, rfIgnoreCase] );
   xStr_Data := xStr_Data +'.000';

   xStr_SQL :=
     ' SELECT DISTINCT tPLet.codperlet, tDTur.coddisc, tNota.ra, tEtap.codetapa,' + #13
   + '        tEtap.tipoetapa, tEtap.descricao,' + #13
   + '        ISNULL(CONVERT(CHAR(10), tNota.notafalta), tNota.codconceito) notafalta_c,' + #13
   + '        tNota.recmodifiedon dataupdate' + #13
   + '   FROM Corpore_Clicksoft.dbo.SETAPAS tEtap, Corpore_Clicksoft.dbo.STURMADISC tDTur,' + #13
   + '        Corpore_Clicksoft.dbo.SPLETIVO tPLet, Corpore_Clicksoft.dbo.SNOTAETAPA tNota' + #13
   + '  WHERE tDTur.IDTURMADISC = tEtap.IDTURMADISC' + #13
   + '    AND tPLet.IDPERLET = tDTur.IDPERLET' + #13
   + '    AND tPLet.codperlet = ''2015''' + #13
   + '    AND tNota.CODETAPA = tEtap.CODETAPA' + #13
   + '    AND tNota.TIPOETAPA = tEtap.TIPOETAPA' + #13
   + '    AND tNota.IDTURMADISC = tEtap.IDTURMADISC' + #13
   + '    AND (tNota.notafalta IS NOT NULL OR tNota.codconceito IS NOT NULL)' + #13
   + '    AND tNota.RA BETWEEN ''2009231'' AND ''2009312''' + #13
   + '    AND tDTur.coddisc = ''11''' + #13
   + '    AND tNota.recmodifiedon > CONVERT(DateTime, '+ QuotedStr( xStr_Data ) +')' + #13
   + '  ORDER BY tNota.recmodifiedon DESC';

   ADOQuery1.SQL.Clear;
   ADOQuery1.SQL.Add( xStr_SQL );
   ADOQuery1.Open;

   if (ADOQuery1.RecordCount > 0) then
   begin
      ADOQuery1.First;
      with ADOQuery1 do
      begin
         while (NOT ADOQuery1.Eof) do
         begin
            xStr_SQL :=
               'INSERT INTO tnotasfaltas' + #13
             + '   (ra, codEtapa, descricao, anoLetivo, codDisc, notaFalta, tipoEtapa, dataUpDate)' + #13
             + 'VALUES' + #13
             + '   ('+ QuotedStr( FieldByName('ra').AsString ) +', '+ QuotedStr( FieldByName('codetapa').AsString ) +', '+ QuotedStr( FieldByName('descricao').AsString ) +', '
             +         QuotedStr( FieldByName('codperlet').AsString ) +', '+ QuotedStr( FieldByName('coddisc').AsString ) +', '+ QuotedStr( FieldByName('notafalta_c').AsString ) +', '
             +         QuotedStr( FieldByName('tipoetapa').AsString ) +', '+ QuotedStr( FieldByName('dataupdate').AsString ) +')';
            Memo1.Lines.Text := Memo1.Lines.Text + #13#13 + xStr_SQL;

            ZQuery1.SQL.Clear;
            ZQuery1.SQL.Add( xStr_SQL );
            ZQuery1.ExecSQL;

            ADOQuery1.Next;
         end;
      end;

      xStr_upData := FormatDateTime('YYYY-MM-DD~hh:nn:ss', Now());
      xStr_SQL :=
         'UPDATE sysinfor' + #13
       + '   SET infor_II = '+ QuotedStr( xStr_upData ) +', '+ #13
       + '       inFlag = "1"' + #13
       + ' WHERE infor_I = "NotaFalta_TS"';

      ZQuery1.SQL.Clear;
      ZQuery1.SQL.Add( xStr_SQL );
      ZQuery1.ExecSQL;
   end;
end;

end.
