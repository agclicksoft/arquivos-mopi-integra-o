unit Func_1R;

interface

uses
   SysUtils, DBTables, Windows;

   function funcRIf( prBolCond: Boolean; prVar_returnIf, prVar_returnElse: Variant ): Variant;
   function funcAjustInfor( prStrInfo: String; prIntTamanho: Integer; prCarPreench, prCarTipo: Char): String;
   function funcFormatInfor( prVarInfor: Variant ): Variant;
   function funcOrd(prmChar:AnsiChar): Integer;

implementation

   function funcOrd(prmChar: AnsiChar): Integer;
   var
      xIntTemp: Integer;
   begin
      xIntTemp := Ord( prmChar );

      if (xIntTemp > 52) then
         Result := (xIntTemp + 13)
      else
         Result := xIntTemp;
   end;


   function funcRIf( prBolCond: Boolean; prVar_returnIf, prVar_returnElse: Variant ): Variant;
   begin
      if (prBolCond) then
         Result := prVar_returnIf
      else
         Result := prVar_returnElse;
   end;


   function funcAjustInfor( prStrInfo: String; prIntTamanho: Integer; prCarPreench, prCarTipo: Char): String;
   var
      rArray: array[0..250] of char;
   begin
      FillChar( rArray, prIntTamanho, Ord(prCarPreench) );

      if (prCarTipo = 'R') then
         Result := Copy( rArray, 0, prIntTamanho - Length(prStrInfo) ) + prStrInfo
      else
         Result := Copy( prStrInfo + rArray, 0, prIntTamanho );
   end;


   function funcFormatInfor( prVarInfor: Variant ): Variant;
   begin
      if (Trim( prVarInfor ) = '') or (Trim( prVarInfor ) = '00/00/0000') then
         Result := 'NULL'
      else
         Result := ''''+ Trim( prVarInfor ) +'''';
   end;

end.
