
<title>aluno</title>

<script language="php">
// header('Content-Type: text/html; charset=utf-8');

// ini_set( 'display_errors', 1 );

require_once( './setcod_localid.php' );

function call(
    $url,
    $oauthtoken='e380a949-1585-bbd8-ed56-552eb76ada72',
    $type='POST',
    $arguments=array(),
    $encodeData=true,
    $returnHeaders=false
)
{
    $type = strtoupper($type);

    if ($type == 'GET')
    {
        $url .= '?' . http_build_query($arguments);
    }

    $curl_request = curl_init($url);

    if ($type == 'POST')
    {
        curl_setopt($curl_request, CURLOPT_POST, 1);
    }
    elseif ($type == 'PUT')
    {
        curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, 'PUT');
    }
    elseif ($type == 'DELETE')
    {
        curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, 'DELETE');
    }

    curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($curl_request, CURLOPT_HEADER, $returnHeaders);
    curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

    if (!empty($oauthtoken))
    {
        $token = array("oauth-token: {$oauthtoken}");
        curl_setopt($curl_request, CURLOPT_HTTPHEADER, $token);
    }

    if (!empty($arguments) && $type !== 'GET')
    {
        if ($encodeData)
        {
            //encode the arguments as JSON
            $arguments = json_encode($arguments);
        }
        curl_setopt($curl_request, CURLOPT_POSTFIELDS, $arguments);
    }

    $result = curl_exec($curl_request);

    if ($returnHeaders)
    {
        //set headers from response
        list($headers, $content) = explode("\r\n\r\n", $result ,2);
        foreach (explode("\r\n",$headers) as $header)
        {
            header($header);
        }

        //return the nonheader data
        return trim($content);
    }

    curl_close($curl_request);

    //decode the response from JSON
    $response = json_decode($result);

    return $response;
}



function checkInfor( $parmInform ) {
   $xReturn = '';

   if ((Empty( $parmInform )) or ($parmInform == '-vazio-'))
      $xReturn = '';
   else
      $xReturn = utf8_decode( $parmInform );

   return $xReturn;
}



function setEstCivil( $parmInform ) {
   $xReturn = '';

   switch (strtolower( $parmInform )) {
   case 'casado':
     $xReturn = 'C';
     break;
   case 'desquitado':
     $xReturn = 'D';
     break;
   case 'divorsiado':
     $xReturn = 'I';
     break;
   case 'separado':
     $xReturn = 'O';
     break;
/* case 'solteiro':
     $xReturn = 'S';
     break; */
   case 'viuvo':
     $xReturn = 'V';
     break;
   default:
     $xReturn = 'S';
   };

   return $xReturn;
}



function setSexo( $parmInform ) {
   $xReturn = '';

   if ($parmInform == 'feminino') 
      $xReturn = 'F';
   else
      $xReturn = 'M';

   return $xReturn;
}



function setTipoAluno( $parmInform ) {
   $xReturn = '';

   switch (strtolower( $parmInform )) {
/* case 'cursando':
     $xReturn = '1';
     break; */
   case 'cancelado':
     $xReturn = '2';
     break;
   case 'transferido':
     $xReturn = '3';
     break;
   case 'nao_renovado':
     $xReturn = '4';
     break;
   case 'em_rematricula':
     $xReturn = '5';
     break; 
   case 'prospect':
     $xReturn = '6';
     break;
   case 'prospect_cancelado':
     $xReturn = '7';
     break;
   default:
     $xReturn = '1';
   };

   return $xReturn;
}




function setTipoMatric( $parmInform ) {
   $xReturn = '';

   switch ($parmInform) {
   case 'inicial':
     $xReturn = '1';
     break;
   case 'renovada':
     $xReturn = '2';
     break;
   case 'pos_transferencia':
     $xReturn = '3';
     break;
   case 'pre_matricula':
     $xReturn = '4';
   };

   return $xReturn;
}



function setCurso( $parmInform ) {
   $xReturn = '';

   switch ($parmInform) {
   case 'educacao_infantil':
     $xReturn = '1';
     break;
   case 'ensino_fundamental_i':
     $xReturn = '2';
     break;
   case 'ensino_fundametal_ii':
     $xReturn = '3';
     break;
   case 'ensino_medio':
     $xReturn = '4';
     break;
   case 'horario_mopi':
     $xReturn = '5';
     break;
   case 'atividades_esportivas':
     $xReturn = '6';
     break;
   case 'oficinas':
     $xReturn = '7';
   };

   return $xReturn;
}



function setSerie( $parmInform ) {
   $xReturn = '';

   switch ($parmInform) {
   case 'mat_1':
     $xReturn = '2';
     break;
   case 'mat_2':
     $xReturn = '3';
     break;
   case 'pre_1':
     $xReturn = '4';
     break;
   case 'pre_2':
     $xReturn = '5';
     break;
   case '1_ano':
     $xReturn = '6';
     break;
   case '2_ano':
     $xReturn = '7';
     break;
   case '3_ano':
     $xReturn = '8';
     break;
   case '4_ano':
     $xReturn = '9';
     break;
   case '5_ano':
     $xReturn = '10';
     break;
   case '6_ano':
     $xReturn = '11';
     break;
   case '7_ano':
     $xReturn = '12';
     break;
   case '8_ano':
     $xReturn = '13';
     break;
   case '9_ano':
     $xReturn = '14';
     break;
   case 'em_1serie':
     $xReturn = '15';
     break;
   case 'em_2serie':
     $xReturn = '16';
     break;
   case 'em_3serie':
     $xReturn = '17';
 /*  break;
   default:
     $xReturn = '-vazio-'; */
   };

   return $xReturn;
}



function setUnidade( $parmInform ) {
   $xReturn = '';

   switch ($parmInform) {
   case 'matriz':
     $xReturn = '1';
     break;
   case 'filial_integrado_1':
     $xReturn = '2';
     break;
   case 'filial_integrado_2':
     $xReturn = '3';
     break;
   default:
     $xReturn = '4';
   };

   return $xReturn;
}



function setModCertidao( $parmInform ) {
   $xReturn = '';

   switch ($parmInform) {
   case 'modelo_antigo':
     $xReturn = 'CN';
     break;
   case 'modelo_novo':
     $xReturn = 'NN';
     break;
   };

   return $xReturn;
}

                  // __.-  --=-=- -===--.__.®.__.--===- -=-=--  -.__ \\


$base_url = 'https://mopi1.sugarondemand.com/rest/v10';

//Login - POST /oauth2/token

$url = $base_url . '/oauth2/token';

$oauth2_token_arguments = array(
   'grant_type' => 'password',
   'client_id' => 'sugar',
   'client_secret' => '',
   'username' => 'Alfredo',
   'password' => 'bSa9RW',
   'platform' => 'base'
);

$oauth2_token_response = call($url, '', 'POST', $oauth2_token_arguments); 





/* [ webbynode ]
$mopi_servername = "173.246.40.203";
$mopi_username = "root";
$mopi_password = "c1l2i3c4k5";
$mopi_database = "mopiintegracao";
*/
$mopi_servername = "199.201.89.205";
$mopi_username = "clicksof_mopi";
$mopi_password = "clicksof.mopi";
$mopi_database = "clicksof_mopi_integracao";


// Create connection
   $mopi_conn = mysql_connect($mopi_servername, $mopi_username, $mopi_password) or die ("Erro no servidor: ".mysql_error());
   $xBD = mysql_select_db($mopi_database, $mopi_conn) or die ("Erro base: ".mysql_error());


                  // __.-  --=-=- -===--.__.®.__.--===- -=-=--  -.__ \\


$xBool_withAtualizac = false;
$mopi_query = "SELECT * FROM tabcontrol WHERE Processo = 'Alunos_ST'";

   $mopi_result = mysql_query( $mopi_query );

/*
echo '<pre>';
   print_r( $mopi_result );
echo '</pre>';
*/

$strData_Alt = mysql_result( $mopi_result, 0, "alt_dateTime" );
$xFlag_Alt =  mysql_result( $mopi_result, 0, "alt_Flag" );

$xArray = explode("~", $strData_Alt);
$strData_Alt = current( $xArray ) .'T'. end( $xArray ); // .'-00:00';


$strData_Inc = mysql_result( $mopi_result, 0, "inc_dateTime" );
$xFlag_Inc =  mysql_result( $mopi_result, 0, "inc_Flag" );

$xArray = explode("~", $strData_Inc);
$strData_Inc = current( $xArray ) .'T'. end( $xArray ); // .'-00:00';


echo '<br>.: '. $strData_Alt .' :.<br>.: '. $strData_Inc .' :.<hr>';


if (($xFlag_Alt == '0') && ($xFlag_Inc == '0')) {
   //Filter - /<module>/filter POST
   $filter_arguments = array(
       "filter" => array(
             array(
               '$and' => array(
                   array(
                    // "date_modified" => '2015-06-23T18:34:52-03:00'
                    /* "first_name" => array(
                           '$starts' => "Loren",
                       ) */
                       "date_modified" => array(
                           '$gt' => $strData_Alt,
                       ) 
                   )
               ),
           ), 
       ), 
    // 'max_num' => 15,
    // 'offset' => 04,
    // 'fields' => 'full_name, unidade_c, registro_academico_c',
    // 'order_by' => 'name:DESC',
    // 'favorites' => false,
    // 'my_items' => false
   );

   $url = $base_url . '/Contacts/filter';

   $filter_response = call($url, $oauth2_token_response->access_token, 'POST', $filter_arguments);

   // $xCidade = 'Rio de Janeiro';
   // $xEstado = 'RJ';

   foreach ( $filter_response as $item_response ) {

      $xQtd_Itens = Count( $item_response );

      $xEst_Ender = 'RJ';
      $xCid_Ender = 'Rio de Janeiro';
      $xEst_Natal = 'RJ';
      $xCid_Natal = 'Rio de Janeiro';
      $xNacionalid = '10';
      $xNaturalidad = '10';
      $xEst_idEmiss = 'RJ';

      $xFlag_Alt = false;
      $xFlag_Inc = false;

      if ($xQtd_Itens > 0) {
         foreach ( $item_response as $xItem ) {

                  // __.-  --=-=- -===--.__.cidadeNatal.estadoNatal.__.--===- -=-=--  -.__ \\
            $filter_arguments = array(
                  "filter" => array(
                      array(
                        '$and' => array(
                            array(
                                "id" => $xItem->ciuf_cidades_uf_id1_c
                            ), 
                        )
                    ) 
                ) 
            );


            $url = $base_url . '/CIUF_cidades_uf/filter';

            $filter_response = call($url, $oauth2_token_response->access_token, 'POST', $filter_arguments);

         // echo '<br>'. $filter_response->records['0']->id .' / '. $filter_response->records['0']->account_id_c .'<br>';

            $xEst_Natal = setEstado_Descr( $filter_response->records['0']->estado_c );
            $xCid_Natal = utf8_decode( $filter_response->records['0']->cidade_c ); 


                  // __.-  --=-=- -===--.__.cidade.estado.__.--===- -=-=--  -.__ \\
            $filter_arguments = array(
                  "filter" => array(
                      array(
                        '$and' => array(
                            array(
                                "id" => $xItem->ciuf_cidades_uf_id_c
                            ), 
                        )
                    ) 
                ) 
            );

            $url = $base_url . '/CIUF_cidades_uf/filter';

            $filter_response = call($url, $oauth2_token_response->access_token, 'POST', $filter_arguments);

         // echo '<br>'. $filter_response->records['0']->id .' / '. $filter_response->records['0']->account_id_c .'<br>';

            $xEst_Ender = $filter_response->records['0']->estado_c;
            $xCid_Ender = utf8_decode( $filter_response->records['0']->cidade_c ); 


                  // __.-  --=-=- -===--.__.®.__.--===- -=-=--  -.__ \\


         // echo $xItem->unidade_c . '<br>';
            echo $xItem->registro_academico_c . '<br>';
            echo $xItem->codpessoa_c . '<br>';
         // echo $xItem->first_name . '<br>';
         // echo $xItem->last_name . '<br>';
            echo $xItem->name . '<br>';

            echo $xItem->sexo_c . '<br>';
            $xSexo = setSexo( $xItem->sexo_c );
            $xNascimento = $xItem->birthdate; // $xItem->data_nascimento_c . '<br>';
            echo $xNascimento . '<br>';
         // echo $xItem->nacionalidade_c . '<br>';
            $xNacionalid = setNacionalidade( $xItem->nacionalidade_c );
            echo $xNacionalid . '<br>';

         // echo $xItem->ciuf_cidades_uf_id1_c . '<br>';
            echo $xEst_Natal . '<br>';
         // echo $xCid_Natal . '<br>';

            $xNaturalidad = setEstado_Siglas( $xItem->estado_natal_c );
            echo $xNaturalidad . '<br>';

            echo $xItem->estado_civil_c . '<br>';
            $xEst_Civil = setEstCivil( $xItem->estado_civil_c );
            echo $xItem->tipo_aluno_c . '<br>';
            $xTipo_Aluno = setTipoAluno( $xItem->tipo_aluno_c );

            echo $xItem->rua_c . '<br>';
            echo $xItem->numero_c . '<br>';
            echo $xItem->complemento_c . '<br>';
            echo $xItem->bairro_c . '<br>';
            echo $xItem->cep_c . '<br>';
         // echo $xItem->ciuf_cidades_uf_id_c . '<br>';
            echo $xEst_Ender . '<br>';
            echo $xCid_Ender . '<br>';
         // echo $xItem->email_aluno_c . '<br>';
            echo $xItem->email1 . '<br>';
            echo $xItem->celular_aluno_c . '<br>';
            echo $xItem->telefone_residencial_aluno_c . '<br>';

            echo $xItem->numero_identidade_c . '<br>';
            echo $xItem->orgao_emissor_c . '<br>';
            echo $xItem->data_emissao_c . '<br>';
            $xEst_idEmiss = setEstado_Siglas( $xItem->estado_emissor_c );
            echo $xEst_idEmiss . '<br>';

         // echo $xItem->certidao_nascimento_c . '<br>';
            $xCert_Modelo = setModCertidao( $xItem->certidao_nascimento_c );
            echo $xCert_Modelo . '<br>';
            echo $xItem->numero_certidao_c . '<br>';
            echo $xItem->cartorio_c . '<br>';
            echo $xItem->comarca_c . '<br>';
            echo $xItem->data_certidao_c . '<br>';
            echo $xItem->distrito_c . '<br>';
         // echo $xItem->estado_c . '<br>';
            $xCert_Estado = setEstado_Siglas( $xItem->estado_c );
            echo $xCert_Estado . '<br>';
            echo $xItem->folha_c . '<br>';
            echo $xItem->livro_c . '<br>';
            echo $xItem->numero_serventia_cartorio_c . '<br>';

            echo $xItem->date_entered . '<br>';
            echo $xItem->date_modified . '<br> &nbsp; <br>';

            if ($xItem->date_entered > $strData_Inc) {
               $xFlagProcess = '1';
               $xID_xRA = $xItem->id;
               $xFlag_Inc = true;
            }
            else {
               $xFlagProcess = '0';
               $xID_xRA = $xItem->registro_academico_c;
               $xFlag_Alt = true;
            }

            if ( strlen( $xItem->picture ) > 0 ) {
               $xFlagFoto = 1;

               $url = $base_url .'/Contacts/'. $xItem->id .'/file/picture?platform=base';

               $result = call($url, $oauth2_token_response->access_token, 'GET', array(), true, true);

            // copy($url, './imgsug/b25702ce-9a50-d604-3530-566ac98e2d2e.jpg');
            // echo $result;
               $fp = fopen('./imgsug/'. $xItem->registro_academico_c .'.jpg', 'w+');
               fwrite( $fp, $result );
               fclose( $fp ); 
            } else 
               $xFlagFoto = 0;

            echo '<br>flagFoto: '. $xFlagFoto .'<br>';

            $mopi_query = "INSERT INTO salunopessoas
                              (ra, idSys, tipoAluno, 
                               codPessoa, nome, sexo, dataNasciment, nacionalidade, naturalidade, estadoNatal, estadoCivil, 
                               endRua, endNumero, endComplement, endCEP, endBairro, endCidade, endEstado, 
                               contEMail, contCelular, contTelResidenc, 
                               docIdent_Numero, docIdent_orgEmissor, docIdent_dataEmissao, docIdent_estEmissor, 
                               docCert_Tipo, docCert_Numero, docCert_Cartorio, docCert_Comarca, docCert_NServent, 
                               docCert_Data, docCert_Destrito, docCert_UF, docCert_Folha, docCert_Livro, 
                               flagFoto, flagProcess, dataUpDate)
                           VALUES
                              ('". $xItem->registro_academico_c ."', '". $xItem->id ."', '". $xTipo_Aluno ."', '". 
                           //      $xItem->codpessoa_c ."', '". utf8_decode( $xItem->name ) ."', '". $xSexo ."', '". $xNascimento ."', '". $xNacionalid ."', '". $xItem->estado_natal_c ."', '". $xEst_Natal ."', '". $xCid_Natal ."', '". $xEst_Civil ."', '". 
                                   $xItem->codpessoa_c ."', '". utf8_decode( $xItem->name ) ."', '". $xSexo ."', '". $xNascimento ."', '". $xNacionalid ."', '". $xEst_Natal ."', '". $xNaturalidad ."', '". $xEst_Civil ."', '". 
                                   utf8_decode( $xItem->rua_c ) ."', '". $xItem->numero_c ."', '". utf8_decode( $xItem->complemento_c ) ."', '". $xItem->cep_c ."', '". utf8_decode( $xItem->bairro_c ) ."', '". $xCid_Ender ."', '". $xEst_Ender ."', '". 
                                   $xItem->email1 ."', '". $xItem->celular_aluno_c ."', '". $xItem->telefone_residencial_aluno_c ."', '". 
                                   $xItem->numero_identidade_c ."', '". $xItem->orgao_emissor_c ."', '". $xItem->data_emissao_c ."', '". $xEst_idEmiss ."', '".
                                   $xCert_Modelo ."', '". $xItem->numero_certidao_c ."', '". $xItem->cartorio_c ."', '". $xItem->comarca_c ."', '". $xItem->numero_serventia_cartorio_c ."', '".
                                   $xItem->data_certidao_c ."', '". $xItem->distrito_c ."', '". $xCert_Estado ."', '". $xItem->folha_c ."', '". $xItem->livro_c ."', '".
                                   $xFlagFoto ."', '". $xFlagProcess ."', '". $xItem->date_modified ."') ";

            $xUnidade = setUnidade( $xItem->unidade_c );

            $xTipoMatric = setTipoMatric( $xItem->tipo_matricula_c );
         // $xTipoAluno = setTipoAluno( $xItem->tipo_aluno_c );
            $xCurso = setCurso( $xItem->curso_c );
            $xSerie = setSerie( $xItem->serie_c );
            $xTurno = strtoupper( $xItem->turno_c ); 

            echo $xItem->unidade_c . '<br>';
            echo $xID_xRA . '<br>';
            echo $xTipo_Aluno . ' - '. $xItem->tipo_aluno_c . '<br>';
            echo $xTipoMatric . ' - '. $xItem->tipo_matricula_c . '<br>';
            echo $xItem->ano_letivo_corrente_c . '<br>';
            echo $xCurso . ' - '. $xItem->curso_c . '<br>';
            echo $xSerie . ' - '. $xItem->serie_c . '<br>';
            echo $xItem->turma_c . '<br>';
            echo $xTurno . ' - '. $xItem->turno_c . '<br>';

            if ( ! empty( $xCurso ) ) {
               $mopi_result = mysql_query( $mopi_query );
            // echo '<br>'. $mopi_query .'<br> &nbsp; <br>';

               $mopi_query = "INSERT INTO salunoacademicos
                                 ( unidade, ra, tipoMatricula, tipoAluno, anoLetivo, codCurso, codSerie, turma, turno )
                              VALUES
                                 ( '". $xUnidade ."', '". $xID_xRA ."', '". $xTipoMatric ."', '". $xTipo_Aluno ."', '". $xItem->ano_letivo_corrente_c ."', '". $xCurso ."', '". $xSerie ."', '". 
                                       $xItem->turma_c ."', '". $xTurno ."' )";

               $mopi_result = mysql_query( $mopi_query );
            // echo '<br>'. $mopi_query;

               $xBool_withAtualizac = true;
            } else {
               echo '<br>[nao_matriculado]';
            }

            echo '<hr>';
         }
      }
   }

   $comp_query = '';
   if ( $xBool_withAtualizac ) {
      if ($xFlag_Alt) {
         $comp_query = " alt_Flag = '1' ";
         if ($xFlag_Inc)
            $comp_query .= ", inc_Flag = '1' ";
      } else if ($xFlag_Inc)
         $comp_query = "inc_Flag = '1' ";

      $mopi_query = "UPDATE tabcontrol SET ". $comp_query ." WHERE Processo = 'Alunos_ST' ";

      $mopi_result = mysql_query( $mopi_query ); 
   // echo '<br>'. $mopi_query .'<br>'. $strData_Alt;

      $xBool_withAtualizac = false;
   }

   /*
   echo '<pre>';
      print_r( $filter_response );
   echo '</pre>';
   */

} else {
   echo '<br>- Existe atualizaçao pendente, por favor aguarde...<br>';
}

</script>
