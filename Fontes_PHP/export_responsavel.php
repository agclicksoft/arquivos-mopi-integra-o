
<script language="php">

// header('Content-Type: text/html; charset=utf-8');


$xFrom_charSet = "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ";
$xTo_charSet = "aaaaeeiooouucAAAAEEIOOOUUC";


                  // __.-  --=-=- -===--.__.functions.ini.__.--===- -=-=--  -.__ \\

function call(
    $url,
    $oauthtoken='e380a949-1585-bbd8-ed56-552eb76ada72',
    $type='POST',
    $arguments=array(),
    $encodeData=true,
    $returnHeaders=false
)
{
    $type = strtoupper($type);

    if ($type == 'GET')
    {
        $url .= '?' . http_build_query($arguments);
    }

    $curl_request = curl_init($url);

    if ($type == 'POST')
    {
        curl_setopt($curl_request, CURLOPT_POST, 1);
    }
    elseif ($type == 'PUT')
    {
        curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, 'PUT');
    }
    elseif ($type == 'DELETE')
    {
        curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, 'DELETE');
    }

    curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($curl_request, CURLOPT_HEADER, $returnHeaders);
    curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

    if (!empty($oauthtoken))
    {
        $token = array("oauth-token: {$oauthtoken}");
        curl_setopt($curl_request, CURLOPT_HTTPHEADER, $token);
    }

    if (!empty($arguments) && $type !== 'GET')
    {
        if ($encodeData)
        {
            //encode the arguments as JSON
            $arguments = json_encode($arguments);
        }
        curl_setopt($curl_request, CURLOPT_POSTFIELDS, $arguments);
    }

    $result = curl_exec($curl_request);

    if ($returnHeaders)
    {
        //set headers from response
        list($headers, $content) = explode("\r\n\r\n", $result ,2);
        foreach (explode("\r\n",$headers) as $header)
        {
            header($header);
        }

        //return the nonheader data
        return trim($content);
    }

    curl_close($curl_request);

    //decode the response from JSON
    $response = json_decode($result);

    return $response;
}



function inverteData( $prmData ) {
   $xReturn = '';
   $parteData = explode("/", $prmData);

   if (($parteData[2] != '1900') && ($parteData[2] != '-vazio-'))
    $xReturn = $parteData[2] . "-" . $parteData[1] . "-" . $parteData[0];

   return $xReturn;  
}



function acentRemove( $parmInform ) {
   global $xFrom_charSet, $xTo_charSet;
   $xReturn = '';

   $xInform = trim( $parmInform );
   $xReturn = strtr($xInform, utf8_decode( $xFrom_charSet ), $xTo_charSet);

   return $xReturn;
}



function setSexo( $parmInform ) {
   $xReturn = '';

   if ($parmInform == 'F') 
      $xReturn = 'feminino';
   else
      $xReturn = 'masculino';

   return $xReturn;
}



function setEstado( $parmInform ) {
   $xReturn = '';

   switch ($parmInform) {
   case 'AC':
     $xReturn = 'acre';
     break;
   case 'AL':
     $xReturn = 'alagoas';
     break;
   case 'AM':
     $xReturn = 'amazonas';
     break;
   case 'AP':
     $xReturn = 'amapa';
     break;
   case 'BA':
     $xReturn = 'bahia';
     break;
   case 'CE':
     $xReturn = 'ceara';
     break;
   case 'DF':
     $xReturn = 'distrito_federal';
     break;
   case 'ES':
     $xReturn = 'espirito_santo';
     break;
   case 'GO':
     $xReturn = 'goias';
     break;
   case 'MA':
     $xReturn = 'maranhao';
     break;
   case 'MG':
     $xReturn = 'minas_gerais';
     break;
   case 'MS':
     $xReturn = 'mato_grosso_sul';
     break;
   case 'MT':
     $xReturn = 'mato_grosso';
     break;
   case 'PA':
     $xReturn = 'para';
     break;
   case 'PB':
     $xReturn = 'paraiba';
     break;
   case 'PE':
     $xReturn = 'pernambuco';
     break;
   case 'PI':
     $xReturn = 'piaui';
     break;
   case 'PR':
     $xReturn = 'parana';
     break;
   case 'RJ':
     $xReturn = 'rio_janeiro';
     break;
   case 'RN':
     $xReturn = 'rio_grande_norte';
     break;
   case 'RO':
     $xReturn = 'rondonia';
     break;
   case 'RR':
     $xReturn = 'roraima';
     break;
   case 'RS':
     $xReturn = 'rio_grande_sul';
     break;
   case 'SC':
     $xReturn = 'santa_catarina';
     break;
   case 'SE':
     $xReturn = 'sergipe';
     break;
   case 'SP':
     $xReturn = 'sao_paulo';
     break;
   case 'TO':
     $xReturn = 'tocantins';
     break;
   default:
     $xReturn = 'outro';
   };

   return $xReturn;
}



function checkInfor( $parmInform ) {
   $xReturn = '';

   if ((Empty( $parmInform )) or ($parmInform == '-vazio-'))
      $xReturn = '';
   else
      $xReturn = utf8_encode( $parmInform );

   return $xReturn;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - \\


function setEstCivil( $parmInform ) {
   $xReturn = '';

   switch (strtoupper( $parmInform )) {
   case 'C':
     $xReturn = 'casado';
     break;
   case 'D':
     $xReturn = 'desquitado';
     break;
   case 'I':
     $xReturn = 'divorsiado';
     break;
   case 'O':
     $xReturn = 'separado';
     break;
/* case 'S':
     $xReturn = 'solteiro';
     break; */
   case 'V':
     $xReturn = 'viuvo';
     break;
   default:
     $xReturn = 'solteiro';
   };

   return $xReturn;
}



function setGrauParent( $parmInform ) {
   $xReturn = '';

   switch (strtoupper( $parmInform )) {
   case '1':
     $xReturn = 'filho_valido';
     break;
   case '3':
     $xReturn = 'filho_invalido';
     break;
   case '5':
     $xReturn = 'conjuge';
     break;
   case '6':
     $xReturn = 'pai';
     break;
   case '7':
     $xReturn = 'mae';
     break;
   case '8':
     $xReturn = 'avo';
     break;
   case 'A':
     $xReturn = 'avo';
     break;
   case 'C':
     $xReturn = 'companheiro';
     break;
   case 'D':
     $xReturn = 'enteado';
     break;
   case 'E':
     $xReturn = 'excluido';
     break;
   case 'G':
     $xReturn = 'ex_conjuge';
     break;
   case 'I':
     $xReturn = 'irmao_valido';
     break;
   case 'N':
     $xReturn = 'irmao_invalido';
     break;
   case 'P':
     $xReturn = 'ex_companheiro';
     break;
   case 'S':
     $xReturn = 'ex_sogro';
     break;
   case 'T':
     $xReturn = 'neto';
     break;
   case 'X':
     $xReturn = 'ex_enteado';
     break;
   default:
     $xReturn = 'outros';
   };

   return $xReturn;
}



function setTipoRespons( $parmInform ) {
   $xReturn = '';

   if ($parmInform == 'financeiro') 
      $xReturn = 'financeiro_1';
   else
      $xReturn = 'academico_1';

   return $xReturn;
}

                  // __.-  --=-=- -===--.__.functions.end.__.--===- -=-=--  -.__ \\


$base_url = 'https://mopi1.sugarondemand.com/rest/v10';

//Login - POST /oauth2/token

$url = $base_url . '/oauth2/token';

$oauth2_token_arguments = array(
   'grant_type' => 'password',
   'client_id' => 'sugar',
   'client_secret' => '',
   'username' => 'Alfredo',
   'password' => 'bSa9RW',
   'platform' => 'base'
);

$oauth2_token_response = call($url, '', 'POST', $oauth2_token_arguments); 


                  // __.-  --=-=- -===--.__.®.__.--===- -=-=--  -.__ \\


$mopi_servername = "173.246.40.203";
$mopi_username = "root";
$mopi_password = "c1l2i3c4k5";
$mopi_database = "mopiintegracao";


// Create connection
   $mopi_conn = mysql_connect($mopi_servername, $mopi_username, $mopi_password) or die ("Erro no servidor: ".mysql_error());
   $xBD = mysql_select_db($mopi_database, $mopi_conn) or die ("Erro base: ".mysql_error());




$mopi_query = "
SELECT GROUP_CONCAT(ra) ras, tipoRespons, grauParent, codPessoa, nome, sexo, 
       Left(pess.dataNasciment, 10) dataNasciment, nacionalidade, estadoNatal, estadoCivil, 
       endRua, endNumero, endComplement, endCEP, endBairro, endCidade, endEstado, contEMail, 
       contCelular, contTelResidenc, 
       docIdentNumero, docIdent_orgEmissor, Left(pess.docIdent_dataEmissao, 10) docIdent_dataEmissao, 
       docEstadEmissor, docCPF
  FROM responsavelpessoas pess
-- WHERE ra BETWEEN '2009298' AND '2009299' 
 GROUP BY codPessoa ";
// LIMIT 03 ";
$xCont = 1;

$mopi_result = mysql_query( $mopi_query );

if (mysql_num_rows( $mopi_result ) > 0) {
   while ($mopi_row = mysql_fetch_array( $mopi_result )) {

      $arrayRA_Respons = explode(",", $mopi_row['ras']);
      $xNome = checkInfor( $mopi_row['nome'] );
      $xGrauParent = setGrauParent( $mopi_row['grauParent'] );

      $xSexo = setSexo( $mopi_row['sexo'] );
      $xEstNatal = setEstado( $mopi_row['estadoNatal'] );
      $xDataNasc = inverteData( $mopi_row['dataNasciment'] );

      $xEstCivil = setEstCivil( $mopi_row['estadoCivil'] );

   // $xTipoRespons = 'academico_1'; // $mopi_row['tipoRespons']
      $xTipoRespons = setTipoRespons( $mopi_row['tipoRespons'] );
      $xCidade_UF = 'be0c70b3-7cc5-8351-9c79-54d2ad611b25';
      $xNacionalid = 'brasileira';

      $xEndRua = checkInfor( $mopi_row['endRua'] );
      $xEndComplement = checkInfor( $mopi_row['endComplement'] );
      $xEndBairro = checkInfor( $mopi_row['endBairro'] );

      $xEndCidade = checkInfor( $mopi_row['endCidade'] );
      $xEndEstado = checkInfor( $mopi_row['endEstado'] );
      $xEndNumero = checkInfor( $mopi_row['endNumero'] );
      $xEndCEP = checkInfor( $mopi_row['endCEP'] );

      $xEMail = checkInfor( $mopi_row['contEMail'] );
      $xCelular = checkInfor( $mopi_row['contCelular'] );
      $xTelResidenc = checkInfor( $mopi_row['contTelResidenc'] );

      $xIdent_dataEmissao = inverteData( $mopi_row['docIdent_dataEmissao'] );
      $xIdent_EstEmissor = setEstado( $mopi_row['docEstadEmissor'] );
      $xIdent_OrgEmissor = checkInfor( $mopi_row['docIdent_orgEmissor'] );
      $xIdent_Numero = checkInfor( $mopi_row['docIdentNumero'] );
      $xCPF = checkInfor( $mopi_row['docCPF'] );


      $url = $base_url . '/Accounts';

      $record_arguments = array( 
         "codpessoa_c" => $mopi_row['codPessoa'], 
      // "name" => acentRemove( $mopi_row['nome'] ), 
         "name" => $xNome, 
         "estado_natal_c" => $xEstNatal, 
         "ciuf_cidades_uf_id1_c" => $xCidade_UF,
         "nacionalidade_c" => $xNacionalid, 

      // "data_nascimento_c" => $xDataNasc, 
         "sexo_c" => $xSexo, 
         "estado_civil_c" => $xEstCivil, 

         "rua_c" => $xEndRua, 
         "numero_c" => $xEndNumero, 
         "complemento_c" => $xEndComplement, 
         "cep_c" => $xEndCEP, 
         "bairro_c" => $xEndBairro, 
         "ciuf_cidades_uf_id_c" => $xCidade_UF, 

         "email_c" => $xEMail, 
         "celular_c" => $xCelular, 
         "telefone_residencial_c" => $xTelResidenc, 

         "numero_carteira_identidade_c" => $xIdent_Numero, 
         "orgao_emissor_c" => $xIdent_OrgEmissor, 
      // "data_emissao_c" => $xIdent_dataEmissao, 
      // "estado_emissor_c" => $xIdent_EstEmissor, 
         "cpf_c" => $xCPF, 

         "tipo_responsaveis_c" => $xTipoRespons,
         "grau_parentesco_c" => $xGrauParent 
      );

      echo '<br> __.-  --=-=- -===--.__.®.__.--===- -=-=--  -.__ <br>';

      echo "<pre>";
      print_r($record_arguments);
      echo "</pre>";

      $record_response = call($url, $oauth2_token_response->access_token, 'POST', $record_arguments);

      echo '<br>'. $xCont++ .' ) &nbsp; '. $record_response->id .' / '. $record_response->name .'<br>';
      if ( empty($record_response->id) )
         exit();
      $xID_Responsavel = $record_response->id;

      // _  __  _  __.- - --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=-- - -.__  _  __  _ \\

      foreach ($arrayRA_Respons as $xRA_Responsavel) {
         //Filter - /<module>/filter POST
         $filter_arguments = array(
             "filter" => array(
                   array(
                     '$or' => array(
                         array(
                             //or name is equal to 'My Account'
                             "registro_academico_c" => $xRA_Responsavel
                         )
                     ),
                 ), 
             ),

          // 'max_num' => 05,
          // 'offset' => 04,
          // 'fields' => 'full_name, unidade_c, registro_academico_c',
          // 'order_by' => 'name:DESC',
          // 'favorites' => false,
          // 'my_items' => false
         );


         $url = $base_url . '/Contacts/filter';

         $filter_response = call($url, $oauth2_token_response->access_token, 'POST', $filter_arguments);

         echo '<br>'. $filter_response->records['0']->id .' / '. $filter_response->records['0']->first_name .'<br>';


         // _  __  _  __.- - --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=-- - -.__  _  __  _ \\


         //upDate Record - PUT /<module>/:record
         $url = $base_url . '/Contacts/'. $filter_response->records['0']->id;

         $record_arguments = array(
            "account_id" => $xID_Responsavel,
            "account_id_c" => $xID_Responsavel
         );

         $record_response = call($url, $oauth2_token_response->access_token, 'PUT', $record_arguments);
      }

      //upDate Record - PUT /<module>/:record
      $url = $base_url . '/Accounts/'. $xID_Responsavel;

      $record_arguments = array(
         "contacts_accounts_1contacts_ida" => $filter_response->records['0']->id
      );

      $record_response = call($url, $oauth2_token_response->access_token, 'PUT', $record_arguments);
   }
}

</script>
