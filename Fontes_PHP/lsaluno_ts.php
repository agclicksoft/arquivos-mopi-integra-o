
<script language="php">

// header('Content-Type: text/html; charset=utf-8');

ini_set('display_errors', 1);

$xFirstName = chr(32);
$xLastName = chr(32);
$xNat_estCidade = chr(32);
$xEnd_estCidade = 'be0c70b3-7cc5-8351-9c79-54d2ad611b25';
$xFrom_charSet = "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ";
$xTo_charSet = "aaaaeeiooouucAAAAEEIOOOUUC";


                  // __.-  --=-=- -===--.__.functions.ini.__.--===- -=-=--  -.__ \\

function call(
    $url,
    $oauthtoken='e380a949-1585-bbd8-ed56-552eb76ada72',
    $type='POST',
    $arguments=array(),
    $encodeData=true,
    $returnHeaders=false
)
{
    $type = strtoupper($type);

    if ($type == 'GET')
    {
        $url .= '?' . http_build_query($arguments);
    }

    $curl_request = curl_init($url);

    if ($type == 'POST')
    {
        curl_setopt($curl_request, CURLOPT_POST, 1);
    }
    elseif ($type == 'PUT')
    {
        curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, 'PUT');
    }
    elseif ($type == 'DELETE')
    {
        curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, 'DELETE');
    }

    curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($curl_request, CURLOPT_HEADER, $returnHeaders);
    curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

    if (!empty($oauthtoken))
    {
        $token = array("oauth-token: {$oauthtoken}");
        curl_setopt($curl_request, CURLOPT_HTTPHEADER, $token);
    }

    if (!empty($arguments) && $type !== 'GET')
    {
        if ($encodeData)
        {
            //encode the arguments as JSON
            $arguments = json_encode($arguments);
        }
        curl_setopt($curl_request, CURLOPT_POSTFIELDS, $arguments);
    }

    $result = curl_exec($curl_request);

    if ($returnHeaders)
    {
        //set headers from response
        list($headers, $content) = explode("\r\n\r\n", $result ,2);
        foreach (explode("\r\n",$headers) as $header)
        {
            header($header);
        }

        //return the nonheader data
        return trim($content);
    }

    curl_close($curl_request);

    //decode the response from JSON
    $response = json_decode($result);

    return $response;
}



function inverteData( $prmData ) {
   $xReturn = '';
   $parteData = explode("/", $prmData);

   if (($parteData[2] != '1900') && ($parteData[2] != '-vazio-'))
      $xReturn = $parteData[2] . "-" . $parteData[1] . "-" . $parteData[0];

   return $xReturn;  
}



function acentRemove( $parmInform ) {
   global $xFrom_charSet, $xTo_charSet;
   $xReturn = '';

   $xInform = trim( $parmInform );
   $xReturn = strtr($xInform, utf8_decode( $xFrom_charSet ), $xTo_charSet);

   return $xReturn;
}



function setFirst_Lastname( $parmFullName ) {
   global $xFirstName, $xLastName;
   $xFirstName = '';
   $xLastName = '';

   $xFullName = str_replace( '*', '', $parmFullName);
   $xFullName = utf8_encode( $xFullName );

   $xPos = strpos($xFullName, chr(32));
   $xFirstName = substr( $xFullName, 0, $xPos );
   $xLastName = substr( $xFullName, $xPos + 1 );
}



function checkInfor( $parmInform ) {
   $xReturn = '';

   if ((Empty( $parmInform )) or ($parmInform == '-vazio-'))
      $xReturn = '';
   else
      $xReturn = utf8_encode( $parmInform );

   return $xReturn;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - \\


function setSexo( $parmInform ) {
   $xReturn = '';

   if ($parmInform == 'F') 
      $xReturn = 'feminino';
   else
      $xReturn = 'masculino';

   return $xReturn;
}



function setEstado( $parmInform ) {
   $xReturn = '';

   switch ($parmInform) {
   case 'AC':
     $xReturn = 'acre';
     break;
   case 'AL':
     $xReturn = 'alagoas';
     break;
   case 'AM':
     $xReturn = 'amazonas';
     break;
   case 'AP':
     $xReturn = 'amapa';
     break;
   case 'BA':
     $xReturn = 'bahia';
     break;
   case 'CE':
     $xReturn = 'ceara';
     break;
   case 'DF':
     $xReturn = 'distrito_federal';
     break;
   case 'ES':
     $xReturn = 'espirito_santo';
     break;
   case 'GO':
     $xReturn = 'goias';
     break;
   case 'MA':
     $xReturn = 'maranhao';
     break;
   case 'MG':
     $xReturn = 'minas_gerais';
     break;
   case 'MS':
     $xReturn = 'mato_grosso_sul';
     break;
   case 'MT':
     $xReturn = 'mato_grosso';
     break;
   case 'PA':
     $xReturn = 'para';
     break;
   case 'PB':
     $xReturn = 'paraiba';
     break;
   case 'PE':
     $xReturn = 'pernambuco';
     break;
   case 'PI':
     $xReturn = 'piaui';
     break;
   case 'PR':
     $xReturn = 'parana';
     break;
   case 'RJ':
     $xReturn = 'rio_janeiro';
     break;
   case 'RN':
     $xReturn = 'rio_grande_norte';
     break;
   case 'RO':
     $xReturn = 'rondonia';
     break;
   case 'RR':
     $xReturn = 'roraima';
     break;
   case 'RS':
     $xReturn = 'rio_grande_sul';
     break;
   case 'SC':
     $xReturn = 'santa_catarina';
     break;
   case 'SE':
     $xReturn = 'sergipe';
     break;
   case 'SP':
     $xReturn = 'sao_paulo';
     break;
   case 'TO':
     $xReturn = 'tocantins';
     break;
   default:
     $xReturn = 'outro';
   };

   return $xReturn;
}



function setEstCivil( $parmInform ) {
   $xReturn = '';

   switch (strtoupper( $parmInform )) {
   case 'C':
     $xReturn = 'casado';
     break;
   case 'D':
     $xReturn = 'desquitado';
     break;
   case 'I':
     $xReturn = 'divorsiado';
     break;
   case 'O':
     $xReturn = 'separado';
     break;
/* case 'S':
     $xReturn = 'solteiro';
     break; */
   case 'V':
     $xReturn = 'viuvo';
     break;
   default:
     $xReturn = 'solteiro';
   };

   return $xReturn;
}



function setTipoAluno( $parmInform ) {
   $xReturn = '';

   switch (strtolower( $parmInform )) {
/* case 'cursando':
     $xReturn = '1';
     break; */
   case '2':
     $xReturn = 'cancelado';
     break;
   case '3':
     $xReturn = 'cancelado';
     break;
   case '4':
     $xReturn = 'nao_renovado';
     break;
   case '5':
     $xReturn = 'em_rematricula';
     break; 
   case '6':
     $xReturn = 'prospect';
     break;
   case '7':
     $xReturn = 'prospect_cancelado';
     break;
   default:
     $xReturn = 'cursando';
   };

   return $xReturn;
}



function setUnidade( $parmInform ) {
   $xReturn = '';

   if ($parmInform == 'tijuca') 
      $xReturn = 'matriz';
   else
      $xReturn = 'filial_itanhanga';

   return $xReturn;
}



function xFunc_incTime( $parmInform ) {
   $xReturn = '';

   $xHora = substr( $parmInform, 0, 2 );
   $xMinut = substr( $parmInform, 3, 2 );
   $xSegund = substr( $parmInform, 6, 2 );

   $tSegund = (intval( $xSegund ) + 30);
   if ( $tSegund > 59 ) {
      $xSegund = ($tSegund - 60);

      $tMinut = (intval( $xMinut ) + 1);
      if ( $tMinut > 59 ) {
         $xMinut = ($tMinut - 60);

         $tHora = (intval( $xHora ) + 1);
         if ( $tHora > 23 ) {
            $xHora = ($tHora - 24);
         } else {
            $xHora = $tHora;
         }
      } else {
         $xMinut = $tMinut;
      }

   } else {
      $xSegund = $tSegund;
   }

   $xTemp = '0'. $xSegund;
   $xSegund = substr( $xTemp, -2, 2 );
   $xTemp = '0'. $xMinut;
   $xMinut = substr( $xTemp, -2, 2 );
   $xTemp = '0'. $xHora;
   $xHora = substr( $xTemp, -2, 2 );

   $xReturn = $xHora .':'. $xMinut .':'. $xSegund;

   return $xReturn;
}

                  // __.-  --=-=- -===--.__.functions.end.__.--===- -=-=--  -.__ \\


$base_url = 'https://mopi1.sugarondemand.com/rest/v10';

//Login - POST /oauth2/token

$url = $base_url . '/oauth2/token';

$oauth2_token_arguments = array(
   'grant_type' => 'password',
   'client_id' => 'sugar',
   'client_secret' => '',
   'username' => 'Alfredo',
   'password' => 'bSa9RW',
   'platform' => 'base'
);

$oauth2_token_response = call($url, '', 'POST', $oauth2_token_arguments); 


                  // __.-  --=-=- -===--.__.®.__.--===- -=-=--  -.__ \\


/* [ webbynode ]
$mopi_servername = "173.246.40.203";
$mopi_username = "root";
$mopi_password = "c1l2i3c4k5";
$mopi_database = "mopiintegracao";
*/
$mopi_servername = "199.201.89.205";
$mopi_username = "clicksof_mopi";
$mopi_password = "clicksof.mopi";
$mopi_database = "clicksof_mopi_integracao";


// Create connection
   $mopi_conn = mysql_connect($mopi_servername, $mopi_username, $mopi_password) or die ("Erro no servidor: ".mysql_error());
   $xBD = mysql_select_db($mopi_database, $mopi_conn) or die ("Erro base: ".mysql_error());


                  // __.-  --=-=- -===--.__.®.__.--===- -=-=--  -.__ \\

$mopi_query = "SELECT * FROM talunopessoas";

$mopi_result = mysql_query( $mopi_query );

$xData_Inc = '0000-00-00T00:00:00';
$xData_Alt = '0000-00-00T00:00:00';

if (mysql_num_rows( $mopi_result ) > 0) {
   while ($mopi_row = mysql_fetch_array( $mopi_result )) {

      // _  __  _  __.- -=- -===--.__.cidadeNatal.estadoNatal.__.--===- -=- -.__  _  __  _ \\
      $filter_arguments = array(
            "filter" => array(
                array(
                  '$and' => array(
                      array(
                          "cidade_c" => $mopi_row['cidadeNatal']
                      ), 
                  )
              ) 
          ) 
      );

      $url = $base_url . '/CIUF_cidades_uf/filter';

      $filter_response = call($url, $oauth2_token_response->access_token, 'POST', $filter_arguments);

      $xNat_estCidade = $filter_response->records['0']->id;
      // _  __  _  __.- - --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=-- - -.__  _  __  _ \\

      $xUnidade = setUnidade( $mopi_row['unidade'] );
      setFirst_Lastname( $mopi_row['nome'] );

      $xSexo = setSexo( $mopi_row['sexo'] );
      $xDataNasc = inverteData( $mopi_row['dataNasciment'] );

      $xEstCivil = setEstCivil( $mopi_row['estadoCivil'] );
      $xTipoAlino = setTipoAluno( $mopi_row['tipoAluno'] );

      $xNacionalid = 'brasileira';

      $xEndRua = $mopi_row['endRua'];
      $xEndComplement = $mopi_row['endComplement'];
      $xEndBairro = $mopi_row['endBairro'];

   // $xEndCidade = checkInfor( $mopi_row['endCidade'] );
   // $xEndEstado = checkInfor( $mopi_row['endEstado'] );
      $xEndNumero = $mopi_row['endNumero'];
      $xEndCEP = $mopi_row['endCEP'];

      $xEMail = $mopi_row['contEMail'];
      $xCelular = $mopi_row['contCelular'];
      $xTelResidenc = $mopi_row['contTelResidenc'];

      $xIdent_dataEmissao = inverteData( $mopi_row['docIdent_dataEmissao'] );
      $xIdent_EstEmissor = setEstado( $mopi_row['docEstadEmissor'] );
      $xIdent_OrgEmissor = $mopi_row['docIdent_orgEmissor'];
      $xIdent_Numero = $mopi_row['docIdentNumero'];
      $xCPF = $mopi_row['docCPF'];

      $record_arguments = array( 
         "registro_academico_c" => $mopi_row['ra'], 
         "codpessoa_c" => $mopi_row['codPessoa'], 
         "unidade_c" => $xUnidade, 

         "first_name" => $xFirstName, 
         "last_name" => $xLastName, 

         "estado_natal_c" => $xEstNatal, 
         "ciuf_cidades_uf_id1_c" => $xNat_estCidade,
         "nacionalidade_c" => $xNacionalid, 

         "data_nascimento_c" => $xDataNasc,
         "birthdate" => $xDataNasc,
          
         "sexo_c" => $xSexo, 
         "estado_civil_c" => $xEstCivil, 
         "tipo_aluno_c" => $xTipoAlino, 

         "rua_c" => $xEndRua, 
         "numero_c" => $xEndNumero, 
         "complemento_c" => $xEndComplement, 
         "cep_c" => $xEndCEP, 
         "bairro_c" => $xEndBairro, 
         "ciuf_cidades_uf_id_c" => $xEnd_estCidade, 

         "email_aluno_c" => $xEMail, 
         "celular_aluno_c" => $xCelular, 
         "telefone_residencial_aluno_c" => $xTelResidenc,

         "numero_identidade_c" => $xIdent_Numero, 
         "orgao_emissor_c" => $xIdent_OrgEmissor, 
         "data_emissao_c" => $xIdent_dataEmissao, 
         "estado_emissor_c" => $xIdent_EstEmissor, 
         "cpf_c" => $xCPF
      );

      echo "<pre>";
         print_r($record_arguments);
      echo "</pre>"; 

   }

}

</script>
