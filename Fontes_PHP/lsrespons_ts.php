
<script language="php">

// header('Content-Type: text/html; charset=utf-8');

   ini_set( 'display_errors', 1 );
// ini_set( 'display_startup_erros', 1 );
// error_reporting( E_ALL );

$xFirstName = chr(32);
$xLastName = chr(32);
$xFrom_charSet = "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ";
$xTo_charSet = "aaaaeeiooouucAAAAEEIOOOUUC";


                  // __.-  --=-=- -===--.__.functions.ini.__.--===- -=-=--  -.__ \\

function call(
    $url,
    $oauthtoken='e380a949-1585-bbd8-ed56-552eb76ada72',
    $type='POST',
    $arguments=array(),
    $encodeData=true,
    $returnHeaders=false
)
{
    $type = strtoupper($type);

    if ($type == 'GET')
    {
        $url .= '?' . http_build_query($arguments);
    }

    $curl_request = curl_init($url);

    if ($type == 'POST')
    {
        curl_setopt($curl_request, CURLOPT_POST, 1);
    }
    elseif ($type == 'PUT')
    {
        curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, 'PUT');
    }
    elseif ($type == 'DELETE')
    {
        curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, 'DELETE');
    }

    curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($curl_request, CURLOPT_HEADER, $returnHeaders);
    curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

    if (!empty($oauthtoken))
    {
        $token = array("oauth-token: {$oauthtoken}");
        curl_setopt($curl_request, CURLOPT_HTTPHEADER, $token);
    }

    if (!empty($arguments) && $type !== 'GET')
    {
        if ($encodeData)
        {
            //encode the arguments as JSON
            $arguments = json_encode($arguments);
        }
        curl_setopt($curl_request, CURLOPT_POSTFIELDS, $arguments);
    }

    $result = curl_exec($curl_request);

    if ($returnHeaders)
    {
        //set headers from response
        list($headers, $content) = explode("\r\n\r\n", $result ,2);
        foreach (explode("\r\n",$headers) as $header)
        {
            header($header);
        }

        //return the nonheader data
        return trim($content);
    }

    curl_close($curl_request);

    //decode the response from JSON
    $response = json_decode($result);

    return $response;
}



function inverteData( $prmData ) {
   $xReturn = '0';
   $parteData = explode("/", $prmData);

   if (($parteData[2] != '1900') && ($parteData[2] != '-vazio-'))
      $xReturn = $parteData[2] . "-" . $parteData[1] . "-" . $parteData[0];

   return $xReturn;  
}



function acentRemove( $parmInform ) {
   global $xFrom_charSet, $xTo_charSet;
   $xReturn = '';

   $xInform = trim( $parmInform );
   $xReturn = strtr($xInform, utf8_decode( $xFrom_charSet ), $xTo_charSet);

   return $xReturn;
}



function setSexo( $parmInform ) {
   $xReturn = '';

   if ($parmInform == 'F') 
      $xReturn = 'feminino';
   else
      $xReturn = 'masculino';

   return $xReturn;
}



function setEstado( $parmInform ) {
   $xReturn = '';

   switch ($parmInform) {
   case 'AC':
     $xReturn = 'acre';
     break;
   case 'AL':
     $xReturn = 'alagoas';
     break;
   case 'AM':
     $xReturn = 'amazonas';
     break;
   case 'AP':
     $xReturn = 'amapa';
     break;
   case 'BA':
     $xReturn = 'bahia';
     break;
   case 'CE':
     $xReturn = 'ceara';
     break;
   case 'DF':
     $xReturn = 'distrito_federal';
     break;
   case 'ES':
     $xReturn = 'espirito_santo';
     break;
   case 'GO':
     $xReturn = 'goias';
     break;
   case 'MA':
     $xReturn = 'maranhao';
     break;
   case 'MG':
     $xReturn = 'minas_gerais';
     break;
   case 'MS':
     $xReturn = 'mato_grosso_sul';
     break;
   case 'MT':
     $xReturn = 'mato_grosso';
     break;
   case 'PA':
     $xReturn = 'para';
     break;
   case 'PB':
     $xReturn = 'paraiba';
     break;
   case 'PE':
     $xReturn = 'pernambuco';
     break;
   case 'PI':
     $xReturn = 'piaui';
     break;
   case 'PR':
     $xReturn = 'parana';
     break;
   case 'RJ':
     $xReturn = 'rio_janeiro';
     break;
   case 'RN':
     $xReturn = 'rio_grande_norte';
     break;
   case 'RO':
     $xReturn = 'rondonia';
     break;
   case 'RR':
     $xReturn = 'roraima';
     break;
   case 'RS':
     $xReturn = 'rio_grande_sul';
     break;
   case 'SC':
     $xReturn = 'santa_catarina';
     break;
   case 'SE':
     $xReturn = 'sergipe';
     break;
   case 'SP':
     $xReturn = 'sao_paulo';
     break;
   case 'TO':
     $xReturn = 'tocantins';
     break;
   default:
     $xReturn = 'outro';
   };

   return $xReturn;
}



function checkInfor( $parmInform ) {
   $xReturn = '';

   if ((Empty( $parmInform )) or ($parmInform == '-vazio-'))
      $xReturn = '';
   else
      $xReturn = utf8_encode( $parmInform );

   return $xReturn;
}


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - \\


function setEstCivil( $parmInform ) {
   $xReturn = '';

   switch (strtoupper( $parmInform )) {
   case 'C':
     $xReturn = 'casado';
     break;
   case 'D':
     $xReturn = 'desquitado';
     break;
   case 'I':
     $xReturn = 'divorsiado';
     break;
   case 'O':
     $xReturn = 'separado';
     break;
/* case 'S':
     $xReturn = 'solteiro';
     break; */
   case 'V':
     $xReturn = 'viuvo';
     break;
   default:
     $xReturn = 'solteiro';
   };

   return $xReturn;
}



function setGrauParent( $parmInform ) {
   $xReturn = '';

   switch (strtoupper( $parmInform )) {
   case '1':
     $xReturn = 'filho_valido';
     break;
   case '3':
     $xReturn = 'filho_invalido';
     break;
   case '5':
     $xReturn = 'conjuge';
     break;
   case '6':
     $xReturn = 'pai';
     break;
   case '7':
     $xReturn = 'mae';
     break;
   case '8':
     $xReturn = 'avo';
     break;
   case 'A':
     $xReturn = 'avo';
     break;
   case 'C':
     $xReturn = 'companheiro';
     break;
   case 'D':
     $xReturn = 'enteado';
     break;
   case 'E':
     $xReturn = 'excluido';
     break;
   case 'G':
     $xReturn = 'ex_conjuge';
     break;
   case 'I':
     $xReturn = 'irmao_valido';
     break;
   case 'N':
     $xReturn = 'irmao_invalido';
     break;
   case 'P':
     $xReturn = 'ex_companheiro';
     break;
   case 'S':
     $xReturn = 'ex_sogro';
     break;
   case 'T':
     $xReturn = 'neto';
     break;
   case 'X':
     $xReturn = 'ex_enteado';
     break;
   default:
     $xReturn = 'outros';
   };

   return $xReturn;
}



function setTipoRespons( $parmInform ) {
   $xReturn = '';

   if ($parmInform == 'financeiro') 
      $xReturn = 'financeiro_1';
   else
      $xReturn = 'academico_1';

   return $xReturn;
}



function xFunc_incTime( $parmInform ) {
   $xReturn = '';

   $xHora = substr( $parmInform, 0, 2 );
   $xMinut = substr( $parmInform, 3, 2 );
   $xSegund = substr( $parmInform, 6, 2 );

   $tSegund = (intval( $xSegund ) + 30);
   if ( $tSegund > 59 ) {
      $xSegund = ($tSegund - 60);

      $tMinut = (intval( $xMinut ) + 1);
      if ( $tMinut > 59 ) {
         $xMinut = ($tMinut - 60);

         $tHora = (intval( $xHora ) + 1);
         if ( $tHora > 23 ) {
            $xHora = ($tHora - 24);
         } else {
            $xHora = $tHora;
         }
      } else {
         $xMinut = $tMinut;
      }

   } else {
      $xSegund = $tSegund;
   }

   $xTemp = '0'. $xSegund;
   $xSegund = substr( $xTemp, -2, 2 );
   $xTemp = '0'. $xMinut;
   $xMinut = substr( $xTemp, -2, 2 );
   $xTemp = '0'. $xHora;
   $xHora = substr( $xTemp, -2, 2 );

   $xReturn = $xHora .':'. $xMinut .':'. $xSegund;

   return $xReturn;
}

                  // __.-  --=-=- -===--.__.functions.end.__.--===- -=-=--  -.__ \\


$base_url = 'https://mopi1.sugarondemand.com/rest/v10';

//Login - POST /oauth2/token

$url = $base_url . '/oauth2/token';

$oauth2_token_arguments = array(
   'grant_type' => 'password',
   'client_id' => 'sugar',
   'client_secret' => '',
   'username' => 'Alfredo',
   'password' => 'bSa9RW',
   'platform' => 'base'
);

$oauth2_token_response = call($url, '', 'POST', $oauth2_token_arguments); 


                  // __.-  --=-=- -===--.__.®.__.--===- -=-=--  -.__ \\

/* [ webbynode ]
$mopi_servername = "173.246.40.203";
$mopi_username = "root";
$mopi_password = "c1l2i3c4k5";
$mopi_database = "mopiintegracao";
*/
$mopi_servername = "199.201.89.205";
$mopi_username = "clicksof_mopi";
$mopi_password = "clicksof.mopi";
$mopi_database = "clicksof_mopi_integracao";


// Create connection
   $mopi_conn = mysql_connect($mopi_servername, $mopi_username, $mopi_password) or die ("Erro no servidor: ".mysql_error());
   $xBD = mysql_select_db($mopi_database, $mopi_conn) or die ("Erro base: ".mysql_error());


                  // __.-  --=-=- -===--.__.®.__.--===- -=-=--  -.__ \\


$mopi_query = "
   SELECT GROUP_CONCAT(ra) ras, codPessoa, nome, sexo, nacionalidade, estadoCivil, 
          flagAcademico, flagFinanceiro, grauParent, 
          endRua, endNumero, endComplement, endCEP, endBairro, endCidade, endEstado, 
          contEMail, contCelular, contTelResidenc, 
          docIdentNumero, docIdent_orgEmissor, docIdent_dataEmissao, docEstadEmissor, docCPF, 
          flagProcess, dataUpDate
     FROM tresponsavelpessoas
    GROUP BY codPessoa ";

$mopi_result = mysql_query( $mopi_query );

$xData_Inc = '0000-00-00T00:00:00';
$xData_Alt = '0000-00-00T00:00:00';

if (mysql_num_rows( $mopi_result ) > 0) {
   while ($mopi_row = mysql_fetch_array( $mopi_result )) {
      $arrayRespons = array();

      $arrayRA_Respons = explode(",", $mopi_row['ras']);
      $xNome = checkInfor( $mopi_row['nome'] );
   // setFirst_Lastname( $mopi_row['nome'] );

      $xSexo = setSexo( $mopi_row['sexo'] );
   // $xEstNatal = setEstado( $mopi_row['estadoNatal'] );
   // $xDataNasc = inverteData( $mopi_row['dataNasciment'] );

      $xEstCivil = setEstCivil( $mopi_row['estadoCivil'] );
      $xGrauParent = setGrauParent( $mopi_row['grauParent'] );
   // $xTipoRespons = 'academico_1'; // $mopi_row['tipoRespons']
   // $xTipoRespons = setTipoRespons( $mopi_row['tipoRespons'] );
      if ( $mopi_row['flagAcademico'] == '1' )
         $arrayRespons = array_merge($arrayRespons, array( "0" => "academico_1" ));
      if ( $mopi_row['flagFinanceiro'] == '1' )
         $arrayRespons = array_merge($arrayRespons, array( "0" => "financeiro_1" ));
      echo '<pre>';
         print_r( $arrayRespons );
      echo '</pre>';

      $xCidade_UF = 'be0c70b3-7cc5-8351-9c79-54d2ad611b25';
      $xNacionalid = 'brasileira';

      $xEndRua = checkInfor( $mopi_row['endRua'] );
      $xEndComplement = checkInfor( $mopi_row['endComplement'] );
      $xEndBairro = checkInfor( $mopi_row['endBairro'] );

   // $xEndCidade = checkInfor( $mopi_row['endCidade'] );
   // $xEndEstado = checkInfor( $mopi_row['endEstado'] );
      $xEndNumero = checkInfor( $mopi_row['endNumero'] );
      $xEndCEP = checkInfor( $mopi_row['endCEP'] );

      $xEMail = checkInfor( $mopi_row['contEMail'] );
      $xCelular = checkInfor( $mopi_row['contCelular'] );
      $xTelResidenc = checkInfor( $mopi_row['contTelResidenc'] );

   // $xIdent_dataEmissao = inverteData( $mopi_row['docIdent_dataEmissao'] );
      $xIdent_EstEmissor = setEstado( $mopi_row['docEstadEmissor'] );
      $xIdent_OrgEmissor = checkInfor( $mopi_row['docIdent_orgEmissor'] );
      $xIdent_Numero = checkInfor( $mopi_row['docIdentNumero'] );
      $xCPF = checkInfor( $mopi_row['docCPF'] );

      $record_arguments = array( 
         "codpessoa_c" => $mopi_row['codPessoa'], 
      // "unidade_c" => $mopi_row['unidade'], 

         "name" => $xNome, 

      // "estado_natal_c" => $xEstNatal, 
         "ciuf_cidades_uf_id1_c" => $xCidade_UF,
         "nacionalidade_c" => $xNacionalid, 

      // "data_nascimento_c" => $xDataNasc, 
         "sexo_c" => $xSexo, 
         "estado_civil_c" => $xEstCivil, 

         "rua_c" => $xEndRua, 
         "numero_c" => $xEndNumero, 
         "complemento_c" => $xEndComplement, 
         "cep_c" => $xEndCEP, 
         "bairro_c" => $xEndBairro, 
         "ciuf_cidades_uf_id_c" => $xCidade_UF, 

         "email_c" => $xEMail, 
         "celular_c" => $xCelular, 
         "telefone_residencial_c" => $xTelResidenc, 

         "numero_carteira_identidade_c" => $xIdent_Numero, 
         "orgao_emissor_c" => $xIdent_OrgEmissor, 
      // "data_emissao_c" => $xIdent_dataEmissao, 
         "estado_emissor_c" => $xIdent_EstEmissor, 
         "cpf_c" => $xCPF,

         "tipo_responsavel_c" => $arrayRespons,
         "grau_parentesco_c" => $xGrauParent 
      );

      if ( strlen( $mopi_row['docIdent_dataEmissao'] ) >= 10 ) {
         $xIdent_dataEmissao = inverteData( $mopi_row['docIdent_dataEmissao'] );
         if ($xIdent_dataEmissao != '0') 
            $record_arguments = array_merge($record_arguments, array( "data_emissao_c" => $xIdent_dataEmissao ));
      } 

      echo "<pre>";
         print_r($record_arguments);
      echo "</pre>"; 

      // _  __  _  __.- - --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=-- - -.__  _  __  _ \\

      $strData_upAluno = '0000-00-00T00:00:00';
      if ($mopi_row['flagProcess'] == '1') {
         $url = $base_url . '/Accounts';

         $record_response = call($url, $oauth2_token_response->access_token, 'POST', $record_arguments);

         echo '<br>new.Reg.: '. $record_response->id .' / '. $record_response->name .'<br>';

         $xData_Inc = substr( $record_response->date_entered, 0, 19 ); // date_entered <- para inclusao

         $xID_Responsavel = $record_response->id;

         foreach ($arrayRA_Respons as $xRA_Responsavel) {
            //Filter - /<module>/filter POST
            $filter_arguments = array(
                "filter" => array(
                      array(
                        '$or' => array(
                            array(
                                //or name is equal to 'My Account'
                                "registro_academico_c" => $xRA_Responsavel
                            )
                        ),
                    ), 
                ),

             // 'max_num' => 05,
             // 'offset' => 04,
             // 'fields' => 'full_name, unidade_c, registro_academico_c',
             // 'order_by' => 'name:DESC',
             // 'favorites' => false,
             // 'my_items' => false
            );

            $url = $base_url . '/Contacts/filter';

            $filter_response = call($url, $oauth2_token_response->access_token, 'POST', $filter_arguments);

            echo '<br>'. $filter_response->records['0']->id .' / '. $filter_response->records['0']->first_name .'<br>';

            // _  __  _  __.- - --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=-- - -.__  _  __  _ \\

            if (isSet( $filter_response->records['0']->id )) {
               //upDate Record - PUT /<module>/:record
               $url = $base_url . '/Contacts/'. $filter_response->records['0']->id;

               $record_arguments = array(
                  "account_id_c" => $xID_Responsavel,
                  "account_id" => $xID_Responsavel
               );

               $record_response = call($url, $oauth2_token_response->access_token, 'PUT', $record_arguments);

               if (isSet( $record_response->date_modified )) 
                  $strData_upAluno = substr( $record_response->date_modified, 0, 19 );
            }

         }

         //upDate Record - PUT /<module>/:record
         $url = $base_url . '/Accounts/'. $xID_Responsavel;

         $record_arguments = array(
            "contacts_accounts_1contacts_ida" => $filter_response->records['0']->id
         );

         $record_response = call($url, $oauth2_token_response->access_token, 'PUT', $record_arguments);

         $xData_Alt = substr( $record_response->date_modified, 0, 19 );

      } else {
         $filter_arguments = array(
             "filter" => array(
                   array(
                     '$or' => array(
                         array(
                             //or name is equal to 'My Account'
                             "codpessoa_c" => $mopi_row['codPessoa']
                         )
                     ),
                 ), 
             ),
          // 'max_num' => 05,
          // 'offset' => 04,
          // 'fields' => 'full_name, unidade_c, registro_academico_c',
          // 'order_by' => 'name:DESC',
          // 'favorites' => false,
          // 'my_items' => false
         );


         $url = $base_url . '/Accounts/filter';

         $filter_response = call($url, $oauth2_token_response->access_token, 'POST', $filter_arguments);

         $tempData = substr( $filter_response->records['0']->date_modified, 0, 19 );
         $strData_M = str_replace('T', chr(32), $tempData);      
         $tempData = substr( $mopi_row['dataUpDate'], 0, 10 );
         $strData_T = inverteData( $tempData ) .chr(32). substr( $mopi_row['dataUpDate'], -8 ); 

         echo '<br>'. $filter_response->records['0']->id .' &nbsp; . . . &nbsp; '. $filter_response->records['0']->name .'<br>';
         echo '<br>'. $filter_response->records['0']->date_modified .' &nbsp; . . . &nbsp; '. $mopi_row['dataUpDate'] .'<br>';
         echo '<br>'. $strData_M .' &nbsp; . . . &nbsp; '. $strData_T;


         // _  __  _  __.- - --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=-- - -.__  _  __  _ \\


         if ((isSet( $filter_response->records['0']->id )) && ($strData_T > $strData_M)) {
            $url = $base_url . '/Accounts/'. $filter_response->records['0']->id; 

            echo ' &nbsp; . . . &nbsp; upDate';

            $record_response = call($url, $oauth2_token_response->access_token, 'PUT', $record_arguments);

         /* echo '<pre>';
               print_r($record_response);
            echo '</pre>'; */

      // if (isSet( $record_response->date_modified )) 
            $xData_Alt = substr( $record_response->date_modified, 0, 19 );
         }
      } 

      echo '<hr>';
   }

   if ($xData_Inc != '0000-00-00T00:00:00') {
   // $strData_Up = str_replace('T', '~', $xData_Inc);      
      $strData_Up = xFunc_incTime( substr( $xData_Inc, 11, 8 ) );
      $xData_Inc = substr( $xData_Inc, 0, 10 );

      $mopi_query = "UPDATE tabcontrol SET inc_dateTime = '". ($xData_Inc .'~'. $strData_Up) ."', alt_dateTime = '". ($xData_Inc .'~'. $strData_Up) ."' WHERE Processo = 'Respons_ST'";
   // $mopi_result = mysql_query( $mopi_query ); 


      if ($strData_upAluno != '0000-00-00T00:00:00') {
         $strData_Up = xFunc_incTime( substr( $strData_upAluno, 11, 8 ) );
         $strData_upAluno = substr( $strData_upAluno, 0, 10 );

         $mopi_query = "UPDATE tabcontrol SET alt_dateTime = '". ($strData_upAluno .'~'. $strData_Up) ."' WHERE Processo = 'Alunos_ST'";
      // $mopi_result = mysql_query( $mopi_query ); 
      }
   }

   if ($xData_Alt != '0000-00-00T00:00:00') {
      $strData_Up = xFunc_incTime( substr( $xData_Alt, 11, 8 ) );
      $xData_Alt = substr( $xData_Alt, 0, 10 );

      $mopi_query = "UPDATE tabcontrol SET alt_dateTime = '". ($xData_Alt .'~'. $strData_Up) ."' WHERE Processo = 'Respons_ST'";
   // $mopi_result = mysql_query( $mopi_query ); 
   }

   $mopi_query = "DELETE FROM tresponsavelpessoas";
// $mopi_result = mysql_query( $mopi_query ); 

   $mopi_query = "UPDATE tabcontrol SET inc_Flag = '0', alt_Flag = '0' WHERE Processo = 'Respons_TS'";
// $mopi_result = mysql_query( $mopi_query ); 

}

</script>
