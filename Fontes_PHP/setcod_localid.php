﻿<script language="php">

function setNacionalidade( $parmInform ) {
   $xReturn = '';

   switch ( $parmInform ) {
   case 'afeganistao':
      $xReturn = '241';
      break;
   case 'albania':
      $xReturn = '093';
      break;
   case 'alema':
      $xReturn = '30';
      break;
   case 'andorra':
      $xReturn = '094';
      break;
   case 'angola':
      $xReturn = '175';
      break;
   case 'antigua_dep_barbuda':
      $xReturn = '028';
      break;
   case 'antilhas_holandesas':
      $xReturn = '029';
      break;
   case 'arabia_saudita':
      $xReturn = '242';
      break;
   case 'argelia':
      $xReturn = '176';
      break;
   case 'argentina':
      $xReturn = '21';
      break;
   case 'armenia':
      $xReturn = '347';
      break;
   case 'aruba':
      $xReturn = '033';
      break;
   case 'asiatica_exc_jap':
      $xReturn = '49';
      break;
   case 'australia':
      $xReturn = '288';
      break;
   case 'austria':
      $xReturn = '095';
      break;
   case 'barbados':
      $xReturn = '044';
      break;
   case 'belga':
      $xReturn = '31';
      break;
   case 'belize':
      $xReturn = '046';
      break;
   case 'bermudas':
      $xReturn = '083';
      break;
   case 'boliviana':
      $xReturn = '22';
      break;
   case 'britanica':
      $xReturn = '32';
      break;
   case 'bulgaria':
      $xReturn = '096';
      break;
   case 'cabo_verde':
      $xReturn = '343';
      break;
   case 'camaroes':
      $xReturn = '181';
      break;
   case 'canadense':
      $xReturn = '34';
      break;
   case 'catar':
      $xReturn = '247';
      break;
   case 'cazaquistao':
      $xReturn = '143';
      break;
   case 'chilena':
      $xReturn = '23';
      break;
   case 'china_taiwan':
      $xReturn = '249';
      break;
   case 'chinesa':
      $xReturn = '42';
      break;
   case 'chipre':
      $xReturn = '097';
      break;
   case 'cingapura':
      $xReturn = '275';
      break;
   case 'colombia':
      $xReturn = '026';
      break;
   case 'comunidade_bahamas':
      $xReturn = '040';
      break;
   case 'comunidade_dominicana':
      $xReturn = '054';
      break;
   case 'coreana':
      $xReturn = '43';
      break;
   case 'costa_do_marfim':
      $xReturn = '186';
      break;
   case 'costa_rica':
      $xReturn = '051';
      break;
   case 'croacia':
      $xReturn = '130';
      break;
   case 'cuba':
      $xReturn = '052';
      break;
   case 'curacao':
      $xReturn = '053';
      break;
   case 'dinamarca':
      $xReturn = '098';
      break;
   case 'eire':
      $xReturn = '099';
      break;
   case 'emirados_arabes_unidos':
      $xReturn = '251';
      break;
   case 'equador':
      $xReturn = '027';
      break;
   case 'escocia':
      $xReturn = '100';
      break;
   case 'eslovaquia':
      $xReturn = '136';
      break;
   case 'eslovenia':
      $xReturn = '132';
      break;
   case 'espanhola':
      $xReturn = '35';
      break;
   case 'estado_cidade_vaticano':
      $xReturn = '129';
      break;
   case 'estados_das_antilhas':
      $xReturn = '057';
      break;
   case 'filipinas':
      $xReturn = '252';
      break;
   case 'finlandia':
      $xReturn = '102';
      break;
   case 'francesa':
      $xReturn = '37';
      break;
   case 'gambia':
      $xReturn = '192';
      break;
   case 'gana':
      $xReturn = '193';
      break;
   case 'georgia':
      $xReturn = '148';
      break;
   case 'gibraltar':
      $xReturn = '103';
      break;
   case 'granada':
      $xReturn = '059';
      break;
   case 'grecia':
      $xReturn = '104';
      break;
   case 'groenlandia':
      $xReturn = '084';
      break;
   case 'guatemala':
      $xReturn = '061';
      break;
   case 'guiana_francesa':
      $xReturn = '087';
      break;
   case 'holanda':
      $xReturn = '105';
      break;
   case 'honduras':
      $xReturn = '064';
      break;
   case 'honduras_britanicas':
      $xReturn = '063';
      break;
   case 'hong_kong':
      $xReturn = '253';
      break;
   case 'hungria':
      $xReturn = '106';
      break;
   case 'ilha_milhos':
      $xReturn = '069';
      break;
   case 'ilhas_falklands':
      $xReturn = '058';
      break;
   case 'ilhas_faroes':
      $xReturn = '101';
      break;
   case 'ilhas_guadalupe':
      $xReturn = '060';
      break;
   case 'ilhas_malvinas':
      $xReturn = '067';
      break;
   case 'ilhas_serranas':
      $xReturn = '065';
      break;
   case 'ilhas_turca':
      $xReturn = '080';
      break;
   case 'ilhas_turks_caicos':
      $xReturn = '047';
      break;
   case 'ilhas_virgens_americanas':
      $xReturn = '082';
      break;
   case 'ilhas_virgens_britanicas':
      $xReturn = '081';
      break;
   case 'india':
      $xReturn = '255';
      break;
   case 'indonesia':
      $xReturn = '256';
      break;
   case 'inglaterra':
      $xReturn = '110';
      break;
   case 'ira':
      $xReturn = '257';
      break;
   case 'iraque':
      $xReturn = '258';
      break;
   case 'irlanda':
      $xReturn = '112';
      break;
   case 'irlanda_norte':
      $xReturn = '111';
      break;
   case 'islandia':
      $xReturn = '113';
      break;
   case 'israel':
      $xReturn = '259';
      break;
   case 'lugoslavia':
      $xReturn = '114';
      break;
   case 'jamaica':
      $xReturn = '066';
      break;
   case 'japonesa':
      $xReturn = '41';
      break;
   case 'jordania':
      $xReturn = '260';
      break;
   case 'libano':
      $xReturn = '264';
      break;
   case 'liberia':
      $xReturn = '201';
      break;
   case 'libia':
      $xReturn = '202';
      break;
   case 'malasia':
      $xReturn = '266';
      break;
   case 'maldivas':
      $xReturn = '267';
      break;
   case 'marrocos':
      $xReturn = '207';
      break;
   case 'martinica':
      $xReturn = '068';
      break;
   case 'mexico':
      $xReturn = '085';
      break;
   case 'mocambique':
      $xReturn = '210';
      break;
   case 'monaco':
      $xReturn = '118';
      break;
   case 'mongolia':
      $xReturn = '269';
      break;
   case 'monte_montenegro':
      $xReturn = '070';
      break;
   case 'naturalizado_brasileiro':
      $xReturn = '20';
      break;
   case 'nepal':
      $xReturn = '270';
      break;
   case 'nicaragua':
      $xReturn = '071';
      break;
   case 'nigeria':
      $xReturn = '213';
      break;
   case 'norte_americana':
      $xReturn = '36';
      break;
   case 'noruega':
      $xReturn = '119';
      break;
   case 'nova_zelandia':
      $xReturn = '317';
      break;
   case 'outros':
      $xReturn = '50';
      break;
   case 'palestina':
      $xReturn = '272';
      break;
   case 'panama':
      $xReturn = '072';
      break;
   case 'panama_zona_canal':
      $xReturn = '073';
      break;
   case 'paquistao':
      $xReturn = '273';
      break;
   case 'peru':
      $xReturn = '089';
      break;
   case 'polinesia_francesa':
      $xReturn = '322';
      break;
   case 'polonia':
      $xReturn = '123';
      break;
   case 'porto_rico':
      $xReturn = '074';
      break;
   case 'portuguesa':
      $xReturn = '45';
      break;
   case 'quenia':
      $xReturn = '217';
      break;
   case 'quitasueno':
      $xReturn = '075';
      break;
   case 'republica_africa_sul':
      $xReturn = '173';
      break;
   case 'republica_de_el_salvador':
      $xReturn = '056';
      break;
   case 'republica_haiti':
      $xReturn = '062';
      break;
   case 'republica_dominicana':
      $xReturn = '055';
      break;
   case 'republica_guiana':
      $xReturn = '088';
      break;
   case 'republica_tcheca':
      $xReturn = '135';
      break;
   case 'romenia':
      $xReturn = '124';
      break;
   case 'roncador':
      $xReturn = '076';
      break;
   case 'russia':
      $xReturn = '348';
      break;
   case 'santa_lucia':
      $xReturn = '077';
      break;
   case 'sao_cristovao':
      $xReturn = '078';
      break;
   case 'sao_vicente':
      $xReturn = '079';
      break;
   case 'senegal':
      $xReturn = '349';
      break;
   case 'servia':
      $xReturn = '131';
      break;
   case 'siria':
      $xReturn = '277';
      break;
   case 'suecia':
      $xReturn = '126';
      break;
   case 'suica':
      $xReturn = '38';
      break;
   case 'suriname':
      $xReturn = '090';
      break;
   case 'tailandia':
      $xReturn = '279';
      break;
   case 'tanzania':
      $xReturn = '350';
      break;
   case 'trindade_tobago':
      $xReturn = '091';
      break;
   case 'turquia':
      $xReturn = '281';
      break;
   case 'ucrania':
      $xReturn = '165';
      break;
   case 'uniao_sovietica':
      $xReturn = '167';
      break;
   case 'uruguaia':
      $xReturn = '25';
      break;
   case 'venezuela':
      $xReturn = '092';
      break;
   case 'zambia':
      $xReturn = '237';
      break;
   default:
      $xReturn = '10';
   };

   return $xReturn;
}



function setEstado_Descr( $parmInform ) {
   $xReturn = '';

   switch ($parmInform) {
   case 'AC':
     $xReturn = 'Acre';
     break;
   case 'AL':
     $xReturn = 'Alagoas';
     break;
   case 'AM':
     $xReturn = 'Amazonas';
     break;
   case 'AP':
     $xReturn = 'Amapa';
     break;
   case 'BA':
     $xReturn = 'Bahia';
     break;
   case 'CE':
     $xReturn = 'Ceara';
     break;
   case 'DF':
     $xReturn = 'Distrito Federal';
     break;
   case 'ES':
     $xReturn = 'Espirito Santo';
     break;
   case 'GO':
     $xReturn = 'Goias';
     break;
   case 'MA':
     $xReturn = 'Maranhao';
     break;
   case 'MG':
     $xReturn = 'Minas Gerais';
     break;
   case 'MS':
     $xReturn = 'Mato Grosso Sul';
     break;
   case 'MT':
     $xReturn = 'Mato Grosso';
     break;
   case 'PA':
     $xReturn = 'Para';
     break;
   case 'PB':
     $xReturn = 'Paraiba';
     break;
   case 'PE':
     $xReturn = 'Pernambuco';
     break;
   case 'PI':
     $xReturn = 'Piaui';
     break;
   case 'PR':
     $xReturn = 'Parana';
     break;
   case 'RJ':
     $xReturn = 'Rio Janeiro';
     break;
   case 'RN':
     $xReturn = 'Rio Grande Norte';
     break;
   case 'RO':
     $xReturn = 'Rondonia';
     break;
   case 'RR':
     $xReturn = 'Roraima';
     break;
   case 'RS':
     $xReturn = 'Rio Grande Sul';
     break;
   case 'SC':
     $xReturn = 'Santa Catarina';
     break;
   case 'SE':
     $xReturn = 'Sergipe';
     break;
   case 'SP':
     $xReturn = 'Sao Paulo';
     break;
   case 'TO':
     $xReturn = 'Tocantins';
     break;
   default:
     $xReturn = 'Outro';
   };

   return $xReturn;
}



function setEstado_Siglas( $parmInform ) {
   $xReturn = '';

   switch ($parmInform) {
   case 'acre':
     $xReturn = 'AC';
     break;
   case 'alagoas':
     $xReturn = 'AL';
     break;
   case 'amazonas':
     $xReturn = 'AM';
     break;
   case 'amapa':
     $xReturn = 'AP';
     break;
   case 'bahia':
     $xReturn = 'BA';
     break;
   case 'ceara':
     $xReturn = 'CE';
     break;
   case 'distrito_federal':
     $xReturn = 'DF';
     break;
   case 'espirito_santo':
     $xReturn = 'ES';
     break;
   case 'goias':
     $xReturn = 'GO';
     break;
   case 'maranhao':
     $xReturn = 'MA';
     break;
   case 'minas_gerais':
     $xReturn = 'MG';
     break;
   case 'mato_grosso_sul':
     $xReturn = 'MS';
     break;
   case 'mato_grosso':
     $xReturn = 'MT';
     break;
   case 'para':
     $xReturn = 'PA';
     break;
   case 'paraiba':
     $xReturn = 'PB';
     break;
   case 'pernambuco':
     $xReturn = 'PE';
     break;
   case 'piaui':
     $xReturn = 'PI';
     break;
   case 'parana':
     $xReturn = 'PR';
     break;
   case 'rio_janeiro':
     $xReturn = 'RJ';
     break;
   case 'rio_grande_norte':
     $xReturn = 'RN';
     break;
   case 'rondonia':
     $xReturn = 'RO';
     break;
   case 'roraima':
     $xReturn = 'RR';
     break;
   case 'rio_grande_sul':
     $xReturn = 'RS';
     break;
   case 'santa_catarina':
     $xReturn = 'SC';
     break;
   case 'sergipe':
     $xReturn = 'SE';
     break;
   case 'sao_paulo':
     $xReturn = 'SP';
     break;
   case 'tocantins':
     $xReturn = 'TO';
     break;
   default:
     $xReturn = '';
   };

   return $xReturn;
}

</script>