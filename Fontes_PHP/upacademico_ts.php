
<script language="php">

// header('Content-Type: text/html; charset=utf-8');

$xFirstName = chr(32);
$xLastName = chr(32);
$xFrom_charSet = "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ";
$xTo_charSet = "aaaaeeiooouucAAAAEEIOOOUUC";


                  // __.-  --=-=- -===--.__.functions.ini.__.--===- -=-=--  -.__ \\

function call(
    $url,
    $oauthtoken='e380a949-1585-bbd8-ed56-552eb76ada72',
    $type='POST',
    $arguments=array(),
    $encodeData=true,
    $returnHeaders=false
)
{
    $type = strtoupper($type);

    if ($type == 'GET')
    {
        $url .= '?' . http_build_query($arguments);
    }

    $curl_request = curl_init($url);

    if ($type == 'POST')
    {
        curl_setopt($curl_request, CURLOPT_POST, 1);
    }
    elseif ($type == 'PUT')
    {
        curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, 'PUT');
    }
    elseif ($type == 'DELETE')
    {
        curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, 'DELETE');
    }

    curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($curl_request, CURLOPT_HEADER, $returnHeaders);
    curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

    if (!empty($oauthtoken))
    {
        $token = array("oauth-token: {$oauthtoken}");
        curl_setopt($curl_request, CURLOPT_HTTPHEADER, $token);
    }

    if (!empty($arguments) && $type !== 'GET')
    {
        if ($encodeData)
        {
            //encode the arguments as JSON
            $arguments = json_encode($arguments);
        }
        curl_setopt($curl_request, CURLOPT_POSTFIELDS, $arguments);
    }

    $result = curl_exec($curl_request);

    if ($returnHeaders)
    {
        //set headers from response
        list($headers, $content) = explode("\r\n\r\n", $result ,2);
        foreach (explode("\r\n",$headers) as $header)
        {
            header($header);
        }

        //return the nonheader data
        return trim($content);
    }

    curl_close($curl_request);

    //decode the response from JSON
    $response = json_decode($result);

    return $response;
}



    function call_rs($method, $parameters, $url)
    {
        ob_start();
        $curl_request = curl_init();

        curl_setopt($curl_request, CURLOPT_URL, $url);
        curl_setopt($curl_request, CURLOPT_POST, 1);
        curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($curl_request, CURLOPT_HEADER, 1);
        curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

        $jsonEncodedData = json_encode($parameters);

        $post = array(
             "method" => $method,
             "input_type" => "JSON",
             "response_type" => "JSON",
             "rest_data" => $jsonEncodedData
        );

        curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
        $result = curl_exec($curl_request);
        curl_close($curl_request);

        $result = explode("\r\n\r\n", $result, 2);
        $response = json_decode($result[1]);
        ob_end_flush();

        return $response;
    }



function acentRemove( $parmInform ) {
   global $xFrom_charSet, $xTo_charSet;
   $xReturn = '';

   $xInform = trim( $parmInform );
   $xReturn = strtr($xInform, utf8_decode( $xFrom_charSet ), $xTo_charSet);

   return $xReturn;
}



function setTurma( $parmInform ) {
   $xReturn = '';

   $xTurma = acentRemove( $parmInform );
   $xReturn = utf8_encode( $xTurma );

   return $xReturn;
}



function setTipoMatric( $parmInform ) {
   $xReturn = '';

   switch ($parmInform) {
   case '1':
     $xReturn = 'inicial';
     break;
   case '2':
     $xReturn = 'renovada';
     break;
   case '3':
     $xReturn = 'pos_transferencia';
     break;
   case '4':
     $xReturn = 'pre_matricula';
     break;
   default:
     $xReturn = '-vazio-';
   };

   return $xReturn;
}



function checkInfor( $parmInform ) {
   $xReturn = '';

   if ($parmInform != '-vazio-')
      $xReturn = utf8_encode( $parmInform );

   return $xReturn;
}



function setTipoAluno( $parmInform ) {
   $xReturn = '';

   switch ($parmInform) {
   case '1':
     $xReturn = 'cursando';
     break;
   case '2':
     $xReturn = 'cancelado';
     break;
   case '3':
     $xReturn = 'transferido';
     break;
   case '4':
     $xReturn = 'nao_renovado';
     break;
   case '5':
     $xReturn = 'em_rematricula';
     break;
   case '6':
     $xReturn = 'prospect';
     break;
   case '7':
     $xReturn = 'prospect_cancelado';
     break;
   default:
     $xReturn = '-vazio-';
   };

   return $xReturn;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - \\


function setCurso( $parmInform ) {
   $xReturn = '';

   switch ($parmInform) {
   case '1':
     $xReturn = 'educacao_infantil';
     break;
   case '2':
     $xReturn = 'ensino_fundamental_i';
     break;
   case '3':
     $xReturn = 'ensino_fundametal_ii';
     break;
   case '4':
     $xReturn = 'ensino_medio';
     break;
   case '5':
     $xReturn = 'horario_mopi';
     break;
   case '6':
     $xReturn = 'atividades_esportivas';
     break;
   case '7':
     $xReturn = 'oficinas';
     break;
   default:
     $xReturn = '-vazio-';
   };

   return $xReturn;
}



function setSerie( $parmInform ) {
   $xReturn = '';

   switch ($parmInform) {
   case '2':
     $xReturn = 'mat_1';
     break;
   case '3':
     $xReturn = 'mat_2';
     break;
   case '4':
     $xReturn = 'pre_1';
     break;
   case '5':
     $xReturn = 'pre_2';
     break;
   case '6':
     $xReturn = '1_ano';
     break;
   case '7':
     $xReturn = '2_ano';
     break;
   case '8':
     $xReturn = '3_ano';
     break;
   case '9':
     $xReturn = '4_ano';
     break;
   case '10':
     $xReturn = '5_ano';
     break;
   case '11':
     $xReturn = '6_ano';
     break;
   case '12':
     $xReturn = '7_ano';
     break;
   case '13':
     $xReturn = '8_ano';
     break;
   case '14':
     $xReturn = '9_ano';
     break;
   case '15':
     $xReturn = 'em_1serie';
     break;
   case '16':
     $xReturn = 'em_2serie';
     break;
   case '17':
     $xReturn = 'em_3serie';
     break;
   default:
     $xReturn = '-vazio-';
   };

   return $xReturn;
}



function setTurno( $parmInform ) {
   $xReturn = '';
   $tempInform = strtoupper( $parmInform );

   $xInforPos = strPos($tempInform, 'MANH');
   if (is_numeric( $xInforPos ))
     $xReturn = 'manha';
   else
     $xReturn = 'tarde';

   return $xReturn;
}

                  // __.-  --=-=- -===--.__.functions.end.__.--===- -=-=--  -.__ \\


   $url_rs = 'https://mopi1.sugarondemand.com/service/v2/rest.php';
   $username = "Alfredo";
   $password = "bSa9RW";

// login
   $login_parameters = array(
      'user_auth' => array(
         'user_name' => 'Alfredo',
         'password' => md5('bSa9RW'),
      ),
   );

   $login_result = call_rs("login", $login_parameters, $url_rs);

   /*
   echo "<pre>";
   print_r($login_result);
   echo "</pre>";
   */

// get session id
   $session_id = $login_result->id;
// echo $session_id ."<br>";




$base_url = 'https://mopi1.sugarondemand.com/rest/v10';

//Login - POST /oauth2/token

$url = $base_url . '/oauth2/token';

$oauth2_token_arguments = array(
   'grant_type' => 'password',
   'client_id' => 'sugar',
   'client_secret' => '',
   'username' => 'Alfredo',
   'password' => 'bSa9RW',
   'platform' => 'base'
);

$oauth2_token_response = call($url, '', 'POST', $oauth2_token_arguments); 


                  // __.-  --=-=- -===--.__.®.__.--===- -=-=--  -.__ \\


$mopi_servername = "199.201.89.205";
$mopi_username = "clicksof_mopi";
$mopi_password = "clicksof.mopi";
$mopi_database = "clicksof_mopi_integracao";


// Create connection
   $mopi_conn = mysql_connect($mopi_servername, $mopi_username, $mopi_password) or die ("Erro no servidor: ".mysql_error());
   $xBD = mysql_select_db($mopi_database, $mopi_conn) or die ("Erro base: ".mysql_error());





$mopi_query = "SELECT * FROM talunoacademicos";
$xCont = 1;
$xRegAcademico = 0;
$mopi_result = mysql_query( $mopi_query );

if (mysql_num_rows( $mopi_result ) > 0) {
   while ($mopi_row = mysql_fetch_array( $mopi_result )) {

      if ( $mopi_row['ra'] != $xRegAcademico ) {
         $filter_arguments = array(
             "filter" => array(
                   array(
                     '$or' => array(
                         array(
                             //or name is equal to 'My Account'
                             "registro_academico_c" => $mopi_row['ra']
                         )
                     ),
                 ), 
             ),
         );


         $url = $base_url . '/Contacts/filter';

         $filter_response = call($url, $oauth2_token_response->access_token, 'POST', $filter_arguments);

      // echo '<br>'. $filter_response->records['0']->id .' / '. $filter_response->records['0']->account_id_c .'<br>';

         $url = $base_url . '/Contacts/'. $filter_response->records['0']->id;



      // $xAluno_ID = 'a56931d0-8736-cff7-424a-5575aeb810be';
      // $xRespons_ID = '32ea8447-5496-1679-5fb7-5575f76ccf02';
         $xAluno_ID = $filter_response->records['0']->id;
         $xRespons_ID = $filter_response->records['0']->account_id_c;
      }

      $xTipoMatric = setTipoMatric( $mopi_row['tipoMatricula'] );
      $xTipoAluno = setTipoAluno( $mopi_row['tipoAluno'] );

      $xAnoLetivo = $mopi_row['anoLetivo'];

      $xCurso = setCurso( $mopi_row['codCurso'] );
      $xSerie = setSerie( $mopi_row['codSerie'] );
      $xTurma = setTurma( $mopi_row['turma'] );
      $xTurno = setTurno( $mopi_row['turno'] );


      if ( $mopi_row['ra'] != $xRegAcademico ) {
         $record_arguments = array (
            "tipo_aluno_c" => $xTipoAluno,
            "tipo_matricula_c" => $xTipoMatric,
            "ano_letivo_corrente_c" => $xAnoLetivo,
            "curso_c" => $xCurso,
            "serie_c" => $xSerie,
            "turma_c" => $xTurma,
            "turno_c" => $xTurno
         );

         $record_response = call($url, $oauth2_token_response->access_token, 'PUT', $record_arguments);

         echo '<br>'. $xCont .' ) &nbsp; '. $record_response->id .' / '. $record_response->name .'<br>';

         $xRegAcademico = $mopi_row['ra'];
      }

      // _  __  _  __.- - --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=-- - -.__  _  __  _ \\


      $url = $base_url . '/Opportunities';

      $record_arguments = array( 'name' => $xAnoLetivo, 'curso_c' => $xCurso, 'serie_c' => $xSerie, 'turma_atual_c' => $xTurma, 
                                 'contact_id_c' => $xAluno_ID, 'account_id' => $xRespons_ID); // , 'contacts_opportunities_1contacts_ida' => $xAluno_ID ); 

      echo "<pre>";
      print_r($record_arguments);
      echo "</pre>";

   // echo '<br> __.-  --=-=- -===--.__.®.__.--===- -=-=--  -.__ <br>';

      $record_response = call($url, $oauth2_token_response->access_token, 'POST', $record_arguments);
      if ( empty($record_response->id) )
         exit();
      $xRM_ID = $record_response->id;
      echo '<br>'. $xCont++ .' ) &nbsp; '. $xRM_ID .' / '. $record_response->name .'<br>';

   /* echo '<pre>';
      print_r($record_response);
      echo '</pre>'; */



    //retrieve related list ------------------------------     
    $set_relationships_parameters = array(
        //session id
        'session' => $session_id,

        //The name of the module.
        'module_name' => 'Opportunities',

        //The ID of the specified module bean.
        'module_id' => $xRM_ID,

        //The relationship name of the linked field from which to relate records.
        'link_field_name' => 'contacts',

        //The list of record ids to relate
        'related_ids' => array(
            $xAluno_ID
        ),

        //Sets the value for relationship based fields
        'name_value_list' => array(), 

        //Whether or not to delete the relationship. 0:create, 1:delete
        'delete'=> 0
    );

   /* echo '<pre>';
      print_r($set_relationships_parameters);
      echo '</pre>'; */

    $set_relationships_result = call_rs("set_relationship", $set_relationships_parameters, $url_rs);

   /* echo '<pre>';
      print_r($set_relationships_result);
      echo '</pre>'; */

   }

   $mopi_query = "DELETE FROM talunoacademicos";
   $mopi_result = mysql_query( $mopi_query ); 


} else {
   echo '<br>...nao existe dados a serem processados!'. $xAnoLet;
}


</script>
