
<script language="php">

// header('Content-Type: text/html; charset=utf-8');


$xFirstName = chr(32);
$xLastName = chr(32);
$xFrom_charSet = "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ";
$xTo_charSet = "aaaaeeiooouucAAAAEEIOOOUUC";


                  // __.-  --=-=- -===--.__.functions.ini.__.--===- -=-=--  -.__ \\

function call(
    $url,
    $oauthtoken='e380a949-1585-bbd8-ed56-552eb76ada72',
    $type='POST',
    $arguments=array(),
    $encodeData=true,
    $returnHeaders=false
)
{
    $type = strtoupper($type);

    if ($type == 'GET')
    {
        $url .= '?' . http_build_query($arguments);
    }

    $curl_request = curl_init($url);

    if ($type == 'POST')
    {
        curl_setopt($curl_request, CURLOPT_POST, 1);
    }
    elseif ($type == 'PUT')
    {
        curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, 'PUT');
    }
    elseif ($type == 'DELETE')
    {
        curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, 'DELETE');
    }

    curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($curl_request, CURLOPT_HEADER, $returnHeaders);
    curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

    if (!empty($oauthtoken))
    {
        $token = array("oauth-token: {$oauthtoken}");
        curl_setopt($curl_request, CURLOPT_HTTPHEADER, $token);
    }

    if (!empty($arguments) && $type !== 'GET')
    {
        if ($encodeData)
        {
            //encode the arguments as JSON
            $arguments = json_encode($arguments);
        }
        curl_setopt($curl_request, CURLOPT_POSTFIELDS, $arguments);
    }

    $result = curl_exec($curl_request);

    if ($returnHeaders)
    {
        //set headers from response
        list($headers, $content) = explode("\r\n\r\n", $result ,2);
        foreach (explode("\r\n",$headers) as $header)
        {
            header($header);
        }

        //return the nonheader data
        return trim($content);
    }

    curl_close($curl_request);

    //decode the response from JSON
    $response = json_decode($result);

    return $response;
}

                  // __.-  --=-=- -===--.__.functions.end.__.--===- -=-=--  -.__ \\



$base_url = 'https://mopi1.sugarondemand.com/rest/v10';

//Login - POST /oauth2/token

$url = $base_url . '/oauth2/token';

$oauth2_token_arguments = array(
   'grant_type' => 'password',
   'client_id' => 'sugar',
   'client_secret' => '',
   'username' => 'Alfredo',
   'password' => 'bSa9RW',
   'platform' => 'base'
);

$oauth2_token_response = call($url, '', 'POST', $oauth2_token_arguments); 


                  // __.-  --=-=- -===--.__.®.__.--===- -=-=--  -.__ \\


$mopi_servername = "173.246.40.203";
$mopi_username = "root";
$mopi_password = "c1l2i3c4k5";
$mopi_database = "mopiintegracao";

// Create connection
   $mopi_conn = mysql_connect($mopi_servername, $mopi_username, $mopi_password) or die ("Erro no servidor: ".mysql_error());
   $xBD = mysql_select_db($mopi_database, $mopi_conn) or die ("Erro base: ".mysql_error());



// $mopi_query = "SELECT * FROM etapas WHERE anoLetivo = 2015";
$mopi_query = "SELECT *, CAST(codEtapa AS DECIMAL) codEtapa_c FROM etapas ORDER BY anoLetivo, codDisc, codEtapa_c";
$xCont = 1;
$xAnoLetivo = 0;
$xIDLetivo = '#';
$xCodDisc = 0;
$xIDDisciplina = '#';

$mopi_result = mysql_query( $mopi_query );

if (mysql_num_rows( $mopi_result ) > 0) {
   while ($mopi_row = mysql_fetch_array( $mopi_result )) {

      // _  __  _  __.- - --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=-- - -.__  _  __  _ \\
      if ( $mopi_row['anoLetivo'] != $xAnoLetivo ) {
         $filter_arguments = array(
             "filter" => array(
                   array(
                     '$or' => array(
                         array(
                             "name" => $mopi_row['anoLetivo'] 
                         )
                     ),
                 ), 
             ),
         );
         $url = $base_url . '/ANO_ano_letivo/filter';

         $filter_response = call($url, $oauth2_token_response->access_token, 'POST', $filter_arguments);

         $xIDLetivo = $filter_response->records['0']->id;
         echo '<br>'. $xCont .' ) &nbsp; '. $filter_response->records['0']->id .' / '. $filter_response->records['0']->name .'<br>'; 

         $xAnoLetivo = $mopi_row['anoLetivo'];
      }




      if ( $mopi_row['codDisc'] != $xCodDisc ) {
         $filter_arguments = array(
             "filter" => array(
                   array(
                     '$or' => array(
                         array(
                             "coddisc_c" => $mopi_row['codDisc'] 
                         )
                     ),
                 ), 
             ),
         );
         $url = $base_url . '/DISCI_disciplina/filter';

         $filter_response = call($url, $oauth2_token_response->access_token, 'POST', $filter_arguments);

         $xIDDisciplina = $filter_response->records['0']->id;
         echo '<br>'. $xCont .' ) &nbsp; '. $filter_response->records['0']->id .' / '. $filter_response->records['0']->name .'<br>'; 

         $xCodDisc = $mopi_row['codDisc'];
      }
      // _  __  _  __.- - --=- - - - -=- -===--.__.®.__.--===- -=- - - - -=-- - -.__  _  __  _ \\


      $url = $base_url . '/ETAPA_etapa';

      $xDescr = utf8_encode( $mopi_row['descricao'] );

      $record_arguments = array( 'codetapa_c' => $mopi_row['codEtapa'], 'name' => $xDescr, 'disci_disciplina_id_c' => $xIDDisciplina, 'ano_ano_letivo_id_c' => $xIDLetivo, 'description' => $xDescr, 'tipoetapa_c' => $mopi_row['tipoEtapa'] ); 

      echo "<pre>";
      print_r($record_arguments);
      echo "</pre>";

   // echo '<br> __.-  --=-=- -===--.__.®.__.--===- -=-=--  -.__ <br>';

      $record_response = call($url, $oauth2_token_response->access_token, 'POST', $record_arguments);
      if ( empty($record_response->id) )
         exit();
      echo '<br>'. $xCont++ .' ) &nbsp; '. $record_response->id .'<br>';

   /* echo '<pre>';
      print_r($record_response);
      echo '</pre>'; */
   }
}

</script>
