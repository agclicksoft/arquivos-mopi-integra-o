
<script language="php">

// header('Content-Type: text/html; charset=utf-8');

$xFirstName = chr(32);
$xLastName = chr(32);
$xFrom_charSet = "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ";
$xTo_charSet = "aaaaeeiooouucAAAAEEIOOOUUC";


                  // __.-  --=-=- -===--.__.functions.ini.__.--===- -=-=--  -.__ \\

function call(
    $url,
    $oauthtoken='e380a949-1585-bbd8-ed56-552eb76ada72',
    $type='POST',
    $arguments=array(),
    $encodeData=true,
    $returnHeaders=false
)
{
    $type = strtoupper($type);

    if ($type == 'GET')
    {
        $url .= '?' . http_build_query($arguments);
    }

    $curl_request = curl_init($url);

    if ($type == 'POST')
    {
        curl_setopt($curl_request, CURLOPT_POST, 1);
    }
    elseif ($type == 'PUT')
    {
        curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, 'PUT');
    }
    elseif ($type == 'DELETE')
    {
        curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, 'DELETE');
    }

    curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($curl_request, CURLOPT_HEADER, $returnHeaders);
    curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

    if (!empty($oauthtoken))
    {
        $token = array("oauth-token: {$oauthtoken}");
        curl_setopt($curl_request, CURLOPT_HTTPHEADER, $token);
    }

    if (!empty($arguments) && $type !== 'GET')
    {
        if ($encodeData)
        {
            //encode the arguments as JSON
            $arguments = json_encode($arguments);
        }
        curl_setopt($curl_request, CURLOPT_POSTFIELDS, $arguments);
    }

    $result = curl_exec($curl_request);

    if ($returnHeaders)
    {
        //set headers from response
        list($headers, $content) = explode("\r\n\r\n", $result ,2);
        foreach (explode("\r\n",$headers) as $header)
        {
            header($header);
        }

        //return the nonheader data
        return trim($content);
    }

    curl_close($curl_request);

    //decode the response from JSON
    $response = json_decode($result);

    return $response;
}



function inverteData( $prmData ) {
  $xReturn = '';
  $parteData = explode("/", $prmData);

  if ($parteData[2] != '1900')
    $xReturn = $parteData[2] . "-" . $parteData[1] . "-" . $parteData[0];

  return $xReturn;  
}



function acentRemove( $parmInform ) {
   global $xFrom_charSet, $xTo_charSet;

   $xReturn = '';

   $xInform = trim( $parmInform );
   $xReturn = strtr($xInform, utf8_decode( $xFrom_charSet ), $xTo_charSet);

   return $xReturn;
}



function setFirst_Lastname( $parmFullName ) {
   global $xFirstName, $xLastName;

   $xFirstName = '';
   $xLastName = '';

   $xFullName = str_replace( '*', '', $parmFullName);
// $xFullName = acentRemove( $xFullName );
   $xFullName = utf8_encode( $xFullName );

   $xPos = strrpos($xFullName, chr(32));
   $xFirstName = substr( $xFullName, 0, $xPos );
   $xLastName = substr( $xFullName, $xPos + 1 );
}



function setSexo( $parmInform ) {
   $xReturn = '';

   if ($parmInform == 'F') 
      $xReturn = 'feminino';
   else
      $xReturn = 'masculino';

   return $xReturn;
}



function setEstado( $parmInform ) {
   $xReturn = '';

   switch ($parmInform) {
   case 'AC':
     $xReturn = 'acre';
     break;
   case 'AL':
     $xReturn = 'alagoas';
     break;
   case 'AM':
     $xReturn = 'amazonas';
     break;
   case 'AP':
     $xReturn = 'amapa';
     break;
   case 'BA':
     $xReturn = 'bahia';
     break;
   case 'CE':
     $xReturn = 'ceara';
     break;
   case 'DF':
     $xReturn = 'distrito_federal';
     break;
   case 'ES':
     $xReturn = 'espirito_santo';
     break;
   case 'GO':
     $xReturn = 'goias';
     break;
   case 'MA':
     $xReturn = 'maranhao';
     break;
   case 'MG':
     $xReturn = 'minas_gerais';
     break;
   case 'MS':
     $xReturn = 'mato_grosso_sul';
     break;
   case 'MT':
     $xReturn = 'mato_grosso';
     break;
   case 'PA':
     $xReturn = 'para';
     break;
   case 'PB':
     $xReturn = 'paraiba';
     break;
   case 'PE':
     $xReturn = 'pernambuco';
     break;
   case 'PI':
     $xReturn = 'piaui';
     break;
   case 'PR':
     $xReturn = 'parana';
     break;
   case 'RJ':
     $xReturn = 'rio_janeiro';
     break;
   case 'RN':
     $xReturn = 'rio_grande_norte';
     break;
   case 'RO':
     $xReturn = 'rondonia';
     break;
   case 'RR':
     $xReturn = 'roraima';
     break;
   case 'RS':
     $xReturn = 'rio_grande_sul';
     break;
   case 'SC':
     $xReturn = 'santa_catarina';
     break;
   case 'SE':
     $xReturn = 'sergipe';
     break;
   case 'SP':
     $xReturn = 'sao_paulo';
     break;
   case 'TO':
     $xReturn = 'tocantins';
     break;
   default:
     $xReturn = 'outro';
   };

   return $xReturn;
}



function checkInfor( $parmInform ) {
   $xReturn = '';

   if ($parmInform != '-vazio-')
      $xReturn = utf8_encode( $parmInform );

   return $xReturn;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - \\


function setEstCivil( $parmInform ) {
   $xReturn = '';

   switch (strtoupper( $parmInform )) {
   case 'C':
     $xReturn = 'casado';
     break;
   case 'D':
     $xReturn = 'desquitado';
     break;
   case 'I':
     $xReturn = 'divorsiado';
     break;
   case 'O':
     $xReturn = 'separado';
     break;
   case 'S':
     $xReturn = 'solteiro';
     break;
   case 'V':
     $xReturn = 'viuvo';
     break;
   default:
     $xReturn = '-vazio-';
   };

   return $xReturn;
}



function setUnidade( $parmInform ) {
   $xReturn = '';

   if ($parmInform == 'tijuca') 
      $xReturn = 'matriz';
   else
      $xReturn = 'filial_itanhanga';

   return $xReturn;
}

                  // __.-  --=-=- -===--.__.functions.end.__.--===- -=-=--  -.__ \\


$base_url = 'https://mopi1.sugarondemand.com/rest/v10';

//Login - POST /oauth2/token

$url = $base_url . '/oauth2/token';

$oauth2_token_arguments = array(
   'grant_type' => 'password',
   'client_id' => 'sugar',
   'client_secret' => '',
   'username' => 'Alfredo',
   'password' => 'bSa9RW',
   'platform' => 'base'
);

$oauth2_token_response = call($url, '', 'POST', $oauth2_token_arguments); 


                  // __.-  --=-=- -===--.__.®.__.--===- -=-=--  -.__ \\


$mopi_servername = "173.246.40.203";
$mopi_username = "root";
$mopi_password = "c1l2i3c4k5";
$mopi_database = "mopiintegracao";

// Create connection
   $mopi_conn = mysql_connect($mopi_servername, $mopi_username, $mopi_password) or die ("Erro no servidor: ".mysql_error());
   $xBD = mysql_select_db($mopi_database, $mopi_conn) or die ("Erro base: ".mysql_error());



$xCont = 1;
$mopi_query = "
SELECT alun.ra, alun.codPessoa, alun.unidade, 
       pess.*, Left(pess.dataNasciment, 10) dataNasc
  FROM alunos alun, alunopessoas pess
 WHERE pess.codPessoa = alun.codPessoa LIMIT 05
 ";


$mopi_result = mysql_query( $mopi_query );

if (mysql_num_rows( $mopi_result ) > 0) {
   while ($mopi_row = mysql_fetch_array( $mopi_result )) {

      $xUnidade = setUnidade( $mopi_row['unidade'] );
      setFirst_Lastname( $mopi_row['nome'] );
      $xSexo = setSexo( $mopi_row['sexo'] );
      $xEstNatal = setEstado( $mopi_row['estadoNatal'] );
      $xDataNasc = inverteData( $mopi_row['dataNasc'] );

      $xEstCivil = setEstCivil( $mopi_row['estadoCivil'] );

      $xCidade_UF = 'be0c70b3-7cc5-8351-9c79-54d2ad611b25';
      $xNacionalid = 'brasileira';

      $xEndRua = checkInfor( $mopi_row['endRua'] );
      $xEndComplement = checkInfor( $mopi_row['endComplement'] );
      $xEndBairro = checkInfor( $mopi_row['endBairro'] );

   // $xEndCidade = checkInfor( $mopi_row['endCidade'] );
   // $xEndEstado = checkInfor( $mopi_row['endEstado'] );
      $xEndNumero = checkInfor( $mopi_row['endNumero'] );
      $xEndCEP = checkInfor( $mopi_row['endCEP'] );

      $xEMail = checkInfor( $mopi_row['contEMail'] );
      $xCelular = checkInfor( $mopi_row['contCelular'] );
      $xTelResidenc = checkInfor( $mopi_row['contTelResidenc'] );


      $record_arguments = array( "registro_academico_c" => $mopi_row['ra'], "codpessoa_c" => $mopi_row['codPessoa'], "unidade_c" => $xUnidade, 
                                 "first_name" => $xFirstName, "last_name" => $xLastName, "sexo_c" => $xSexo, "data_nascimento_c" => $xDataNasc, 
                                 "nacionalidade_c" => $xNacionalid, "ciuf_cidades_uf_id1_c" => $xCidade_UF, "estado_natal_c" => $xEstNatal, 
                                 "estado_civil_c" => $xEstCivil, 

                                 "rua_c" => $xEndRua, "numero_c" => $xEndNumero, "complemento_c" => $xEndComplement, 
                                 "bairro_c" => $xEndBairro, "cep_c" => $xEndCEP, "ciuf_cidades_uf_id_c" => $xCidade_UF, 
                                 "email_aluno_c" => $xEMail, "celular_aluno_c" => $xCelular, "telefone_residencial_aluno_c" => $xTelResidenc );

      echo "<pre>";
         print_r($record_arguments);
      echo "</pre>";

   // echo '<br> __.-  --=-=- -===--.__.®.__.--===- -=-=--  -.__ <br>';

   /* $url = $base_url . '/Contacts';

      $record_response = call($url, $oauth2_token_response->access_token, 'POST', $record_arguments);

      echo '<br>'. $xCont++ .' ) &nbsp; '. $record_response->id .' / '. $record_response->name .'<br>';

   /* echo '<pre>';
         print_r($record_response);
      echo '</pre>'; */
   }
}

</script>
