
<html>

<head>
   <title>:: int.mobile ::</title>

   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body>
   <br> &nbsp; <br> 

   <script language="php">

   ini_set('display_errors', 1);

   function setArea( $parmInform ) {
      $xReturn = '';
      $xInform = '.'. $parmInform .'.';

      if ( $xInform == '.1.' ) 
         $xReturn = 'matematica';
      else if ( strPos('.02.10.11.21.40.', $xInform) > -1 ) 
         $xReturn = 'linguagens';
      else if ( strPos('.05.06.45.52.', $xInform) > -1 ) 
         $xReturn = 'ciencia_humanas';
      else if ( strPos('.16.54.', $xInform) > -1 ) 
         $xReturn = 'ciencia_natureza';
      else if ( strPos('.08.22.69.68.69.', $xInform) > -1 ) 
         $xReturn = 'nucleo_diversificado';
      /*
      matematica
      ----------
      1  Matemática

      linguagens
      ----------
      02 Língua Portuguesa
      10 Língua Estrangeira [Inglês]
      11 Língua Estrangeira [Espanhol]
      21 Literatura Infanto Juvenil
      40 Produção Textual

      ciencia_humana
      --------------
      05 História
      06 Geografia
      45 Filosofia
      52 Tecnologias de Informação e Comunicação

      ciencia_natureza
      ----------------
      16 Ciências
      54 Ciências Naturais

      nucleo_diversificado
      --------------------
      08 Educação Física
      22 Música
      69 Arte
      68 Natação
      69 Arte
      */
      return $xReturn;
   }



   $totvs_servername = "189.39.176.225";
   $totvs_username = "rm";
   $totvs_password = "rm";
   $totvs_database = "Corpore_Clicksoft";

   // Create connection
   $mopi_sql_conn = mssql_connect($totvs_servername, $totvs_username, $totvs_password) or die(mssql_get_last_message());

/* if($mopi_sql_conn != false)
      echo "Connected to the database server OK<br>";
   else
      die("Couldn't connect"); */


// $sqlserver_result = mssql_query('SELECT CODTURMA, IDPERLET, NOME FROM Corpore_Clicksoft.dbo.STURMA WHERE IDPERLET = 56 AND CODFILIAL = 4');

$xInstruct_SQL = "
  select distinct
    SA.RA as ra,
    case when SF.CODFILIAL is not null  then SF.CODFILIAL else '-1' end as codfilial,
    case when SPL.IDPERLET is not null then SPL.IDPERLET else '-1' end as idperlet,
    case when SPL.DESCRICAO is not null then SPL.DESCRICAO  else 'nulo' end as descperlet,
    case when ST.CODTURMA is not null then  ST.CODTURMA else 'nulo' end as codturma,
    case when ST.NOME is not null then ST.NOME else 'nulo'  end as descturma,
    case when SH.NOME is not null then SH.NOME else 'nulo'  end as serieturma,
    case when SC.NOME  is not null then SC.NOME else 'nulo' end as cursoturma,
    case when STN.TIPO is not null  then STN.TIPO else 'nulo' end as turnoturma,
    case when STD.IDTURMADISC is not null then STD.IDTURMADISC else '-1' end as idturmadisc,
    case when SD.CODDISC  is not null then SD.CODDISC else '-1' end as coddisc,
    case when SD.NOME is not null then SD.NOME else 'nulo' end as descdisc,
    case when SD.NOMEREDUZIDO is not null then SD.NOMEREDUZIDO else 'nulo' end as sigladisc,
    case when SR.CODSTATUS = 6 OR SR.CODSTATUS = 11  then SR.DESCRICAO when SR.CODSTATUS = 7 OR SR.CODSTATUS = 12 then SR.DESCRICAO when SR.CODSTATUS = 8 OR SR.CODSTATUS = 13 then SR.DESCRICAO when SR.CODSTATUS = 14 then SR.DESCRICAO  else 'Cursando'  end as  situacao
    from Corpore_Clicksoft.dbo.SMATRICULA SA
  inner join Corpore_Clicksoft.dbo.SMATRICPL SM on SM.CODCOLIGADA = SA.CODCOLIGADA and SM.RA = SA.RA and SM.IDPERLET = SA.IDPERLET and SM.IDHABILITACAOFILIAL = SA.IDHABILITACAOFILIAL and SM.CODFILIAL = 4 and SM.IDPERLET = 56
  inner join Corpore_Clicksoft.dbo.SPLETIVO SPL on SA.CODCOLIGADA = SPL.CODCOLIGADA and SA.IDPERLET = SPL.IDPERLET
  inner join Corpore_Clicksoft.dbo.SFILIAL SF on SPL.CODCOLIGADA = SF.CODCOLIGADA and SPL.CODFILIAL = SF.CODFILIAL
  inner join Corpore_Clicksoft.dbo.STURMADISC STD on SA.CODCOLIGADA = STD.CODCOLIGADA and SA.IDTURMADISC = STD.IDTURMADISC and SA.IDPERLET = STD.IDPERLET
  inner join Corpore_Clicksoft.dbo.SHABILITACAOFILIAL SHF on SHF.CODCOLIGADA = STD.CODCOLIGADA and SHF.IDHABILITACAOFILIAL = STD.IDHABILITACAOFILIAL and SHF.CODFILIAL = SF.CODFILIAL
  inner join Corpore_Clicksoft.dbo.SDISCIPLINA SD on SD.CODCOLIGADA = STD.CODCOLIGADA and SD.CODDISC = STD.CODDISC  
  inner join Corpore_Clicksoft.dbo.STURMA ST on ST.CODCOLIGADA = STD.CODCOLIGADA and ST.CODTURMA = STD.CODTURMA and SPL.IDPERLET = STD.IDPERLET and SHF.IDHABILITACAOFILIAL = ST.IDHABILITACAOFILIAL
  inner join Corpore_Clicksoft.dbo.SHABILITACAO SH on SH.CODCOLIGADA = SHF.CODCOLIGADA and SH.CODHABILITACAO = SHF.CODHABILITACAO and SHF.CODCURSO = SH.CODCURSO
  inner join Corpore_Clicksoft.dbo.SCURSO SC on SC.CODCOLIGADA = SH.CODCOLIGADA and SC.CODCURSO = SH.CODCURSO 
  inner join Corpore_Clicksoft.dbo.STURNO STN on STN.CODCOLIGADA = SHF.CODCOLIGADA and STN.CODTURNO = SHF.CODTURNO
  left outer join Corpore_Clicksoft.dbo.SSTATUS SR on SA.CODCOLIGADA = Sr.CODCOLIGADA and SA.CODSTATUSRES = SR.CODSTATUS
  where SPL.ENCERRADO = 'N' 
    and SA.RA in ('2009236', '0900817')
";


   $sqlserver_result = mssql_query( $xInstruct_SQL );

   $mysqlConect = mysql_connect("173.246.40.203", "root", "c1l2i3c4k5") or die("Conexao com mySQL nao estabelecida!");
   $mysqlBDados = mysql_select_db( "mopi_integracao" );

   while ( $row = mssql_fetch_array( $sqlserver_result ) ) {
   // var_dump( $row );

      $mopi_query = $row['ra'] .' ... '. $row['descperlet'] .' ... '. $row['codturma'] .' ... '. $row['descdisc'];
      echo '<br>'. $mopi_query .'<br>';


                  // __.-  --=-=- -===--.__.®.__.--===- -=-=--  -.__ \\
      $mopi_query = " SELECT * FROM periodo_letivos WHERE idperlet = '". $row['idperlet'] ."' ";
      $mysql_result = mysql_query( $mopi_query ); // or Die( $mopi_query );

      if (mysql_num_rows( $mysql_result ) < 1) {
         $mopi_query = "INSERT INTO periodo_letivos
                           (idperlet, descricao, created_at, updated_at, atual)
                        VALUES
                           ('". $row['idperlet'] ."', '". $row['descperlet'] ."', Now(), Now(), '1') ";
         $flagQuery = mysql_query( $mopi_query );
      }


                  // __.-  --=-=- -===--.__.®.__.--===- -=-=--  -.__ \\
      $mopi_query = " SELECT * FROM turmas WHERE codturma = '". $row['codturma'] ."' ";
   // echo $mopi_query .'<br>';
      $mysql_result = mysql_query( $mopi_query ); // or Die( $mopi_query );

      if (mysql_num_rows( $mysql_result ) < 1) {
         $mopi_query = "INSERT INTO turmas
                           (codturma, created_at, updated_at, idperlet, codfilial, descricao, serie, turno, curso)
                        VALUES
                           ('". $row['codturma'] ."', Now(), Now(), '". $row['idperlet'] ."', '". $row['codfilial'] ."', '". $row['descturma'] ."', '". $row['serieturma'] ."', '". $row['turnoturma'] ."', '". $row['cursoturma'] ."') ";
   // echo $mopi_query .'<br>';
      $flagQuery = mysql_query( $mopi_query );
      }


                  // __.-  --=-=- -===--.__.®.__.--===- -=-=--  -.__ \\
      $mopi_query = " SELECT * FROM disciplinas WHERE coddisc = '". $row['coddisc'] ."' ";
   // echo $mopi_query .'<br>';
      $mysql_result = mysql_query( $mopi_query ); // or Die( $mopi_query );

      if (mysql_num_rows( $mysql_result ) < 1) {
         $xArea = setArea( $row['coddisc'] );

         $mopi_query = "INSERT INTO disciplinas
                           (coddisc, descricao, created_at, updated_at, sigla, area)
                        VALUES
                           ('". $row['coddisc'] ."', '". $row['descdisc'] ."', Now(), Now(), '". $row['sigladisc'] ."', '". $xArea ."') ";
   // echo $mopi_query .'<br>';
      $flagQuery = mysql_query( $mopi_query );
      }


                  // __.-  --=-=- -===--.__.®.__.--===- -=-=--  -.__ \\
      $mopi_query = " SELECT * FROM turma_disciplinas WHERE idturmadisc = '". $row['idturmadisc'] ."' ";
   // echo $mopi_query .'<br>';
      $mysql_result = mysql_query( $mopi_query ); // or Die( $mopi_query );

      if (mysql_num_rows( $mysql_result ) < 1) {
         $mopi_query = "INSERT INTO turma_disciplinas
                           (idturmadisc, coddisc, codfilial, idperlet, codturma, created_at, updated_at)
                        VALUES
                           ('". $row['idturmadisc'] ."', '". $row['coddisc'] ."', '". $row['codfilial'] ."', '". $row['idperlet'] ."', '". $row['codturma'] ."', Now(), Now()) ";
   // echo $mopi_query .'<br>';
      $flagQuery = mysql_query( $mopi_query );
      }


                  // __.-  --=-=- -===--.__.®.__.--===- -=-=--  -.__ \\
      $mopi_query = " SELECT * FROM matriculas
                       WHERE RA = '". $row['ra'] ."'
                         AND codfilial = '". $row['codfilial'] ."'
                         AND idperlet = '". $row['idperlet'] ."'
                         AND codturma = '". $row['codturma'] ."' ";
   // echo $mopi_query .'<br>';
      $mysql_result = mysql_query( $mopi_query ); // or Die( $mopi_query );

      if (mysql_num_rows( $mysql_result ) < 1) {
         $mopi_query = "INSERT INTO matriculas
                           (RA, codfilial, idperlet, codturma, created_at, updated_at)
                        VALUES
                           ('". $row['ra'] ."', '". $row['codfilial'] ."', '". $row['idperlet'] ."', '". $row['codturma'] ."', Now(), Now()) ";
   // echo $mopi_query .'<br>';
      $flagQuery = mysql_query( $mopi_query );
      }


                  // __.-  --=-=- -===--.__.®.__.--===- -=-=--  -.__ \\
      $mopi_query = " SELECT * FROM notas
                       WHERE RA = '". $row['ra'] ."'
                         AND idturmadisc = '". $row['idturmadisc'] ."' ";
   // echo $mopi_query .'<br>';
      $mysql_result = mysql_query( $mopi_query ); // or Die( $mopi_query );

      if (mysql_num_rows( $mysql_result ) < 1) {
         $mopi_query = "INSERT INTO notas
                           (RA, situacao, created_at, updated_at, idturmadisc)
                        VALUES
                           ('". $row['ra'] ."', 'Cursando', Now(), Now(), '". $row['idturmadisc'] ."') ";
   // echo $mopi_query .'<br>';
      $flagQuery = mysql_query( $mopi_query );
      }
   }


   mysql_close( $mysqlConect );

   mssql_free_result($sqlserver_result);
   mssql_close($mopi_sql_conn);
   </script>

   <br> &nbsp; <br> 
</body>

</html>
